package ee.translate.keeleleek.mtpluginframework.chopup;

public class TemplateParameterElement {

	private String name;
	private String value;
	
	private boolean nameTouched = false;

	
	// INIT
	public TemplateParameterElement(String name, String value) {
		this.name = name;
		this.value = value;
		strip();
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
		this.nameTouched = true;
		strip();
	}
	
	public boolean isNameTouched() {
		return nameTouched;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	private void strip() {
		int i = name.indexOf('=');
		if (i == -1) return;
		value = name.substring(i + 1).trim();
		name = name.substring(0, i).trim();
	}
	
	
}
