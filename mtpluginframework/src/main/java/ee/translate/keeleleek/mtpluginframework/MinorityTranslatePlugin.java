package ee.translate.keeleleek.mtpluginframework;

/**
 * Common MinorityTranslate plugin interface.
 */
public interface MinorityTranslatePlugin {

	/**
	 * Gets plugin name.
	 * 
	 * @return plugin name
	 */
	public String getName();

	/**
	 * Gets localised plugin name.
	 * 
	 * @param langCode wiki language code
	 * @return plugin name
	 */
	public String getName(String langCode);

	/**
	 * Sets up the plugin.
	 * 
	 * @param plugins plugins callback
	 * @throws Exception when setup fails
	 */
	void setup(MinorityTranslatePlugins plugins) throws Exception;

	/**
	 * Closes the plugin.
	 * 
	 * @throws Exception when close fails
	 */
	void close() throws Exception;
	
	
}
