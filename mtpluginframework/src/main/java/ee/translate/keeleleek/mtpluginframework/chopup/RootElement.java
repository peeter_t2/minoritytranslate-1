package ee.translate.keeleleek.mtpluginframework.chopup;

public class RootElement extends ArticleElement {

	private String code = "";
	
	public RootElement() {
		super(Type.ROOT);
	}
	
	@Override
	public void init(String text) {
		this.code = text;
	}
	
	@Override
	protected String asCode() {
		return code;
	}

	public void dump()
	 {
		super.dump();
	 }
	
	
	
}
