package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SymbolFX {

	public final StringProperty dstLang;
	public final StringProperty name;
	public final StringProperty trigger;
	public final Character symbol;
	public final IntegerProperty row;
	
	
	public SymbolFX(Symbol symbol)
	 {
		this.dstLang = new SimpleStringProperty(symbol.getFilter().getDstLangRegex());
		this.name = new SimpleStringProperty(symbol.getName());
		this.trigger = new SimpleStringProperty(symbol.getTrigger());
		this.symbol = symbol.getSymbol();
		this.row = new SimpleIntegerProperty(symbol.getRow());
	 }

	public Symbol toSymbol()
	 {
		return new Symbol(dstLang.getValue(), name.getValue(), trigger.getValue(), symbol, row.getValue());
	 }
	
	
}
