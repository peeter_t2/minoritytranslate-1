package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class AddCategoryCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String title = (String) notification.getBody();
		String langCode = notification.getType();
		
		// request
		MinorityTranslateModel.queuer().requestCategory(langCode, title);
	 }
	
}
