package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.preferences.Insert;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InsertFX {

	public final StringProperty name;
	public final StringProperty trigger;
	public final StringProperty insert;
	
	
	public InsertFX(Insert template)
	 {
		this.name = new SimpleStringProperty(template.getName());
		this.trigger = new SimpleStringProperty(template.getTrigger());
		this.insert = new SimpleStringProperty(template.getInsert());
	 }

	public Insert toInsert()
	 {
		return new Insert(name.getValue(), trigger.getValue(), insert.getValue());
	 }
	
	
}
