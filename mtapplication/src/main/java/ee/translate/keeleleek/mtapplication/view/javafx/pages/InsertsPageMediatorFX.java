package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.Insert;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.InsertFX;
import ee.translate.keeleleek.mtapplication.view.pages.InsertsPageMediator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Callback;


public class InsertsPageMediatorFX extends InsertsPageMediator {

	@FXML
	private TableView<InsertFX> insertsTable;
	@FXML
	private TableColumn<InsertFX, String> nameColumn;
	@FXML
	private TableColumn<InsertFX, String> triggerColumn;

	@FXML
	private Button insertButton;
	@FXML
	private Button appendButton;
	@FXML
	private Button moveUpButton;
	@FXML
	private Button moveDownButton;
	@FXML
	private Button removeButton;
	@FXML
	private MenuButton variablesButton;

	@FXML
	private TextField nameEdit;
	@FXML
	private TextField triggerEdit;
	@FXML
	private TextArea insertEdit;
	
	
	// IMPLEMENT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		nameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<InsertFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<InsertFX, String> insert) {
				return insert.getValue().name;
			}
		});
		
		triggerColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<InsertFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<InsertFX, String> insert) {
				return insert.getValue().trigger;
			}
		});
		
		insertsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<InsertFX>() {
			@Override
			public void changed(ObservableValue<? extends InsertFX> obs, InsertFX oldVal, InsertFX newVal)
			 {
				if (oldVal != null) {
					oldVal.name.unbind();
					oldVal.trigger.unbind();
					oldVal.insert.unbind();
				}
				if (newVal != null) {
					
					nameEdit.setText(newVal.name.getValue());
					newVal.name.bind(nameEdit.textProperty());

					triggerEdit.setText(newVal.trigger.getValue());
					newVal.trigger.bind(triggerEdit.textProperty());
					
					insertEdit.setText(newVal.insert.getValue());
					newVal.insert.bind(insertEdit.textProperty());
					
				}
			 }
		});
	
		insertsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<InsertFX>() {
			@Override
			public void changed(ObservableValue<? extends InsertFX> obs, InsertFX oldVal, InsertFX newVal)
			 {
				appendButton.setDisable(false);
				insertButton.setDisable(false);
				moveUpButton.setDisable(newVal == null);
				moveUpButton.setDisable(newVal == null);
				moveDownButton.setDisable(newVal == null);
				removeButton.setDisable(newVal == null);
				nameEdit.setDisable(newVal == null);
				triggerEdit.setDisable(newVal == null);
				insertEdit.setDisable(newVal == null);
				if (newVal == null) {
					nameEdit.setText("");
					triggerEdit.setText("");
					insertEdit.setText("");
				}
			 }
		});
		
		variablesButton.getItems().clear();
		List<String> variables = MinorityTranslateModel.processer().collectAllVariables();
		for (String variable : variables) {
			MenuItem item = new MenuItem(variable);
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					if (triggerEdit.isFocused()) triggerEdit.replaceSelection(((MenuItem) event.getSource()).getText());
					else if (insertEdit.isFocused()) insertEdit.replaceSelection(((MenuItem) event.getSource()).getText());
				}
			});
			variablesButton.getItems().add(item);
		}
		
		triggerEdit.focusedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				variablesButton.setDisable(!triggerEdit.isFocused() && !insertEdit.isFocused());
			}
		});
		insertEdit.focusedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				variablesButton.setDisable(!triggerEdit.isFocused() && !insertEdit.isFocused());
			}
		});
	 }

	
	// PAGE
	@Override
	public void open(InsertsPreferences inserts)
	 {
		List<Insert> insertList = inserts.getInserts();
		ObservableList<InsertFX> insertListFX = FXCollections.observableArrayList();
		for (Insert insert : insertList) {
			insertListFX.add(new InsertFX(insert));
		}
		
		insertsTable.setItems(insertListFX);
		
		nameEdit.setText("");
		triggerEdit.setText("");
		insertEdit.setText("");
		
		insertsTable.getSelectionModel().selectFirst();
	 }
	
	@Override
	public InsertsPreferences close()
	 {
		ObservableList<InsertFX> insertListFX = insertsTable.getItems();
		ArrayList<Insert> insertList = new ArrayList<>(insertListFX.size());
		for (InsertFX insertFX : insertListFX) {
			insertList.add(insertFX.toInsert());
		}
		
		return new InsertsPreferences(insertList);
	 }
	
	
	// TABLE
	private void addRow(int index)
	 {
		ObservableList<InsertFX> items = insertsTable.getItems();
		if (index > items.size()) index = items.size();
		if (index < 0) index = 0;
		
		items.add(index, new InsertFX(new Insert("", "", "")));
		insertsTable.getSelectionModel().select(items.get(index));
	 }
	
	private void removeRow(int index)
	 {
		ObservableList<InsertFX> items = insertsTable.getItems();
		if (index >= items.size()) return;
		if (index < 0) return;

		boolean select = insertsTable.getSelectionModel().getSelectedIndex() != -1;
		
		items.remove(index);
		
		if (select) {
			if (index == items.size()) index--;
			insertsTable.getSelectionModel().select(index);
		}
	 }

	private void moveRowDown(int index)
	 {
		ObservableList<InsertFX> items = insertsTable.getItems();
		if (index >= items.size() - 1) return;
		if (index < 0) return;

		boolean select = insertsTable.getSelectionModel().getSelectedIndex() != -1;
		
		InsertFX item = items.remove(index);
		items.add(index + 1, item);

		if (select) {
			insertsTable.getSelectionModel().select(item);
		}
	 }

	private void moveRowUp(int index)
	 {
		ObservableList<InsertFX> items = insertsTable.getItems();
		if (index >= items.size()) return;
		if (index < 1) return;

		boolean select = insertsTable.getSelectionModel().getSelectedIndex() != -1;
		
		InsertFX item = items.remove(index);
		items.add(index - 1, item);

		if (select) {
			insertsTable.getSelectionModel().select(item);
		}
	 }
	
	
	// ACTIONS
	@FXML
	private void onAppendClick() {
		addRow(insertsTable.getSelectionModel().getSelectedIndex() + 1);
	}
	
	@FXML
	private void onInsertClick() {
		addRow(insertsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveUpClick() {
		moveRowUp(insertsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveDownClick() {
		moveRowDown(insertsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onRemoveClick() {
		removeRow(insertsTable.getSelectionModel().getSelectedIndex());
	}

	
}
