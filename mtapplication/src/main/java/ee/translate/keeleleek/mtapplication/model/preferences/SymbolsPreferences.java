package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.List;

public class SymbolsPreferences {

	private ArrayList<Symbol> symbols;

	
	// INIT
	public SymbolsPreferences()
	 {
		symbols = new ArrayList<>();
	 }
	

	public SymbolsPreferences(ArrayList<Symbol> symbols)
	 {
		this.symbols = symbols;
	 }

	public SymbolsPreferences(SymbolsPreferences other)
	 {
		symbols = new ArrayList<>();
		
		for (Symbol symbol : other.symbols) {
			this.symbols.add(new Symbol(symbol));
		}
	 }
	

	// SYMBOLS
	public ArrayList<Symbol> getSymbols() {
		return symbols;
	}
	
	public ArrayList<Symbol> getSymbols(String dstLangCode)
	 {
		ArrayList<Symbol> result = new ArrayList<Symbol>();
		for (Symbol symbol : this.symbols) {
			if (!symbol.getFilter().isAccept("", dstLangCode)) continue;
			result.add(symbol);
		}
		return result;
	 }

	public void addSymbol(String langCode, String name, String trigger, Character symbol)
	 {
		symbols.add(new Symbol(langCode, name, trigger, symbol));
	 }

	public void addSymbols(List<Symbol> symbolList)
	 {
		symbols.addAll(symbolList);
	 }
	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbols == null) ? 0 : symbols.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SymbolsPreferences other = (SymbolsPreferences) obj;
		if (symbols == null) {
			if (other.symbols != null) return false;
		} else if (!symbols.equals(other.symbols)) return false;
		return true;
	}

	
}
