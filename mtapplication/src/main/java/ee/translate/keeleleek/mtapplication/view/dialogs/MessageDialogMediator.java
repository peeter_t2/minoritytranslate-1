package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import javafx.fxml.FXML;


public abstract class MessageDialogMediator extends Mediator {

	final public static String NAME = "{603E312C-77A5-4662-888B-A2A1E5F559EE}";
	
	
	// INITIATION:
	public MessageDialogMediator() {
		super(NAME, null);
	}
	
	
	// IMPLEMENTATION:
	public abstract void setText(String message, String details);
	public abstract void close();
	
	
	// BUTTONS:
	@FXML
    public void onOkClick() {
		close();
    }
    
}
