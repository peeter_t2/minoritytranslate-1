package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.HashMap;

import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;

public class LanguagesPreferences {

	private ArrayList<String> langCodes;
	private HashMap<String, Display> displays;
	
	
	// INIT
	public LanguagesPreferences()
	 {
		this.langCodes = new ArrayList<>();
		this.displays = new HashMap<>();
	 }
	
	public LanguagesPreferences(ArrayList<String> langCodes, HashMap<String, Display> displays)
	 {
		this.langCodes = langCodes;
		this.displays = displays;
	 }

	public LanguagesPreferences(LanguagesPreferences languages)
	 {
		this.langCodes = new ArrayList<>(languages.langCodes);
		this.displays = new HashMap<>(languages.displays);
	 }
	

	// PREFERENCES
	public ArrayList<String> getLangCodes() {
		return langCodes;
	}
	
	public HashMap<String, Display> getDisplays() {
		return displays;
	}
	
	public Display getDisplay(String langCode) {
		Display display = displays.get(langCode);
		if (display == null) display = Display.NONE;
		return display;
	}
	
	
	public void addLangCode(String langCode) {
		langCodes.remove(langCode);
		langCodes.add(langCode);
	}

	public void removeLangCode(String langCode) {
		langCodes.remove(langCode);
		displays.remove(langCode);
	}
	
	public void changeDisplay(String langCode, Display display) {
		displays.put(langCode, display);
	}

	
	// FILTERING
	public String[] filterSrcLangCodes()
	 {
		ArrayList<String> result = new ArrayList<>();
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == Display.SOURCE) result.add(langCode);
		}
		return result.toArray(new String[result.size()]);
	 }

	public String[] filterDstLangCodes()
	 {
		ArrayList<String> result = new ArrayList<>();
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == Display.DESTINATION) result.add(langCode);
		}
		return result.toArray(new String[result.size()]);
	 }

	public String[] filterActiveLangCodes()
	 {
		ArrayList<String> result = new ArrayList<>();
		for (String langCode : this.langCodes) {
			if (displays.get(langCode) == Display.SOURCE || displays.get(langCode) == Display.DESTINATION) result.add(langCode);
		}
		return result.toArray(new String[result.size()]);
	 }

	
	
	// UTILITY
	@Override
	public int hashCode()
	 {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displays == null) ? 0 : displays.hashCode());
		result = prime * result + ((langCodes == null) ? 0 : langCodes.hashCode());
		return result;
	 }

	@Override
	public boolean equals(Object obj)
	 {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		LanguagesPreferences other = (LanguagesPreferences) obj;
		if (displays == null) {
			if (other.displays != null) return false;
		} else if (!displays.equals(other.displays)) return false;
		if (langCodes == null) {
			if (other.langCodes != null) return false;
		} else if (!langCodes.equals(other.langCodes)) return false;
		return true;
	 }
	
}
