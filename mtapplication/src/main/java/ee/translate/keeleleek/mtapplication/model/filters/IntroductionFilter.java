package ee.translate.keeleleek.mtapplication.model.filters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntroductionFilter implements Filter {
	
	public final static String NAME = "introduction";
	
	
	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public String filter(String text) {
		Pattern pattern = Pattern.compile(Pattern.quote("==") + "(.*?)" + Pattern.quote("=="));
	    Matcher matcher = pattern.matcher(text);
	    if(matcher.find()) {
	    	return text.substring(0, matcher.start());
	    }
		return text;
	}
	
}
