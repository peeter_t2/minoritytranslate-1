package ee.translate.keeleleek.mtapplication.controller.preferences;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class LoadPreferencesCommand extends SimpleCommand {

	public final static Charset ENCODING = StandardCharsets.UTF_8;
	
	public final static Path FILE_PATH = Paths.get(MinorityTranslate.ROOT_PATH + "/preferences.json");
	public final static String JAR_PATH = "/preferences.json";
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());

	
	@Override
	public void execute(INotification notification)
	 {
		// From file:
		String json;
		try {
			
			if (FILE_PATH.toFile().isFile()) {
				json = FileUtil.read(FILE_PATH);
			} else {
				json = FileUtil.readFromJar(JAR_PATH);
			}
			
		} catch (IOException e) {
			LOGGER.error("Failed to read preferences", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.preferences.load.failed"), Messages.getString("messages.preferences.load.failed.to").replaceFirst("#", FILE_PATH.toString()).replaceFirst("#",e.getMessage()));
			return;
		}
		
		Gson gson = new Gson();
		PreferencesProxy preferencesProxy = gson.fromJson(json, PreferencesProxy.class);

		getFacade().registerProxy(preferencesProxy);
	 }
	
}
