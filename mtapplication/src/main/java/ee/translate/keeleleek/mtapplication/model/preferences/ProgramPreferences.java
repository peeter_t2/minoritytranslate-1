package ee.translate.keeleleek.mtapplication.model.preferences;

import ee.translate.keeleleek.mtapplication.model.filters.FilesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.Filter;
import ee.translate.keeleleek.mtapplication.model.filters.IntroductionFilter;
import ee.translate.keeleleek.mtapplication.model.filters.ReferencesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.TemplatesFilter;


public class ProgramPreferences {

	public final static String[] FONT_SIZES = new String[]{"150%", "140%", "130%", "120%", "110%", "100%", "90%", "80%", "70%", "60%", "50%"};
	
	private Integer categoryDepth;
	private Boolean templatesFilter;
	private Boolean filesFilter;
	private Boolean introductionFilter;
	private Boolean referencesFilter;
	private String guiLangCode = "en";
	private Boolean autoLogin;
	private String fontSize = "100%";
	
	
	// INIT
	public ProgramPreferences()
	 {
		this.categoryDepth = 0;
		this.templatesFilter = false;
		this.filesFilter = false;
		this.introductionFilter = false;
		this.referencesFilter = false;
		this.guiLangCode = "en";
		this.autoLogin = true;
	 }

	public ProgramPreferences(ProgramPreferences preferences)
	 {
		this.categoryDepth = preferences.categoryDepth;
		this.templatesFilter = preferences.templatesFilter;
		this.filesFilter = preferences.filesFilter;
		this.introductionFilter = preferences.introductionFilter;
		this.referencesFilter = preferences.referencesFilter;
		this.guiLangCode = preferences.guiLangCode;
		this.autoLogin = preferences.autoLogin;
		this.fontSize = preferences.fontSize;
	 }
	

	// PREFERENCES
	public Integer getCategoryDepth() {
		return categoryDepth;
	}

	public void setCategoryDepth(Integer categoryDepth) {
		this.categoryDepth = categoryDepth;
	}
	
	public Boolean isTemplatesFilter() {
		return templatesFilter;
	}

	public void setTemplatesFilter(Boolean templatesFilter) {
		this.templatesFilter = templatesFilter;
	}
	
	public Boolean isFilesFilter() {
		return filesFilter;
	}
	
	public void setFilesFilter(Boolean filesFilter) {
		this.filesFilter = filesFilter;
	}

	public Boolean isIntroductionFilter() {
		return introductionFilter;
	}
	
	public void setIntroductionFilter(Boolean introductionFilter) {
		this.introductionFilter = introductionFilter;
	}
	
	public Boolean isReferencesFilter() {
		return referencesFilter;
	}

	public void setReferencesFilter(Boolean referencesFilter) {
		this.referencesFilter = referencesFilter;
	}
	
	public String getGUILangCode() {
		return guiLangCode;
	}

	public void setGuiLangCode(String guiLangCode) {
		this.guiLangCode = guiLangCode;
	}
	
	public Boolean isAutoLogin() {
		return autoLogin;
	}
	
	public String getFontSize() {
		return fontSize;
	}
	
	public void setFontSize(String interfaceFontSize) {
		this.fontSize = interfaceFontSize;
	}
	
	
	// FILTERS
	public Filter[] collectActiveFilters()
	 {
		int size = 0;
		
		if (templatesFilter) size++;
		if (filesFilter) size++;
		if (introductionFilter) size++;
		if (referencesFilter) size++;
		
		Filter[] result = new Filter[size];
		
		int i = 0;

		if (templatesFilter) {
			result[i] = new TemplatesFilter();
			i++;
		}

		if (filesFilter) {
			result[i] = new FilesFilter();
			i++;
		}

		if (introductionFilter) {
			result[i] = new IntroductionFilter();
			i++;
		}

		if (referencesFilter) {
			result[i] = new ReferencesFilter();
			i++;
		}
		
		return result;
	 }


	
	
	// UTILITY
	
	
}
