package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.common.ConfirmationInfo;
import ee.translate.keeleleek.mtapplication.common.ConfirmationInfo.ConfirmState;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;


public abstract class ConfirmationDialogMediator extends Mediator {

	final public static String NAME = "{D2386EEB-B1F7-4D2F-B18A-9AEAC249E128}";
	
	protected ConfirmationInfo info;
	
	
	// INITIATION:
	public ConfirmationDialogMediator() {
		super(NAME, null);
	}
	
	public void setup(ConfirmationInfo info) {
		this.info = info;
		setText(Messages.getString(info.getMessageKey()), Messages.getString(info.getDetailsKey()), Messages.getString(info.getYesKey()), Messages.getString(info.getNoKey()), Messages.getString(info.getCancelKey()));
	}
	
	
	// IMPLEMENTATION:
	public abstract void close();
	public abstract void setText(String message, String details, String yes, String no, String cancel);
	
	
	// BUTTONS:
	public void onYesClick() {
		info.setState(ConfirmState.YES);
		close();
    }

	public void onNoClick() {
		info.setState(ConfirmState.NO);
		close();
    }

	public void onCancelClick() {
		info.setState(ConfirmState.CANCEL);
		close();
    }

    
}
