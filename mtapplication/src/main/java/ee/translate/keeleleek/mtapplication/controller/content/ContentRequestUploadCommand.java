package ee.translate.keeleleek.mtapplication.controller.content;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class ContentRequestUploadCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Reference ref = (Reference) notification.getBody();
		
		String title = MinorityTranslateModel.content().getTitle(ref);
		String text = MinorityTranslateModel.content().getText(ref);
		
		if (title == null || text == null) return;
		if (text.isEmpty() && title.isEmpty()) return;
		
		if (title.isEmpty()) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.upload.missing.title"), Messages.getString("messages.upload.missing.title.details"));
			return;
		}
		
		MinorityTranslateModel.usage().createUsage(ref);
		
		MinorityTranslateModel.content().requestUpload(ref);
	 }
	
}
