package ee.translate.keeleleek.mtapplication.view.javafx.elements;

import ee.translate.keeleleek.mtapplication.view.elements.NotesMediator;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.control.TextArea;


public class NotesMediatorFX extends NotesMediator {

	private TextArea notesEdit;
	
	boolean feedback = false;
	
	
	// INIT
	public NotesMediatorFX(TextArea notesEdit) {
		this.notesEdit = notesEdit;
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		notesEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable)
			 {
				if (feedback) return;
				onTextEdited();
			 }
		});
	 }
	
	
	// INHERIT (CONTENT)
	@Override
	public String getText() {
		return notesEdit.getText();
	}
	
	@Override
	public void setText(String text) {
		feedback = true;
		notesEdit.setText(text);
		feedback = false;
	}
	
	
}
