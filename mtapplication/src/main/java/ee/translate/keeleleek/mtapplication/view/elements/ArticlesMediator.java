package ee.translate.keeleleek.mtapplication.view.elements;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;


public abstract class ArticlesMediator extends Mediator {

	public final static String NAME = "{6ECEFA93-8429-417C-9543-FBF1FE91D3EB}";
	
	
	boolean feedback = false;
	
	
	// INIT:
	public ArticlesMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}
	

	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		{
			Notifications.ENGINE_SELECTED_ARTICLE_CHANGED,
			Notifications.ENGINE_SELECTABLE_ITEMS_CHANGED,
			Notifications.SESSION_LOADED,
			Notifications.ENGINE_ARTICLE_TITLE_CHANGED,
			Notifications.ENGINE_ARTICLE_TEXT_CHANGED,
			Notifications.ENGINE_ARTICLE_STATUS_CHANGED
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		feedback = true;
		
		String[] selectionItems;
		
		switch (notification.getName()) {

		case Notifications.ENGINE_SELECTED_ARTICLE_CHANGED:
			setSelectedIndex(MinorityTranslateModel.content().findSelecedIndex());
			break;

		case Notifications.ENGINE_SELECTABLE_ITEMS_CHANGED:
			selectionItems = (String[]) notification.getBody();
			setList(selectionItems);
			setSelectedIndex(MinorityTranslateModel.content().findSelecedIndex());
			break;

		case Notifications.SESSION_LOADED:
			selectionItems = MinorityTranslateModel.content().compileSelectionItems();
			setList(selectionItems);
			setSelectedIndex(MinorityTranslateModel.content().findSelecedIndex());
			break;

		case Notifications.ENGINE_ARTICLE_TITLE_CHANGED:
		case Notifications.ENGINE_ARTICLE_TEXT_CHANGED:
		case Notifications.ENGINE_ARTICLE_STATUS_CHANGED:
			Reference ref = (Reference) notification.getBody();
			int i = MinorityTranslateModel.content().findIndex(ref.getQid());
			if (i != -1) fetchArticon(i);
			break;
			
		default:
			break;
		}
		
		feedback = false;
	 }
	
	
	// INHERIT
	protected abstract void setList(String[] items);
	protected abstract void fetchArticon(int i);
	protected abstract void setSelectedIndex(int i);
	
	
	// EVENTS
	protected void selectedIndexChanged(int i) {
		if (feedback) return;
		String qid = MinorityTranslateModel.content().findQid(i);
		sendNotification(Notifications.ENGINE_SELECT_ARTICLE_QID, qid);
	}
	
	
}
