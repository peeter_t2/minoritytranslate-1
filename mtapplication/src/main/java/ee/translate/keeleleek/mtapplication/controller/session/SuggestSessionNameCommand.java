package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class SuggestSessionNameCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String name = (String) notification.getBody();
		String oldName = MinorityTranslateModel.session().getName();
		if (oldName != null && !oldName.isEmpty()) return;
		MinorityTranslateModel.session().setName(name);
	 }
	
}
