package ee.translate.keeleleek.mtapplication.model.content;

public class WikiReference {

	private String langCode;
	private Namespace namespace;
	private String wikiTitle;
	

	// INIT
	public WikiReference(String langCode, Namespace namespace, String wikiTitle) throws NullPointerException
	 {
		if (langCode == null) throw new NullPointerException("langCode cannot be null");
		if (wikiTitle == null) wikiTitle = "";
		if (namespace == null) throw new NullPointerException("namespace cannot be null");
		
		this.langCode = langCode;
		this.namespace = namespace;
		this.wikiTitle = wikiTitle;
	 }

	
	// VALUES
	public String getLangCode() {
		return langCode;
	}

	public String getWikiTitle() {
		return wikiTitle;
	}

	public String getNamespacedWikiTitle() {
		return namespace.getPrefix() + wikiTitle;
	}
	
	public Namespace getNamespace() {
		return namespace;
	}


	// WORKINGS
	@Override
	public String toString() {
		return langCode + '&' + namespace + ":" + wikiTitle;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((langCode == null) ? 0 : langCode.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result + ((wikiTitle == null) ? 0 : wikiTitle.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WikiReference other = (WikiReference) obj;
		if (langCode == null) {
			if (other.langCode != null)
				return false;
		} else if (!langCode.equals(other.langCode))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		if (wikiTitle == null) {
			if (other.wikiTitle != null)
				return false;
		} else if (!wikiTitle.equals(other.wikiTitle))
			return false;
		return true;
	}
	
}
