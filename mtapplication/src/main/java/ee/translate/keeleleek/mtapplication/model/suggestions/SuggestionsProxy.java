package ee.translate.keeleleek.mtapplication.model.suggestions;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.actions.SearchAction;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.ContentUtil;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.suggestions.SearchRequest.RequestType;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class SuggestionsProxy extends Proxy {

	public final static String NAME = "{ED2197FC-CBB4-4FC2-82C7-187D67725FD2}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(SuggestionsProxy.class);

	int sguard = 0;
	
	
	// INIT
	public SuggestionsProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		
	 }

	
	// SUGGESTIONS
	public void searchSuggestions(final SearchRequest request)
	 {
		this.sguard++;

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run()
			 {
				try {
					
					int guard = sguard;
					
					String langCode = request.getLangCode();
					String title = request.getTitle();
					String wikiTitle = WikisProxy.wikiTitle(title, langCode);
					RequestType type = request.getType();
					
					if (langCode == null || wikiTitle.isEmpty()) {
						MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SUGGESTIONS_CHANGED, new SearchResponse(request, new String[0]));
						return;
					}
					
					MediaWikiBot bot;
					SearchAction action;
					
					// fetch results
					String[] results = new String[0];
					switch (type) {
					case ARTICLES:
					case ARTICLE_LINKS:
						bot = MinorityTranslateModel.bots().fetchBot(langCode);
						action = new SearchAction(wikiTitle, MediaWiki.NS_MAIN);
						bot.getPerformedAction(action);
						results = action.getResults();
						break;
						
					case CATEGORIES:
						bot = MinorityTranslateModel.bots().fetchBot(langCode);
						action = new SearchAction(wikiTitle, MediaWiki.NS_CATEGORY);
						bot.getPerformedAction(action);
						results = action.getResults();
						break;
						
					case LOADED_ARTICLES:
						results = ContentUtil.searchTitles(MinorityTranslateModel.content(), title, langCode);
						break;

					default:
						break;
					}
					
					// trim
					WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
					switch (wikiType) {
					case REGULAR:
						for (int i = 0; i < results.length; i++) {
							int indexColon = results[i].indexOf(':');
							if (indexColon != -1) results[i] = results[i].substring(indexColon + 1);
						}
						break;
					
					case INCUBATOR:
						for (int i = 0; i < results.length; i++) {
							int indexColon = results[i].indexOf(':');
							if (indexColon != -1) results[i] = results[i].substring(indexColon + 1);
							String prefix = WikisProxy.prefix(langCode);
							results[i] = WikisProxy.deprefixTitle(results[i], prefix);
						}
						break;
						
					default:
						break;
					}
					
					// new request
					if (guard != sguard) return;
					
					// finish
					MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SUGGESTIONS_CHANGED, new SearchResponse(request, results));
					
				} catch (IllegalStateException e) {
					LOGGER.warn("Failed to fetch title suggestions: " + e.getMessage() + "!");
				}
			 }
		});
		thread.start();
	 }
	
	
}
