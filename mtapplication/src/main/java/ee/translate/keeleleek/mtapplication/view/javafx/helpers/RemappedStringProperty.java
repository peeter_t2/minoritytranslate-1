package ee.translate.keeleleek.mtapplication.view.javafx.helpers;

import java.util.HashMap;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class RemappedStringProperty extends SimpleStringProperty {

	public RemappedStringProperty(IntegerProperty integerProperty, final HashMap<Integer, String> mappings)
	 {
		super(mappings.get(integerProperty.get()));
		
		integerProperty.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				set(mappings.get(newValue));
			}
		});
	 }
	
	
}
