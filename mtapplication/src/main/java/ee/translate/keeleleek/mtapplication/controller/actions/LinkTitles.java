package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import net.sourceforge.jwbf.core.actions.Post;
import net.sourceforge.jwbf.core.actions.util.ActionException;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.ApiRequestBuilder;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.editing.GetApiToken;
import net.sourceforge.jwbf.mediawiki.actions.editing.GetApiToken.Intoken;
import net.sourceforge.jwbf.mediawiki.actions.editing.GetApiToken.TokenResponse;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class LinkTitles extends MWAction {

	private HttpAction tokenHTTPAction = null;
	private HttpAction linkHTTPAction = null;
	
	private String fromLangCode; 
	private String fromTitle; 
	private String toLangCode; 
	private String toTitle; 
	
	private GetApiToken apiTokenAction = null;
	
	
	public LinkTitles(String srcLangCode, String srcTitle, String dstLangCode, String dstTitle)
	 {
		this.fromLangCode = srcLangCode;
		this.fromTitle = srcTitle;
		this.toLangCode = dstLangCode;
		this.toTitle = dstTitle;
		
		apiTokenAction = new GetApiToken(Intoken.EDIT, srcTitle);
		tokenHTTPAction = apiTokenAction.popAction();
	 }

	private Post createLinkHTTPAction(TokenResponse tokenResponse)
	 {
		Post action = new ApiRequestBuilder()
			.action("wbsetsitelink")
			.postParam(tokenResponse.token())
			.postParam("site", fromLangCode.replace('-', '_') + "wiki")
			.postParam("title", fromTitle)
			.postParam("linksite", toLangCode.replace('-', '_') + "wiki")
			.postParam("linktitle", toTitle)
			.postParam("format", "xml")
			.buildPost();
		
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		if (tokenHTTPAction != null) return tokenHTTPAction;
		if (linkHTTPAction != null) return linkHTTPAction;
		return null;
	}

	@Override
	public boolean hasMoreMessages() {
		return tokenHTTPAction != null || linkHTTPAction != null;
	}

	@Override
	public String processReturningText(String xml, HttpAction action)
	 {
		Logger.getLogger(getClass()).info("LinkTitles response: " + xml);
		
		if (xml.contains("error")) {
			
			Pattern pError = Pattern.compile("<error(.*?)>", Pattern.DOTALL | Pattern.MULTILINE);
			Matcher mError = pError.matcher(xml);
			
			if (mError.find()) {
				
				String error = mError.group(0);
				
				Pattern pCode = Pattern.compile("code=\"(.*?)\"", Pattern.DOTALL | Pattern.MULTILINE);
				Matcher mCode = pCode.matcher(error);

				if (mCode.find()) {
				
					String code = MediaWiki.htmlUnescape(mCode.group(1));

					Pattern pInfo = Pattern.compile("info=\"(.*?)\"", Pattern.DOTALL | Pattern.MULTILINE);
					Matcher mInfo = pInfo.matcher(error);
					
					String info = "";
					
					if (mInfo.find()) info = MediaWiki.htmlUnescape(mInfo.group(1)); 
					
					String message = code + ": " + info;
					
					LoggerFactory.getLogger(getClass()).error("Linking failed: " + message);
					throw new ActionException(message);
					
				}
				
				
			}
			
			LoggerFactory.getLogger(getClass()).error("Linking failed: " + xml);
			
			throw new ActionException("Unhandled message");
			
		}
		
		// token
		if (action == tokenHTTPAction) {
			apiTokenAction.processReturningText(xml, action);
			linkHTTPAction = createLinkHTTPAction(apiTokenAction.get());
			tokenHTTPAction = null;
		}
		
		// link
		else if (action == linkHTTPAction) {
			System.out.println("LINK RESULT=" + xml);
			linkHTTPAction = null;
		}
		
		return super.processReturningText(xml, action);
	 }

	
}
