package ee.translate.keeleleek.mtapplication.model.content;

public class Reference {

	private String qid;
	private String langCode;
	

	// INIT
	public Reference(String qid, String langCode) throws NullPointerException
	 {
		if (qid == null) throw new NullPointerException("qid cannot be null");
		if (langCode == null) throw new NullPointerException("langCode cannot be null");
		
		this.qid = qid;
		this.langCode = langCode;
	 }

	
	// VALUES
	public String getQid() {
		return qid;
	}
	
	public String getLangCode() {
		return langCode;
	}
	

	// WORKINGS
	@Override
	public String toString() {
		return qid + '&' + langCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((langCode == null) ? 0 : langCode.hashCode());
		result = prime * result + ((qid == null) ? 0 : qid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	 {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Reference other = (Reference) obj;
		if (langCode == null) {
			if (other.langCode != null) return false;
		} else if (!langCode.equals(other.langCode)) return false;
		if (qid == null) {
			if (other.qid != null) return false;
		} else if (!qid.equals(other.qid)) return false;
		return true;
	 }
	
	public static boolean equals(Reference ref1, Reference ref2)
	 {
		if (ref1 == null) return ref2 == null;
		return ref1.equals(ref2);
	 }
	
	
}
