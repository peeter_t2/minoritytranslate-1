package ee.translate.keeleleek.mtapplication.model.bots;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.HashMap;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.actions.Logout;
import ee.translate.keeleleek.mtapplication.controller.login.Protector;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.preferences.CredentialsPreferences;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class LoginProxy extends Proxy {
	
	public final static String NAME = "{57FA1862-789C-447D-ABE3-A08D0FA52B29}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(LoginProxy.class);

	private HashMap<String, MediaWikiBot> bots = new HashMap<>();
	private MediaWikiBot wikidataBot;
	private MediaWikiBot incubatorBot;
	
	private String currentUsername = null;
	private String currentPassword = null;
	
	private boolean busy = false;
	
	
	// INIT
	public LoginProxy() {
		super(NAME);
	}
	
	
	// CREDENTIALS
	public String getUsername() {
		return currentUsername;
	}
	

	// BOTS
	public MediaWikiBot fetchBot(final String langCode)
	 {
		MediaWikiBot bot = bots.get(langCode);
		if (bot == null) {
			
			WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
			switch (wikiType) {
			case REGULAR:
				bot = new MediaWikiBot("https://" + langCode + ".wikipedia.org/w/");
				break;
				
			case INCUBATOR:
				bot = getIncubatorBot();
				break;

			default:
				LOGGER.error("Failed to fetch bot, because the wikiType is missing for langCode " + langCode);
				break;
			}
			
			bots.put(langCode, bot);
			
			if (currentUsername != null && currentPassword != null) {
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run()
					 {
						try {
							MediaWikiBot bot = bots.get(langCode);
							if (bot == null) return;
							LoggerFactory.getLogger(getClass()).info("Logging in to " + langCode + " wiki");
							bot.login(currentUsername, currentPassword);
							MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.LOGIN_STATUS_CHANGED);
						} catch (Exception e) { }
					 }
				});
				thread.start();
			}
			
		}
		return bot;
	 }

	public void unloadBot(String langCode) {
		bots.remove(langCode);
	}
	
	public MediaWikiBot getWikidataBot() {
		if (wikidataBot == null) {
			wikidataBot = new MediaWikiBot("https://www.wikidata.org/w/");
		}
		return wikidataBot;
	}

	public MediaWikiBot getIncubatorBot() {
		if (incubatorBot == null) {
			incubatorBot = new MediaWikiBot("https://incubator.wikimedia.org/w/");
		}
		return incubatorBot;
	}
	
	public MediaWikiBot[] findToBots()
	 {
		String[] dstLangCodes = MinorityTranslateModel.preferences().getDstLangCodes();

		MediaWikiBot[] bots = new MediaWikiBot[dstLangCodes.length + 1];
		for (int i = 0; i < dstLangCodes.length; i++) {
			bots[i] = fetchBot(dstLangCodes[i]);
		}
		bots[dstLangCodes.length] = getWikidataBot();
		
		return bots;
	 }

	public boolean isLoggedIn()
	 {
		String[] dstLangCodes = MinorityTranslateModel.preferences().getDstLangCodes();

		for (int i = 0; i < dstLangCodes.length; i++) {
			if (!fetchBot(dstLangCodes[i]).isLoggedIn()) return false;
		}
		
		return true;
	 }
	
	
	// LOGGING
	private void logInBlocking(String username, String password, boolean automatic)
	 {
		if (currentUsername != null && !currentUsername.equals(username)) logOutBlocking(false);
		
		busy = true;
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.LOGIN_BUSY_CHANGED, busy);
		
		boolean failed = false;
		
		MediaWikiBot[] bots = MinorityTranslateModel.bots().fetchToBots();
		for (int i = 0; i < bots.length; i++) {
			
			try {
				
				if (bots[i].isLoggedIn()) continue;
				
				LoggerFactory.getLogger(getClass()).info("Logging in to " + bots[i].getWikiType());
				bots[i].login(username, password);
				
			} catch (Exception e) {
				
				if (new String("Wrong Password").equals(e.getMessage())) {
					MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ERROR_MESSAGE, Messages.getString("messages.login.error.wrong.password"));
					break;
				}
				
				failed = true;
				
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ERROR_MESSAGE, Messages.getString("messages.login.error"), e.getMessage());
				break;
				
			}
			
		}
		
		busy = false;
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.LOGIN_BUSY_CHANGED, busy);

		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.LOGIN_STATUS_CHANGED);
		
		if (!failed) {
			
			currentUsername = username;
			currentPassword = password;
			
			if (automatic) {
				CredentialsPreferences credentials;
				try {
					credentials = new CredentialsPreferences(username, Protector.encrypt(password, "preferences.json"));
					MinorityTranslateModel.preferences().changeCredentials(credentials);
				} catch (UnsupportedEncodingException | GeneralSecurityException e) {
					LOGGER.error("Failed to store!");
					return;
				}
			}
			
		}
	 }
	
	public void logIn(final String username, final String password, final boolean automatic)
	 {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				logInBlocking(username, password, automatic);
			}
		});
		thread.start();
	 }

	public void logIn()
	 {
		if (isLoggedIn()) return;
		
		String username = MinorityTranslateModel.preferences().credentials().getUsername();
		String password = MinorityTranslateModel.preferences().credentials().getPassword();
		if (username.isEmpty() || password.isEmpty()) return;
		
		try {
			password = Protector.decrypt(password, "preferences.json");
		} catch (Exception e) {
			LOGGER.error("Failed to retrieve!" + username);
			return;
		}

		logIn(username, password, false);
	 }
	
	private void logOutBlocking(boolean automatic)
	 {
		String[] langCodes = MinorityTranslateModel.preferences().getDstLangCodes();
		
		for (String langCode : langCodes) {
			
			MediaWikiBot bot = bots.get(langCode);
			if (bot == null || !bot.isLoggedIn()) continue;
			
			LOGGER.info("Logging out " + langCode);
			bot.getPerformedAction(new Logout());
			bots.remove(langCode);
			
		}
		
		if (automatic) MinorityTranslateModel.preferences().changeCredentials(new CredentialsPreferences());

		currentUsername = null;
		currentPassword = null;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.LOGIN_STATUS_CHANGED);
	 }
	
	public void logOut()
	 {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run()
			 {
				logOutBlocking(true);
			 }
		});
		thread.start();
	 }

	public boolean isBusy()
	 {
		return busy;
	 }
	
	
}
