package ee.translate.keeleleek.mtapplication.controller.suggestions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.ContentUtil;
import ee.translate.keeleleek.mtapplication.view.suggestions.Suggestable;

public class SuggestLoadedTitleCommand extends SimpleCommand {

	private final int RESULT_COUNT = 10;
	
	
	@Override
	public void execute(INotification notification)
	 {
		Suggestable suggestable = (Suggestable) notification.getBody();
		String searchTerm = suggestable.getSearchTerm();
		String langCode = notification.getType();
		if (langCode == null || langCode.length() == 0) {
			suggestable.suggest(new String[0]);
			return;
		}

		List<String> titles = ContentUtil.extractTitles(MinorityTranslateModel.content(), langCode);
		Collections.sort(titles);
		
		String search = searchTerm.toLowerCase();
		ArrayList<String> results = new ArrayList<>();
		for (String title : titles) {
			if (title.length() == 0) continue;
			if (title.toLowerCase().startsWith(search)) results.add(title);
			if (results.size() >= RESULT_COUNT) break;
		}
		
		suggestable.suggest(results.toArray(new String[results.size()]));
	 }
	
}
