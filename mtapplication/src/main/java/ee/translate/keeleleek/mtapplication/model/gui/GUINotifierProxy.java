package ee.translate.keeleleek.mtapplication.model.gui;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

public abstract class GUINotifierProxy extends Proxy {

	public final static String NAME = "{740BCF30-F33E-444A-B1C8-8DF7572F0A3C}";
	
	
	// INIT:
	public GUINotifierProxy() {
		super(NAME, null);
	}

	
	// NOTIFICATIONS:
	/**
	 * Sends a thread safe notification.
	 * 
	 * @param name notification name
	 * @param body notification body
	 */
	public abstract void sendNotificationThreadSafe(final String name, final Object body, final String type);
	
	/**
	 * Sends a thread safe notification.
	 * 
	 * @param name notification name
	 * @param body notification body
	 */
	public abstract void sendNotificationThreadSafe(final String name, final Object body);

	/**
	 * Sends a thread safe notification.
	 * 
	 * @param name notification name
	 */
	public abstract void sendNotificationThreadSafe(final String name);

	
	/**
	 * Runs thread safe.
	 * 
	 * @param runnable runnable to run
	 */
	public abstract void doThreadSafe(Runnable runnable);
	
}
