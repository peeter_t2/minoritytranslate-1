package ee.translate.keeleleek.mtapplication.model.application;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class MessageVersions {

	private HashMap<String, String> versions = null;
	
	
	public MessageVersions()
	 {
		
	 }
	
	
	public String getLangVersion(String langCode) {
		return versions.get(langCode);
	}
	
	public String setLangVersion(String langCode, String version) {
		return versions.put(langCode, version);
	}
	
	public Set<String> getLangCodes() {
		return new HashSet<>(versions.keySet());
	}
	
	
}
