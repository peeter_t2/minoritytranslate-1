package ee.translate.keeleleek.mtapplication.view.pages;

public interface PreferencesPage<T> {

	public void open(T preferences);
	public T close();
	
}
