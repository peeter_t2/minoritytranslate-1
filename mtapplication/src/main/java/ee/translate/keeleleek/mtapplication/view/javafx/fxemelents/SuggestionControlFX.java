package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.content.WikiRequest;
import ee.translate.keeleleek.mtapplication.model.lists.ListSuggestion;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SuggestionControlFX implements SuggestionFX {

	private final static String TRANSLATED = "\u2714";
	private final static String NOT_TRANSLATED = "\u2718";
	
	private final ListSuggestion suggestion;

	private StringProperty title;
	private StringProperty type;
	
	
	public SuggestionControlFX(ListSuggestion suggestion)
	 {
		this.suggestion = suggestion;

		this.title = new SimpleStringProperty(suggestion.getTitle());
		this.type = new SimpleStringProperty(suggestion.getTypeList().toString());
	 }
	
	
	public ListSuggestion getSuggestion() {
		return suggestion;
	}
	
	public WikiRequest getRequest() {
		return suggestion.getRequest();
	}
	
	public StringProperty title() {
		return title;
	}

	public StringProperty type() {
		return type;
	}

	public StringProperty translated(String langCode) {
		if (suggestion.isTranslated(langCode)) return new SimpleStringProperty(TRANSLATED);
		else return new SimpleStringProperty(NOT_TRANSLATED);
	}

	
	@Override
	public String toString() {
		return suggestion.getTitle();
	}
	
	
}
