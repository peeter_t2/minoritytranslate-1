package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProcessingUtil {

	public static List<String> collectHTML(String find, String content)
	 {
		String rgxFind = find;
		
		rgxFind = "\\Q" + rgxFind; // treat as one (start)
		rgxFind = rgxFind + "\\E"; // treat as one (end)
		
		rgxFind = rgxFind.replace("#", "\\E(.*?)\\Q"); // capture params
		rgxFind = rgxFind.replace("~", "\\E(?:.*?)\\Q"); // anything between

		rgxFind = rgxFind.replace("\\Q\\E", ""); // clean
		
		Pattern p = Pattern.compile(rgxFind, Pattern.DOTALL);
		Matcher m = p.matcher(content);
		
		ArrayList<String> arguments = new ArrayList<>();
		while (m.find()) {
			for (int i = 0; i < m.groupCount(); i++) {
				arguments.add(m.group(1 + i));
			}
		}
		
		return arguments;
	 }
	
	public static String replaceHTML(String opening, String element, String closing, List<String> arguments)
	 {
		StringBuilder result = new StringBuilder();
		
		result.append(opening);
		
		int args = count(element, "#");
		if (args == 0) return element;
		
		int k = 0;
		while (k < arguments.size()) {
			String elem = element;
			for (int i = 0; i < args; i++) {
				elem = elem.replaceFirst(Pattern.quote("#"), arguments.get(k));
				k++;
				if (k >= arguments.size()) break;
			}
			result.append(elem);
		}
		
		result.append(closing);
		
		return result.toString();
	 }
	
	// HELPERS
	private static int count(String str, String substr)
	 {
		int lastIndex = 0;
		int count = 0;

		while(lastIndex != -1){

		       lastIndex = str.indexOf(substr,lastIndex);

		       if( lastIndex != -1){
		             count ++;
		             lastIndex+= substr.length();
		      }
		       
		}
		
		return count;
	 }
	
}
