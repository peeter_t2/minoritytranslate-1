package ee.translate.keeleleek.mtapplication.model.content;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.controller.actions.CategoryMembersAction;
import ee.translate.keeleleek.mtapplication.controller.actions.EntitiesAction;
import ee.translate.keeleleek.mtapplication.controller.actions.QIDEntitiesAction;
import ee.translate.keeleleek.mtapplication.controller.actions.RedirectsAction;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public abstract class ContentExpanderProxy extends Proxy {

	transient protected LinkedHashSet<WikiReference> articles = new LinkedHashSet<>();
	transient protected LinkedHashSet<WikiSubReference> categories = new LinkedHashSet<>();
	transient protected LinkedHashSet<WikiReference> redirected = new LinkedHashSet<>();
	transient protected LinkedHashSet<String> qids = new LinkedHashSet<>();
	transient protected LinkedHashSet<WikiRequest> expanded = new LinkedHashSet<>();
	
	transient private String[] langCodes = new String[]{};
	
	transient protected int guard = 0;
	
	
	// INIT
	public ContentExpanderProxy(String name) {
		super(name);
	}
	
	protected void work()
	 {
		workArticles();
		
		workCategories();

		workLangCodes();
		
		workQIDs();
		
		workExpand();
		
		HashSet<WikiRequest> expanded = this.expanded;
		this.expanded = new LinkedHashSet<>();
		workConclude(expanded);
	 }
	
	protected void workArticles()
	 {
		int qguard = guard;
		
		if (articles.size() == 0) return;
		
		Set<String> langCodes = ContentUtil.findLangCodes(this.articles);
		for (String langCode : langCodes) {
			
			// taking from initial
			Collection<WikiReference> requests = ContentUtil.takeRequests(langCode, this.articles, RedirectsAction.MAX_TITLE_COUNT);
			if (requests.size() == 0) continue;
			
			// finding redirects/normalisations
			MediaWikiBot langBot = MinorityTranslateModel.bots().fetchBot(langCode);
			RedirectsAction action = new RedirectsAction(ContentUtil.extractTitles(requests));
			langBot.getPerformedAction(action);
			
			// putting to redirected
			for (WikiReference request : requests) {
				String newTitle = action.findRedirect(request.getNamespacedWikiTitle());
				WikiReference newRequest = new WikiReference(request.getLangCode(), request.getNamespace(), newTitle);
				if (qguard == guard) redirected.add(newRequest);
			}
			
		}
		
	 }
	
	protected void workCategories()
	 {
		if (categories.size() == 0) return;
		
		int qguard = guard;
		
		Set<String> langCodes = ContentUtil.findLangCodes(this.categories);
		for (String langCode : langCodes) {
			
			// taking from initial
			WikiSubReference initRequest = ContentUtil.takeSubrequest(langCode, this.categories);
			if (initRequest == null) continue;
			String wikiTitle = initRequest.getWikiTitle();
			
			// finding members
			MediaWikiBot langBot = MinorityTranslateModel.bots().fetchBot(langCode);
			CategoryMembersAction action = new CategoryMembersAction(wikiTitle);
			CategoryMembersAction next = action;
			while (next != null) {
				action = next;
				langBot.getPerformedAction(action);
				next = action.next();
			}

			// putting articles
			if (initRequest.isAddArticles()) {
				
				List<String> articles = action.getArticles();
				for (String article : articles) {
					WikiReference request = new WikiReference(langCode, Namespace.ARTICLE, article);
					if (qguard == guard) this.articles.add(request);
				}
				
			}

			// putting articles
			if (initRequest.isAddCategories()) {
				
				List<String> categories = action.getCategories();
				for (String category : categories) {
					WikiReference request = new WikiReference(langCode, Namespace.CATEGORY, category);
					if (qguard == guard) this.articles.add(request);
				}
				
			}

			// depth check
			Integer depth = initRequest.getDepth();
			if (depth >= MinorityTranslateModel.preferences().program().getCategoryDepth()) continue;
			
			// putting categories
			List<String> categories = action.getCategories();
			for (String category : categories) {
				WikiSubReference nextRequest = new WikiSubReference(initRequest, category, depth + 1);
				if (qguard == guard) {
					this.categories.add(nextRequest);
				}
			}
			
		}
	 }
	
	protected void workLangCodes()
	 {
		String[] langCodes = MinorityTranslateModel.preferences().getLangCodes();
		
		if (Arrays.equals(this.langCodes, langCodes)) return;
		
		this.langCodes = langCodes;
		
		// expand again
		expandAgain();
	 }

	protected void workQIDs()
	 {
		int qguard = guard;
		
		if (qids.size() == 0) return;
		
		// taking from qids
		Collection<String> requests = ContentUtil.takeStringRequests(this.qids, QIDEntitiesAction.MAX_QID_COUNT);
		
		//if (requests.size() == 0) continue;
		
		// finding qid and langlinks
		MediaWikiBot wdataBot = MinorityTranslateModel.bots().getWikidataBot();
		QIDEntitiesAction action = new QIDEntitiesAction(requests, langCodes);
		wdataBot.getPerformedAction(action);
		
		for (String qid : requests) {
			
			// find base request
			WikiReference request = null;
			for (String langCode : langCodes) {
				String title = action.findLangtitle(qid, langCode);
				if (title != null) {
					request = new WikiReference(langCode, Namespace.ARTICLE, title);
					break;
				}
			}
			if (request == null) continue;

			HashMap<String, String> langlinks = action.findLangtitles(qid);
			
			QidMeta qidMeta = new QidMeta(qid, langlinks);
			WikiRequest expandedRequest = new WikiRequest(request, qid, ContentUtil.extractAllArticleRequests(langlinks, langCodes), qidMeta);
			if (qguard == guard) expanded.add(expandedRequest);
			
		}
		
	 }
	
	protected void workExpand()
	 {
		if (redirected.size() == 0) return;
		
		int qguard = guard;
		
		Set<String> langCodes = ContentUtil.findLangCodes(this.redirected);
		String[] allLangCodes = this.langCodes;
		for (String langCode : langCodes) {
			
			// taking from redirected
			Collection<WikiReference> requests = ContentUtil.takeRequests(langCode, this.redirected, EntitiesAction.MAX_TITLE_COUNT);
			
			if (requests.size() == 0) continue;
			
			// finding qid and langlinks
			MediaWikiBot wdataBot = MinorityTranslateModel.bots().getWikidataBot();
			EntitiesAction action = new EntitiesAction(langCode, ContentUtil.extractTitles(requests), allLangCodes);
			wdataBot.getPerformedAction(action);
			
			// putting to expanded
			for (WikiReference request : requests) {
				
				String title = request.getWikiTitle();
				String qid = action.findQid(title);
				HashMap<String, String> langlinks = action.findLangtitles(qid);
				
				if (qid == null) {
					qid = createQid(request.getLangCode(), request.getWikiTitle());
					langlinks = new HashMap<>();
					langlinks.put(request.getLangCode(), request.getWikiTitle());
				}
				
				QidMeta qidMeta = new QidMeta(qid, langlinks);
				WikiRequest expandedRequest = new WikiRequest(request, qid, ContentUtil.extractAllArticleRequests(langlinks, allLangCodes), qidMeta);
				if (qguard == guard) expanded.add(expandedRequest);
				
			}
			
		}
	 }

	protected abstract void workConclude(HashSet<WikiRequest> expanded);
	
	
	// CANCEL
	protected void cancel()
	 {
		this.guard++;
		articles.clear();
		categories.clear();
		redirected.clear();
		qids.clear();
		expanded.clear();
	 }
	
	
	// ERROR HANDLING
	public static String createQid(String langCode, String title) {
		return "_W" + langCode + title + "_";
	}
	

	// WORK HELPERS
	private void expandAgain()
	 {
		Set<WikiReference> origRequests = ContentUtil.extractOrigRequests(MinorityTranslateModel.content());
		
		redirected.addAll(origRequests);
	 }
	
	
}
