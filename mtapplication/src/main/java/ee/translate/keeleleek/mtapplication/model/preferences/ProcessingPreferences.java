package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;
import ee.translate.keeleleek.mtapplication.model.processing.RegexReplace;
import ee.translate.keeleleek.mtapplication.model.processing.processers.RegexReplaceProcesser;

public class ProcessingPreferences {

	private ArrayList<CollectEntry> collects = new ArrayList<>();
	private ArrayList<ReplaceEntry> replaces = new ArrayList<>();

	
	// INIT
	public ProcessingPreferences()
	 {
		this.collects = new ArrayList<>();
		this.replaces = new ArrayList<>();
	 }
	
	public ProcessingPreferences(ArrayList<CollectEntry> collects, ArrayList<ReplaceEntry> replaces)
	 {
		this.collects = collects;
		this.replaces = replaces;
	 }

	public ProcessingPreferences(ProcessingPreferences other)
	 {
		ArrayList<CollectEntry> otherCollects = other.getCollects();
		this.collects = new ArrayList<>(otherCollects.size());
		for (int i = 0; i < otherCollects.size(); i++) {
			this.collects.add(new CollectEntry(otherCollects.get(i)));
		}
		
		ArrayList<ReplaceEntry> otherReplaces = other.getReplaces();
		this.replaces = new ArrayList<>(otherReplaces.size());
		for (int i = 0; i < otherReplaces.size(); i++) {
			this.replaces.add(new ReplaceEntry(otherReplaces.get(i)));
		}
	 }
	
	public static ProcessingPreferences create()
	 {
		ProcessingPreferences preferences = new ProcessingPreferences();
		preferences.replaces.add(new ReplaceEntry(new EntryFilter(), RegexReplaceProcesser.SRC_TITLE_KEY, RegexReplaceProcesser.DST_TITLE_KEY));
		preferences.replaces.add(new ReplaceEntry(new EntryFilter(), RegexReplaceProcesser.SRC_TITLE_KEY_LOWER_CASE, RegexReplaceProcesser.DST_TITLE_KEY_LOWER_CASE));
		preferences.replaces.add(new ReplaceEntry(new EntryFilter("en", "et"), "hypothesis", "hüpotees"));
		return preferences;
	 }
	

	// COLLECTS
	public ArrayList<CollectEntry> getCollects() {
		return collects;
	}

	public CollectEntry createCollect(int i)
	 {
		CollectEntry entry = new CollectEntry();
		collects.add(i, entry);
		return entry;
	 }

	public CollectEntry removeCollect(int i)
	 {
		return collects.remove(i);
	 }

	public void switchCollect(int i, int j)
	 {
		CollectEntry collect = collects.get(i);
		collects.set(i, collects.get(j));
		collects.set(j, collect);
	 }
	
	
	// REPLACES
	public ArrayList<ReplaceEntry> getReplaces() {
		return replaces;
	}

	public ReplaceEntry createReplace(int i)
	 {
		ReplaceEntry entry = new ReplaceEntry();
		replaces.add(i, entry);
		return entry;
	 }

	public ReplaceEntry removeReplace(int i)
	 {
		return replaces.remove(i);
	 }

	public void switchReplace(int i, int j)
	 {
		ReplaceEntry replace = replaces.get(i);
		replaces.set(i, replaces.get(j));
		replaces.set(j, replace);
	 }
	
	
	// CONVERSION
	public String toStringCollect(int r, int c)
	 {
		if (r < 0 || r >= collects.size()) return null;
		
		CollectEntry entry = collects.get(r);

		return entry.toStringEntry(c);
	 }
	
	public void fromStringCollect(int r, int c, String value)
	 {
		if (r < 0 || r >= collects.size()) return;
		
		CollectEntry entry = collects.get(r);

		entry.fromStringEntry(c, value);
	 }
	
	
	public String toStringReplace(int r, int c)
	 {
		if (r < 0 || r >= replaces.size()) return null;
		
		ReplaceEntry entry = replaces.get(r);

		return entry.toStringEntry(c);
	 }
	
	public void fromStringReplace(int r, int c, String value)
	 {
		if (r < 0 || r >= replaces.size()) return;
		
		ReplaceEntry entry = replaces.get(r);

		entry.fromStringEntry(c, value);
	 }
	
	
	// FILTERING
	public ArrayList<RegexCollect> filterCollects(MinorityArticle srcArticle, MinorityArticle dstArticle)
	 {
		String srcLangCode = srcArticle.getRef().getLangCode();
		String dstLangCode = dstArticle.getRef().getLangCode();
		
		return filterCollects(srcLangCode, dstLangCode);
	 }

	public ArrayList<RegexCollect> filterCollects(String srcLangCode, String dstLangCode)
	 {
		ArrayList<RegexCollect> result = new ArrayList<>();
		for (CollectEntry entry : collects) {
			if (entry.getFilter().isAccept(srcLangCode, dstLangCode)) result.add(entry.createRegex());
		}
		
		return result;
	 }

	public ArrayList<RegexReplace> filterReplaces(MinorityArticle srcArticle, MinorityArticle dstArticle)
	 {
		String srcLangCode = srcArticle.getRef().getLangCode();
		String dstLangCode = dstArticle.getRef().getLangCode();
		
		return filterReplaces(srcLangCode, dstLangCode);
	 }

	public ArrayList<RegexReplace> filterReplaces(String srcLangCode, String dstLangCode)
	 {
		ArrayList<RegexReplace> result = new ArrayList<>();
		for (ReplaceEntry entry : replaces) {
			if (entry.getFilter().isAccept(srcLangCode, dstLangCode)) result.add(entry.createRegex());
		}
		
		return result;
	 }

	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((collects == null) ? 0 : collects.hashCode());
		result = prime * result + ((replaces == null) ? 0 : replaces.hashCode());
		return result;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ProcessingPreferences other = (ProcessingPreferences) obj;
		if (collects == null) {
			if (other.collects != null) return false;
		} else if (!collects.equals(other.collects)) return false;
		if (replaces == null) {
			if (other.replaces != null) return false;
		} else if (!replaces.equals(other.replaces)) return false;
		return true;
	}

	
}
