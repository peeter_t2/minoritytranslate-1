package ee.translate.keeleleek.mtapplication.controller.content;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class ContentCancelRequestUploadQidCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String qid = (String) notification.getBody();
		if (qid == null) return;
		
		String[] langCodes = MinorityTranslateModel.preferences().getDstLangCodes();
		for (String langCode : langCodes) {
			Reference ref = new Reference(qid, langCode);
			MinorityTranslateModel.content().cancelRequestUpload(ref);
		}
	 }
	
}
