package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.nio.file.Path;
import java.util.List;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.dialogs.ConfirmDeclineDialog;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.dialogs.QuickStartDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.mapping.PullMapping;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class DialogFactoryFXBackwardCompatible extends DialogFactory implements ApplicationDialogFactoryFX {

	@Override
	public List<ContentRequest> askAddContent() {
		return null;
	}
	
	@Override
	public String askString(String title, String header, String content, String initial, boolean allowEmpty)
	 {
		// create grid
		GridPane grid = new GridPane();
		grid.setHgap(7);
		grid.setVgap(7);
		grid.setPadding(new Insets(7, 7, 7, 7));
		grid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		grid.setMinSize(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);

		final TextField textField = new TextField(initial);
		
		Label headerLabel = new Label(header);
		headerLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		grid.add(headerLabel, 0, 0, 2, 1);
		grid.add(new Label(content), 0, 1);
		grid.add(textField, 1, 1);
		
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(7);
		Pane pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		pane.setMaxWidth(Double.MAX_VALUE);
		buttonsBox.getChildren().add(pane);
		
		grid.add(buttonsBox, 0, 2, 2, 1);
		
		// create stage
		final Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		Scene scene = new Scene(grid);
		stage.setScene(scene);
		
		// create buttons
		final Button okButton = new Button(Messages.getString("button.ok"));
		Button cancelButton = new Button(Messages.getString("button.cancel"));
		okButton.setMinWidth(60);
		cancelButton.setMinWidth(60);
		
		buttonsBox.getChildren().add(cancelButton);
		buttonsBox.getChildren().add(okButton);

		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				textField.setText(null);
				stage.close();
			}
		});
		
		// disable
		if (!allowEmpty) {
			okButton.setDisable(textField.getText().trim().isEmpty());
			
			textField.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					okButton.setDisable(newValue == null || newValue.trim().isEmpty());
				}
			});
		}
		
		// focus
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				textField.requestFocus();
			}
		});
		
		// show
		stage.showAndWait();
		
		return textField.getText();
	 }
	
	@Override
	protected boolean askConfirmDecline(String title, String header, String content, String confirm, String cancel)
	 {
		return true;
	 }
	
	@Override
	protected YesNoCancel askConfirmYesNoCancel(String title, String header, String content, String yes, String no, String cancel) {
		return YesNoCancel.YES;
	}
	
	@Override
	public void showError(String header, String content)
	 {
		// create grid
		GridPane grid = new GridPane();
		grid.setHgap(7);
		grid.setVgap(7);
		grid.setPadding(new Insets(7, 7, 7, 7));
		grid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		grid.setMinSize(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);

		Label headerLabel = new Label(header);
		headerLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		grid.add(headerLabel, 0, 0, 2, 1);
		grid.add(new Label(content), 0, 1);
		
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(7);
		Pane pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		pane.setMaxWidth(Double.MAX_VALUE);
		buttonsBox.getChildren().add(pane);
		
		grid.add(buttonsBox, 0, 2, 2, 1);
		
		// create stage
		final Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle(Messages.getString("messages.error"));
		stage.setResizable(false);
		Scene scene = new Scene(grid);
		stage.setScene(scene);
		
		// create buttons
		final Button okButton = new Button(Messages.getString("button.ok"));
		okButton.setMinWidth(60);
		
		buttonsBox.getChildren().add(okButton);

		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		
		// show
		stage.showAndWait();
	 }
	
	@Override
	public void showWarning(String header, String content)
	 {
		// create grid
		GridPane grid = new GridPane();
		grid.setHgap(7);
		grid.setVgap(7);
		grid.setPadding(new Insets(7, 7, 7, 7));
		grid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		grid.setMinSize(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);

		Label headerLabel = new Label(header);
		headerLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		grid.add(headerLabel, 0, 0, 2, 1);
		grid.add(new Label(content), 0, 1);
		
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(7);
		Pane pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		pane.setMaxWidth(Double.MAX_VALUE);
		buttonsBox.getChildren().add(pane);
		
		grid.add(buttonsBox, 0, 2, 2, 1);
		
		// create stage
		final Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle(Messages.getString("messages.warning"));
		stage.setResizable(false);
		Scene scene = new Scene(grid);
		stage.setScene(scene);
		
		// create buttons
		final Button okButton = new Button(Messages.getString("button.ok"));
		okButton.setMinWidth(60);
		
		buttonsBox.getChildren().add(okButton);

		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		
		// show
		stage.showAndWait();
	 }

	@Override
	public PullCallback showPullProgressDialog(String title, String header, String ok, String cancel) {
		return new PullCallback() {
			@Override
			protected void onStart(int total) { }
			
			@Override
			protected void onAction(String action) { }
			
			@Override
			protected void onAdvance(int current) { }

			@Override
			protected void onMap(PullMapping association) { }
			
			@Override
			protected void onEnd() { }
			
			@Override
			protected void onFinish(boolean success) { }
		};
	}
	
	@Override
	public PullCallback showPullMappingsDialog(String title, String header, String ok, String cancel) {
		return new PullCallback() {
			@Override
			protected void onStart(int total) { }
			
			@Override
			protected void onAction(String action) { }
			
			@Override
			protected void onAdvance(int current) { }

			@Override
			protected void onMap(PullMapping association) { }
			
			@Override
			protected void onEnd() { }
			
			@Override
			protected void onFinish(boolean success) { }
		};
	}
	
	@Override
	public void showInfo(String header, String content)
	 {
		// create grid
		GridPane grid = new GridPane();
		grid.setHgap(7);
		grid.setVgap(7);
		grid.setPadding(new Insets(7, 7, 7, 7));
		grid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		grid.setMinSize(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);

		Label headerLabel = new Label(header);
		headerLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		grid.add(headerLabel, 0, 0, 2, 1);
		grid.add(new Label(content), 0, 1);
		
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(7);
		Pane pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		pane.setMaxWidth(Double.MAX_VALUE);
		buttonsBox.getChildren().add(pane);
		
		grid.add(buttonsBox, 0, 2, 2, 1);
		
		// create stage
		final Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle(Messages.getString("messages.info"));
		stage.setResizable(false);
		Scene scene = new Scene(grid);
		stage.setScene(scene);
		
		// create buttons
		final Button okButton = new Button(Messages.getString("button.ok"));
		okButton.setMinWidth(60);
		
		buttonsBox.getChildren().add(okButton);

		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		
		// show
		stage.showAndWait();
	 }
	
	
	@Override
	public void askQuickStart(List<String> allPages)
	 {
		final Stage dialog = new Stage(StageStyle.UTILITY);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.setTitle(Messages.getString("quick.start.title"));
		
		final Button nextButton = new Button(Messages.getString("quick.start.next.button"));
		nextButton.setPrefWidth(75.0);
		
		final QuickStartDialogMediator mediator = MediatorLoaderFX.loadQuickStartDialogMediator();
		
		VBox content = new VBox((Node)mediator.getViewComponent());
		HBox controls = new HBox(nextButton);
		HBox.setMargin(nextButton, new Insets(11,11,11,11));
		controls.setAlignment(Pos.CENTER_RIGHT);
		content.getChildren().add(controls);
		
		mediator.open(allPages);
		dialog.setScene(new Scene(content));
		
		if (mediator.isLast()) nextButton.setText(Messages.getString("quick.start.finish.button"));

		nextButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event)
			 {
				boolean close = mediator.isLast();
				String pageName = mediator.onClosePage();
				MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_ACKNOWLEDGE, pageName);
				if (mediator.isLast()) nextButton.setText(Messages.getString("quick.start.finish.button"));
				if (close) dialog.close();
			 }
		});
		
		if (mediator.hasPages()) dialog.showAndWait();
	 }
	
	@Override
	public String askUnicodeCharacter() {
		return null;
	}
	
	@Override
	public Path askSymbolsSavePath() {
		return null;
	}

	@Override
	public Path askSymbolsOpenPath() {
		return null;
	}
	
	@Override
	public ConfirmDecline showConfirmDeclineDialog(Mediator mediator, String title, String header, String ok, String cancel)
	 {
		// create grid
		GridPane grid = new GridPane();
		grid.setHgap(7);
		grid.setVgap(7);
		grid.setPadding(new Insets(7, 7, 7, 7));
		grid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		grid.setMinSize(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);

		Label headerLabel = new Label(header);
		headerLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		grid.add(headerLabel, 0, 0, 2, 1);
		grid.add((Node) mediator.getViewComponent(), 0, 1, 2 , 1);
		
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(7);
		Pane pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		pane.setMaxWidth(Double.MAX_VALUE);
		buttonsBox.getChildren().add(pane);
		
		grid.add(buttonsBox, 0, 2, 2, 1);
		
		// create stage
		final Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		Scene scene = new Scene(grid);
		stage.setScene(scene);
		
		// create buttons
		final Button okButton = new Button(ok);
		Button cancelButton = new Button(cancel);
		okButton.setMinWidth(60);
		cancelButton.setMinWidth(60);
		
		buttonsBox.getChildren().add(cancelButton);
		buttonsBox.getChildren().add(okButton);

		final ConfirmDeclineDialog confirmDeclineDialog = mediator instanceof ConfirmDeclineDialog ? (ConfirmDeclineDialog) mediator : null;

		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (confirmDeclineDialog != null) confirmDeclineDialog.confirm();
				stage.close();
			}
		});
		
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (confirmDeclineDialog != null) confirmDeclineDialog.decline();
				stage.close();
			}
		});
		
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (confirmDeclineDialog != null) confirmDeclineDialog.decline();
			}
		});
		
		if (confirmDeclineDialog != null) confirmDeclineDialog.prepare();
		
		// focus
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (confirmDeclineDialog != null) confirmDeclineDialog.focus();
			}
		});
		
		// show
		stage.showAndWait();
		
		return ConfirmDecline.UNKNOWN;
	 }
	
	@Override
	public void showCloseDialog(Mediator mediator, String title, String header, String close)
	 {
		// create grid
		GridPane grid = new GridPane();
		grid.setHgap(7);
		grid.setVgap(7);
		grid.setPadding(new Insets(7, 7, 7, 7));
		grid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		grid.setMinSize(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);

		Label headerLabel = new Label(header);
		headerLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		grid.add(headerLabel, 0, 0, 2, 1);
		grid.add((Node) mediator.getViewComponent(), 0, 1, 2 , 1);
		
		HBox buttonsBox = new HBox();
		buttonsBox.setSpacing(7);
		Pane pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		pane.setMaxWidth(Double.MAX_VALUE);
		buttonsBox.getChildren().add(pane);
		
		grid.add(buttonsBox, 0, 2, 2, 1);
		
		// create stage
		final Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		Scene scene = new Scene(grid);
		stage.setScene(scene);
		
		// create buttons
		final Button okButton = new Button(close);
		okButton.setMinWidth(60);
		
		buttonsBox.getChildren().add(okButton);

		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.close();
			}
		});
		
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				
			}
		});
		
		// show
		stage.showAndWait();
	 }
	
	
}
