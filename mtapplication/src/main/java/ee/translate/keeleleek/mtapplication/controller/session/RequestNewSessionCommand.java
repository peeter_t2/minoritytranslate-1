package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory.YesNoCancel;

public class RequestNewSessionCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		// Save confirmation:
		if (!MinorityTranslateModel.content().isSaved()) {
			
			YesNoCancel state = DialogFactory.dialogs().confirmNewSession();
			
			switch (state) {
				case YES:
					sendNotification(Notifications.MENU_SESSION_SAVE);
					break;
				
				case CANCEL:
					return;

				default:
					break;
			}
			
		}

		// New session:
		getFacade().sendNotification(Notifications.SESSION_NEW);
	 }
	
}
