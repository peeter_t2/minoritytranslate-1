package ee.translate.keeleleek.mtapplication.model;

import org.puremvc.java.multicore.core.model.Model;
import org.puremvc.java.multicore.interfaces.IProxy;

import ee.translate.keeleleek.mtapplication.model.application.ApplicationProxy;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteProxy;
import ee.translate.keeleleek.mtapplication.model.bots.BotsProxy;
import ee.translate.keeleleek.mtapplication.model.bots.LoginProxy;
import ee.translate.keeleleek.mtapplication.model.connection.ConnectionProxy;
import ee.translate.keeleleek.mtapplication.model.content.ContentDownloaderProxy;
import ee.translate.keeleleek.mtapplication.model.content.ContentProxy;
import ee.translate.keeleleek.mtapplication.model.content.ContentQueuerProxy;
import ee.translate.keeleleek.mtapplication.model.gui.GUINotifierProxy;
import ee.translate.keeleleek.mtapplication.model.gui.GUINotifierProxyFX;
import ee.translate.keeleleek.mtapplication.model.keeleleek.UsageInfoProxy;
import ee.translate.keeleleek.mtapplication.model.keys.KeyHandlerProxy;
import ee.translate.keeleleek.mtapplication.model.lists.ListsProxy;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxy;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxyFX;
import ee.translate.keeleleek.mtapplication.model.media.LogosProxyFX;
import ee.translate.keeleleek.mtapplication.model.plugins.PluginsProxy;
import ee.translate.keeleleek.mtapplication.model.plugins.PullProxy;
import ee.translate.keeleleek.mtapplication.model.plugins.SpellerProxy;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy;
import ee.translate.keeleleek.mtapplication.model.processing.ProcesserProxy;
import ee.translate.keeleleek.mtapplication.model.requests.RequestsProxy;
import ee.translate.keeleleek.mtapplication.model.session.SessionProxy;
import ee.translate.keeleleek.mtapplication.model.suggestions.SuggestionsProxy;
import ee.translate.keeleleek.mtapplication.model.timer.TimerProxy;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;

public class MinorityTranslateModel extends Model {

	private static PreferencesProxy preferences;
	private static WikisProxy wikis;
	private static UsageInfoProxy usage;
	private static SessionProxy session;
	private static ConnectionProxy connection;
	private static GUINotifierProxy notifier;
	private static ListsProxy lists;
	private static BotsProxy bots;
	private static ContentProxy content;
	private static ContentQueuerProxy queuer;
	private static ContentDownloaderProxy downloader;
	private static ProcesserProxy processer;
	private static IconsProxy icons;
	private static SuggestionsProxy suggestions;
	private static LoginProxy login;
	private static KeyHandlerProxy keys;
	private static AutocompleteProxy autocomplete;
	private static RequestsProxy requests;
	private static ApplicationProxy application;
	private static PluginsProxy plugins;
	private static TimerProxy timer;
	private static PullProxy pull;
	private static SpellerProxy speller;
	
	/**
	 * Gets application preferences.
	 * 
	 * @return preferences proxy
	 */
	public static PreferencesProxy preferences() { return preferences; }

	/**
	 * Gets wikis.
	 * 
	 * @return wikis proxy
	 */
	public static WikisProxy wikis() { return wikis; }

	/**
	 * Gets usage info.
	 * 
	 * @return usage proxy
	 */
	public static UsageInfoProxy usage() { return usage; }

	/**
	 * Gets session.
	 * 
	 * @return session proxy
	 */
	public static SessionProxy session() { return session; }

	/**
	 * Gets connection.
	 * 
	 * @return connection proxy
	 */
	public static ConnectionProxy connection() { return connection; }

	/**
	 * Gets notifier.
	 * 
	 * @return notifier proxy
	 */
	public static GUINotifierProxy notifier() { return notifier; }

	/**
	 * Gets lists.
	 * 
	 * @return lists proxy
	 */
	public static ListsProxy lists() { return lists; }

	/**
	 * Gets bots.
	 * 
	 * @return bots proxy
	 */
	public static BotsProxy bots() { return bots; }

	/**
	 * Gets content.
	 * 
	 * @return content proxy
	 */
	public static ContentProxy content() { return content; }

	/**
	 * Gets queuer.
	 * 
	 * @return queuer proxy
	 */
	public static ContentQueuerProxy queuer() { return queuer; }

	/**
	 * Gets downloader.
	 * 
	 * @return downloader proxy
	 */
	public static ContentDownloaderProxy downloader() { return downloader; }

	/**
	 * Gets processer.
	 * 
	 * @return processer proxy
	 */
	public static ProcesserProxy processer() { return processer; }

	/**
	 * Gets icons.
	 * 
	 * @return icons proxy
	 */
	public static IconsProxy icons() { return icons; }

	/**
	 * Gets suggestions.
	 * 
	 * @return icons suggestions
	 */
	public static SuggestionsProxy suggestions() { return suggestions; }

	/**
	 * Gets login.
	 * 
	 * @return login
	 */
	public static LoginProxy login() { return login; }

	/**
	 * Gets key handler.
	 * 
	 * @return key handler
	 */
	public static KeyHandlerProxy keys() { return keys; }

	/**
	 * Gets autocomplete.
	 * 
	 * @return autocomplete
	 */
	public static AutocompleteProxy autocomplete() { return autocomplete; }

	/**
	 * Gets requests.
	 * 
	 * @return requests
	 */
	public static RequestsProxy requests() { return requests; }

	/**
	 * Gets application.
	 * 
	 * @return application
	 */
	public static ApplicationProxy application() { return application; }

	/**
	 * Gets plugins.
	 * 
	 * @return plugins
	 */
	public static PluginsProxy plugins() { return plugins; }

	/**
	 * Gets timer.
	 * 
	 * @return timer
	 */
	public static TimerProxy timer() { return timer; }

	/**
	 * Gets speller.
	 * 
	 * @return speller
	 */
	public static SpellerProxy speller() { return speller; }

	/**
	 * Gets pull.
	 * 
	 * @return pull
	 */
	public static PullProxy pull() { return pull; }

	
	// INIT
	public synchronized static Model getInstance(String key)
	 {
		if (instanceMap.get(key) == null) new MinorityTranslateModel(key);
		return instanceMap.get(key);
	 }
	
	protected MinorityTranslateModel(String key) {
		super(key);
	}

	@Override
	protected void initializeModel()
	 {
		registerProxy(new GUINotifierProxyFX());
		registerProxy(new LogosProxyFX());
		registerProxy(new IconsProxyFX());
		
		registerProxy(new ConnectionProxy());
		registerProxy(new BotsProxy());
		registerProxy(new SuggestionsProxy());
		registerProxy(new ProcesserProxy());
		registerProxy(new LoginProxy());
		registerProxy(new KeyHandlerProxy());
		registerProxy(new AutocompleteProxy());
		registerProxy(new RequestsProxy());
		registerProxy(new ApplicationProxy());
		registerProxy(new SpellerProxy());
		registerProxy(new PullProxy());
		registerProxy(new PluginsProxy());
		registerProxy(new TimerProxy());
	 }
	
	
	// PROXY MANAGEMENT
	@Override
	public void registerProxy(IProxy proxy)
	 {
		String proxyName = proxy.getProxyName();
		if (hasProxy(proxyName)) removeProxy(proxyName);
		
		switch (proxy.getProxyName()) {
		case PreferencesProxy.NAME:
			preferences = (PreferencesProxy) proxy;
			break;
			
		case WikisProxy.NAME:
			wikis = (WikisProxy) proxy;
			break;

		case UsageInfoProxy.NAME:
			usage = (UsageInfoProxy) proxy;
			break;

		case SessionProxy.NAME:
			session = (SessionProxy) proxy;
			break;

		case ConnectionProxy.NAME:
			connection = (ConnectionProxy) proxy;
			break;

		case GUINotifierProxy.NAME:
			notifier = (GUINotifierProxy) proxy;
			break;

		case ListsProxy.NAME:
			lists = (ListsProxy) proxy;
			break;

		case BotsProxy.NAME:
			bots = (BotsProxy) proxy;
			break;

		case ContentProxy.NAME:
			content = (ContentProxy) proxy;
			break;

		case ContentQueuerProxy.NAME:
			queuer = (ContentQueuerProxy) proxy;
			break;

		case ContentDownloaderProxy.NAME:
			downloader = (ContentDownloaderProxy) proxy;
			break;

		case ProcesserProxy.NAME:
			processer = (ProcesserProxy) proxy;
			break;

		case IconsProxy.NAME:
			icons = (IconsProxy) proxy;
			break;

		case SuggestionsProxy.NAME:
			suggestions = (SuggestionsProxy) proxy;
			break;

		case LoginProxy.NAME:
			login = (LoginProxy) proxy;
			break;

		case KeyHandlerProxy.NAME:
			keys = (KeyHandlerProxy) proxy;
			break;

		case AutocompleteProxy.NAME:
			autocomplete = (AutocompleteProxy) proxy;
			break;

		case RequestsProxy.NAME:
			requests = (RequestsProxy) proxy;
			break;

		case ApplicationProxy.NAME:
			application = (ApplicationProxy) proxy;
			break;

		case PluginsProxy.NAME:
			plugins = (PluginsProxy) proxy;
			break;

		case TimerProxy.NAME:
			timer = (TimerProxy) proxy;
			break;

		case SpellerProxy.NAME:
			speller = (SpellerProxy) proxy;
			break;

		case PullProxy.NAME:
			pull = (PullProxy) proxy;
			break;

		default:
			break;
		}
		
		super.registerProxy(proxy);
	 }
	
	@Override
	public IProxy removeProxy(String proxyName)
	 {
		IProxy proxy = super.removeProxy(proxyName);
		proxy.onRemove();
		
		return proxy;
	 }
	
	
}
