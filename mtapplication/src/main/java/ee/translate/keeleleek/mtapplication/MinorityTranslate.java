package ee.translate.keeleleek.mtapplication;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.view.dialogs.AboutDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MinorityTranslateFX;

public class MinorityTranslate {

	public static String ROOT_PATH = "mtdata";
	
	public static String VERSION;
	public static String BUILD_TIME;
	
	public static boolean NEUTERED = false;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(MinorityTranslate.class);
	
	public static void main(String[] args) throws IOException
	 {
		setupRoot();
		setupVersion();
		
		Logging.setup();
		
		for (String arg : args) {
			if (arg.equalsIgnoreCase("neuter")) NEUTERED = true;
		}
		
		try
		 {
			LOGGER.info("Starting application");
			
			MinorityTranslateFX.main(args);
		 } 
		catch (Throwable e)
		 {
			LOGGER.error("Application failure: ", e);
		 }
	 }
	
	
	// ROOT
	public static void setupRoot()
	 {
		File root = javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory();
		if (root != null) ROOT_PATH = new File(root, "Minority Translate").getAbsolutePath();
		System.setProperty("ROOT_PATH", ROOT_PATH);
	 }
	
	
	// VERSION
	private static void setupVersion()
	 {
		Properties properties = new Properties();
		try {
			properties.load(AboutDialogMediator.class.getResourceAsStream("/application.properties"));
			VERSION = properties.getProperty("version");
			BUILD_TIME = properties.getProperty("build.time");
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (VERSION == null) VERSION = "";
		if (BUILD_TIME == null) BUILD_TIME = "";
	 }

}
