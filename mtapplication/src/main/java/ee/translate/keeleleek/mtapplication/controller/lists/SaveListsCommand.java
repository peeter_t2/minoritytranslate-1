package ee.translate.keeleleek.mtapplication.controller.lists;

import java.io.IOException;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.lists.ListsProxy;

public class SaveListsCommand extends SimpleCommand {

	private static Logger LOGGER = LoggerFactory.getLogger(SaveListsCommand.class);
	
	
	@Override
	public void execute(INotification notification)
	 {
		ListsProxy listsProxy = MinorityTranslateModel.lists();
		
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			Gson gson = builder.create();
			String json = gson.toJson(listsProxy);
			
			FileUtil.write(FetchListsCommand.LISTS_PATH, json);
		
		} catch (IOException e) {
			LOGGER.error("Failed to write lists", e);
//			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.languages.save.failed"), Messages.getString("messages.languages.save.failed.to").replaceFirst("#", FetchLanguagesCommand.LANGUAGES_PATH.toString()).replaceFirst("#",e.getMessage()));
			return;
		}
	 }

	
}
