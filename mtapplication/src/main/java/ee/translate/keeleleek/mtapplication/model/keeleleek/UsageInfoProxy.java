package ee.translate.keeleleek.mtapplication.model.keeleleek;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class UsageInfoProxy extends Proxy {

	public final static String NAME = "{D2BBC8C0-C7EF-11E3-9C1A-0800200C9A66}";
	
	public final static String URI_STR = "http://mwtranslate.keeleleek.ee/log";

	
	private static Logger LOGGER = LoggerFactory.getLogger(UsageInfoProxy.class);
	

	private HashMap<Reference, UsageInfoElement> usages = new HashMap<>();
	
	
	// INIT
	public UsageInfoProxy() {
		super(NAME);
	}

	
	// USAGEINFO
	public void createUsage(MinorityArticle article)
	 {
		Reference ref = article.getRef();
		String qid = article.getRef().getQid();
		String langCode = article.getRef().getLangCode();
		String title = article.getTitle();
		String oldRev = article.getPreviousRevision();
		
		UsageInfoElement usage = new UsageInfoElement();
		usages.put(ref, usage);
		
		// title
		usage.setTitle(title);
		
		// old id
		usage.setOldId(oldRev);

		// language code
		usage.setLangCode(langCode);
		
		// usage
		usage.setReadyAt(now());

		// source language code
		usage.setSrcLangCode(article.getSrcLangCode());
		
		// from
		String[] fromLangCodes = MinorityTranslateModel.preferences().getSrcLangCodes();
		HashMap<String, String> fromRevs = new HashMap<>();
		for (String otherLangCode : fromLangCodes) {
			
			Reference otherRef = new Reference(qid, otherLangCode);
			MinorityArticle otherArticle = MinorityTranslateModel.content().getArticle(otherRef);
			
			String otherRev = otherArticle.getPreviousRevision();
			fromRevs.put(otherLangCode, otherRev);
		
		}
		usage.setFromIds(fromRevs);

		// to
		String[] toLangCodes = MinorityTranslateModel.preferences().getDstLangCodes();
		HashMap<String, String> toRevs = new HashMap<>();
		for (String otherLangCode : toLangCodes) {
			
			Reference otherRef = new Reference(qid, otherLangCode);
			MinorityArticle otherArticle = MinorityTranslateModel.content().getArticle(otherRef);
			
			String otherRev = otherArticle.getPreviousRevision();
			toRevs.put(otherLangCode, otherRev);
		
		}
		usage.setToIds(toRevs);
		
		// version
		usage.setVersion(MinorityTranslate.VERSION);
		usage.setSavedAt(now());

		// saved
		usage.setSavedAt(now());
		
		// one-to-one
		usage.setOneToOne(article.isExact());
		
		// proficiencies
		usage.setProficiencies(MinorityTranslateModel.preferences().corpus().getProficiencies());
		
		// properties
		HashMap<String, String> properties = new HashMap<>();
		properties.put("java vendor", System.getProperty("java.vendor"));
		properties.put("java version", System.getProperty("java.version"));
		properties.put("java vendor", System.getProperty("java.vendor"));
		properties.put("os name", System.getProperty("os.name"));
		properties.put("os arch", System.getProperty("os.arch"));
		properties.put("os version", System.getProperty("os.version"));
		usage.setSystemProperties(properties);
	 }

	public void createUsage(Reference ref)
	 {
		MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
		if (article == null) return;
		createUsage(article);
	 }

	public void updateNewRev(Reference ref, String rev)
	 {
		UsageInfoElement usage = usages.get(ref);

		if (usage == null) {
			LOGGER.error("Article updating failed, because the usage entry with reference " + ref + " is missing!");
			return;
		}
		
		usage.setRevisionId(rev);
	 }

	public void updateUsername(Reference ref, String username)
	 {
		UsageInfoElement usage = usages.get(ref);

		if (usage == null) {
			LOGGER.error("Article updating failed, because the usage entry with reference " + ref + " is missing!");
			return;
		}
		
		usage.setMd5(md5(username));
	 }

	public void updateTag(Reference ref, String username)
	 {
		UsageInfoElement usage = usages.get(ref);

		if (usage == null) {
			LOGGER.error("Article updating failed, because the usage entry with reference " + ref + " is missing!");
			return;
		}
		
		usage.setTag(MinorityTranslateModel.session().getTag());
	 }
	
	
	// SERVER
	public void send(final Reference ref)
	 {
		// check if allowed
		if (!MinorityTranslateModel.preferences().corpus().isCollect()) return;
		
		// send usage
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				UsageInfoElement usage = usages.get(ref);

				if (usage == null) {
					LOGGER.error("Article updating failed, because the usage entry with reference " + ref + " is missing!");
					return;
				}
				
				LOGGER.info("Sending usage info for reference " + ref);
				send(usage);
				
			}
			
		});
		thread.start();
	}
	
	private void send(UsageInfoElement usage)
	 {
		LOGGER.debug("Usage info: " + usage.toJson());

		if (usage.getRevisionId() == null) {
			LOGGER.warn("Usage missing revisionId");
			return;
		}

		if (usage.getRevisionId().equals(usage.getOldId())) {
			LOGGER.warn("Usage oldId and revisionId match: " + usage.getRevisionId());
			return;
		}
		
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(new URI(URI_STR));
			post.setHeader("Content-type", "application/json; charset=utf-8");
			post.setEntity(new StringEntity(usage.toJson(), "utf-8"));

			HttpResponse r = client.execute(post);

			BufferedReader rd = new BufferedReader(new InputStreamReader(r.getEntity().getContent()));
			String line = "";
			LOGGER.debug("Response:");
			while ((line = rd.readLine()) != null) {
				LOGGER.debug(" " + line);
			}
		} catch (Exception e) {
			LOGGER.error("Failed to send usage info", e);
		}
	 }
	
	
	// UTILITY
	private long now() {
		return System.currentTimeMillis();
	}
	
	private String md5(String username) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(username.getBytes());
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			LOGGER.warn("MD5 retrieval failed", e);
			return username;
		}
	}
	
	
}
