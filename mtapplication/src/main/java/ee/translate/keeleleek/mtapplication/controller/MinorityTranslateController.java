package ee.translate.keeleleek.mtapplication.controller;

import org.puremvc.java.multicore.core.controller.Controller;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.autocomplete.AutocompleteCommand;
import ee.translate.keeleleek.mtapplication.controller.connection.InitialConnectionAvailableCommand;
import ee.translate.keeleleek.mtapplication.controller.connection.StartConnectionMonitoringCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentCancelRequestUploadCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentCancelRequestUploadQidCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentChangeExactCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentChangeTextCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentChangeTitleCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentRequestFilteredPreviewCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentRequestPreviewCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentRequestUploadCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentRequestUploadQidCommand;
import ee.translate.keeleleek.mtapplication.controller.content.ContentResetArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.find.FindCommand;
import ee.translate.keeleleek.mtapplication.controller.gui.PrepareGUICommand;
import ee.translate.keeleleek.mtapplication.controller.gui.RestartGUICommand;
import ee.translate.keeleleek.mtapplication.controller.info.OpenAboutCommand;
import ee.translate.keeleleek.mtapplication.controller.info.OpenManualCommand;
import ee.translate.keeleleek.mtapplication.controller.lists.EnableSuggestionList;
import ee.translate.keeleleek.mtapplication.controller.lists.FetchListsCommand;
import ee.translate.keeleleek.mtapplication.controller.lists.RemoveSuggestion;
import ee.translate.keeleleek.mtapplication.controller.lists.ResetSuggestionLists;
import ee.translate.keeleleek.mtapplication.controller.lists.SaveListsCommand;
import ee.translate.keeleleek.mtapplication.controller.login.LoginCommand;
import ee.translate.keeleleek.mtapplication.controller.login.LoginRequestCommand;
import ee.translate.keeleleek.mtapplication.controller.login.LogoutCommand;
import ee.translate.keeleleek.mtapplication.controller.login.OpenLoginCommand;
import ee.translate.keeleleek.mtapplication.controller.navigation.SelectArticleQidCommand;
import ee.translate.keeleleek.mtapplication.controller.navigation.SelectNextArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.navigation.SelectPreviousArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.paste.PasteCommand;
import ee.translate.keeleleek.mtapplication.controller.plugins.RequestPullCommand;
import ee.translate.keeleleek.mtapplication.controller.plugins.RequestSpellcheckCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.AddArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.AddCategoryCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.AddContentCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.AddExpandedContentCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.AddLinksCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.OpenAddArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.OpenAddCategoryCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.OpenAddLinksCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.OpenListsCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.OpenRemoveArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.RemoveArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.RemoveArticleQidCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.RemoveSelectedArticleCommand;
import ee.translate.keeleleek.mtapplication.controller.populating.StopDownloadingCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.AcknowledgeCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesContentAssistCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesCorpusCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesFilterCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesLanguagesCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesListsEnabledCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesLookupsCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesProcessingCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesProgramCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesSymbolsCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesTemplateMappingCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesTemplatesCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.ChangePreferencesVisualCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.LoadPreferencesCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.OpenPreferencesCommand;
import ee.translate.keeleleek.mtapplication.controller.preferences.SavePreferencesCommand;
import ee.translate.keeleleek.mtapplication.controller.program.QuitCommand;
import ee.translate.keeleleek.mtapplication.controller.program.RequestQuitCommand;
import ee.translate.keeleleek.mtapplication.controller.quickstart.QuickStartCommand;
import ee.translate.keeleleek.mtapplication.controller.quickstart.ResetQuickStartCommand;
import ee.translate.keeleleek.mtapplication.controller.requests.RequestCommand;
import ee.translate.keeleleek.mtapplication.controller.session.AutosaveSessionCommand;
import ee.translate.keeleleek.mtapplication.controller.session.ChangeNotesCommand;
import ee.translate.keeleleek.mtapplication.controller.session.ChangeTagCommand;
import ee.translate.keeleleek.mtapplication.controller.session.ChangeUploadEnabledCommand;
import ee.translate.keeleleek.mtapplication.controller.session.LoadSessionCommand;
import ee.translate.keeleleek.mtapplication.controller.session.NewSessionCommand;
import ee.translate.keeleleek.mtapplication.controller.session.OpenSessionOpenCommand;
import ee.translate.keeleleek.mtapplication.controller.session.OpenSessionSaveAsCommand;
import ee.translate.keeleleek.mtapplication.controller.session.OpenSessionSaveCommand;
import ee.translate.keeleleek.mtapplication.controller.session.RequestNewSessionCommand;
import ee.translate.keeleleek.mtapplication.controller.session.RequestSaveSessionCommand;
import ee.translate.keeleleek.mtapplication.controller.session.SaveSessionCommand;
import ee.translate.keeleleek.mtapplication.controller.session.SessionOpenCommand;
import ee.translate.keeleleek.mtapplication.controller.session.SuggestSessionNameCommand;
import ee.translate.keeleleek.mtapplication.controller.suggestions.RequestSuggestionsCommand;
import ee.translate.keeleleek.mtapplication.controller.suggestions.SuggestCategoryCommand;
import ee.translate.keeleleek.mtapplication.controller.suggestions.SuggestLanguageCommand;
import ee.translate.keeleleek.mtapplication.controller.suggestions.SuggestLoadedLanguageCommand;
import ee.translate.keeleleek.mtapplication.controller.suggestions.SuggestLoadedTitleCommand;
import ee.translate.keeleleek.mtapplication.controller.timer.TimerStartCommand;
import ee.translate.keeleleek.mtapplication.controller.wikis.FetchWikisCommand;
import ee.translate.keeleleek.mtapplication.controller.wikis.SaveWikisCommand;
import ee.translate.keeleleek.mtapplication.view.MinorityTranslateView;

public class MinorityTranslateController extends Controller {

	// INIT
	public synchronized static Controller getInstance(String key)
	 {
		if (instanceMap.get(key) == null) new MinorityTranslateController(key);
		return instanceMap.get(key);
	 }
	
	protected MinorityTranslateController(String key) {
		super(key);
	}

	@Override
	protected void initializeController()
	 {
		this.view = MinorityTranslateView.getInstance(multitonKey);
		
		// Application:
		registerCommand(Notifications.PREPARE_GUI, new PrepareGUICommand());
		registerCommand(Notifications.REQUEST_QUIT, new RequestQuitCommand());
		registerCommand(Notifications.QUIT, new QuitCommand());
		registerCommand(Notifications.GUI_RESTART, new RestartGUICommand());
		
		// quick start
		registerCommand(Notifications.SHOW_QUICK_START, new QuickStartCommand());
		registerCommand(Notifications.RESET_QUICK_START, new ResetQuickStartCommand());
		
		// Login:
		registerCommand(Notifications.LOGIN, new LoginCommand());
		registerCommand(Notifications.LOGOUT, new LogoutCommand());
		
		// Menu:
		registerCommand(Notifications.MENU_SESSION_OPEN, new OpenSessionOpenCommand());
		registerCommand(Notifications.MENU_SESSION_OPEN_CONFIRMED, new SessionOpenCommand());
		registerCommand(Notifications.MENU_SESSION_SAVE, new OpenSessionSaveCommand());
		registerCommand(Notifications.MENU_SESSION_SAVE_AS, new OpenSessionSaveAsCommand());
		registerCommand(Notifications.MENU_SESSION_SAVE_CONFIRMED, new RequestSaveSessionCommand());
		
		registerCommand(Notifications.MENU_PREFERENCES_OPEN, new OpenPreferencesCommand());
		registerCommand(Notifications.MENU_LOGIN_OPEN, new OpenLoginCommand());
		registerCommand(Notifications.MENU_LOGIN_CONFIMED, new LoginRequestCommand());
		
		registerCommand(Notifications.MENU_SELECT_NEXT_ARTICLE, new SelectNextArticleCommand());
		registerCommand(Notifications.MENU_SELECT_PREVIOUS_ARTICLE, new SelectPreviousArticleCommand());
		
		registerCommand(Notifications.MENU_ADD_CATEGORY_OPEN, new OpenAddCategoryCommand());
		registerCommand(Notifications.MENU_ADD_CATEGORY_CONFIRMED, new AddCategoryCommand());
		registerCommand(Notifications.MENU_ADD_ARTICLE_OPEN, new OpenAddArticleCommand());
		registerCommand(Notifications.MENU_ADD_ARTICLE_CONFIRMED, new AddArticleCommand());
		registerCommand(Notifications.ADD_CONTENT, new AddContentCommand());
		registerCommand(Notifications.ADD_EXPANDED_CONTENT, new AddExpandedContentCommand());
		registerCommand(Notifications.MENU_ADD_LINKS_OPEN, new OpenAddLinksCommand());
		registerCommand(Notifications.MENU_ADD_LINKS_CONFIRMED, new AddLinksCommand());
		registerCommand(Notifications.MENU_LISTS_OPEN, new OpenListsCommand());
		registerCommand(Notifications.MENU_REMOVE_ARTICLE_OPEN, new OpenRemoveArticleCommand());
		registerCommand(Notifications.MENU_REMOVE_ARTICLE_CONFIRMED, new RemoveArticleCommand());
		registerCommand(Notifications.MENU_REMOVE_SELECTED_ARTICLE, new RemoveSelectedArticleCommand());
		
		registerCommand(Notifications.MENU_ABOUT_OPEN, new OpenAboutCommand());
		registerCommand(Notifications.MENU_MANUAL_OPEN, new OpenManualCommand());
		
		// suggestions
		registerCommand(Notifications.SUGGEST_LOADED_TITLE, new SuggestLoadedTitleCommand());
		registerCommand(Notifications.SUGGEST_CATEGORY, new SuggestCategoryCommand());
		registerCommand(Notifications.SUGGEST_LANGUAGE, new SuggestLanguageCommand());
		registerCommand(Notifications.SUGGEST_LOADED_LANGUAGE, new SuggestLoadedLanguageCommand());
		
		registerCommand(Notifications.REQUEST_SUGGESTIONS, new RequestSuggestionsCommand());

		// Change preferences:
		registerCommand(Notifications.PREFERENCES_LOAD, new LoadPreferencesCommand());
		registerCommand(Notifications.PREFERENCES_SAVE, new SavePreferencesCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_PROGRAM, new ChangePreferencesProgramCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_LANGUAGES, new ChangePreferencesLanguagesCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_CORPUS, new ChangePreferencesCorpusCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_PROCESSING, new ChangePreferencesProcessingCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_FILTER, new ChangePreferencesFilterCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_LISTS_ENABLED, new ChangePreferencesListsEnabledCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_VISUAL, new ChangePreferencesVisualCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_TEMPLATE_MAPPING, new ChangePreferencesTemplateMappingCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_CONTENT_ASSIST, new ChangePreferencesContentAssistCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_INSERTS, new ChangePreferencesTemplatesCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_SYMBOLS, new ChangePreferencesSymbolsCommand());
		registerCommand(Notifications.PREFERENCES_CHANGE_LOOKUPS, new ChangePreferencesLookupsCommand());
		registerCommand(Notifications.PREFERENCES_ACKNOWLEDGE, new AcknowledgeCommand());
		
		// Preview:
		registerCommand(Notifications.PREVIEW_UPDATE_REQUEST, new ContentRequestPreviewCommand());
		
		// sessions
		registerCommand(Notifications.SESSION_SAVE, new SaveSessionCommand());
		registerCommand(Notifications.SESSION_LOAD, new LoadSessionCommand());
		registerCommand(Notifications.SESSION_NEW_REQUEST, new RequestNewSessionCommand());
		registerCommand(Notifications.SESSION_NEW, new NewSessionCommand());
		registerCommand(Notifications.SESSION_SUGGEST_NAME, new SuggestSessionNameCommand());
		registerCommand(Notifications.SESSION_CHANGE_TAG, new ChangeTagCommand());
		
		// wikis
		registerCommand(Notifications.FETCH_WIKIS, new FetchWikisCommand());
		registerCommand(Notifications.SAVE_WIKIS, new SaveWikisCommand());
		
		// notes
		registerCommand(Notifications.NOTES_UPDATE, new ChangeNotesCommand());
		registerCommand(Notifications.CHANGE_UPLOAD_ENABLED, new ChangeUploadEnabledCommand());
		
		// Connection:
		registerCommand(Notifications.CONNECTION_MONITORING_START, new StartConnectionMonitoringCommand());
		registerCommand(Notifications.CONNECTION_INITIAL_AVAILABLE, new InitialConnectionAvailableCommand());

		// Downloader:
		registerCommand(Notifications.DOWNLOADER_STOP, new StopDownloadingCommand());
		
		// new engine
		registerCommand(Notifications.ENGINE_SELECT_ARTICLE_QID, new SelectArticleQidCommand());
		
		registerCommand(Notifications.ENGINE_CHANGE_ARTICLE_TITLE, new ContentChangeTitleCommand());
		registerCommand(Notifications.ENGINE_CHANGE_ARTICLE_TEXT, new ContentChangeTextCommand());
		registerCommand(Notifications.ENGINE_CHANGE_ARTICLE_EXACT, new ContentChangeExactCommand());
		registerCommand(Notifications.ENGINE_RESET_ARTICLE, new ContentResetArticleCommand());
		
		registerCommand(Notifications.ENGINE_REQUEST_FETCH_PREVIEW, new ContentRequestPreviewCommand());
		registerCommand(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, new ContentRequestFilteredPreviewCommand());

		registerCommand(Notifications.ENGINE_REQUEST_UPLOAD, new ContentRequestUploadCommand());
		registerCommand(Notifications.ENGINE_REQUEST_UPLOAD_QID, new ContentRequestUploadQidCommand());
		registerCommand(Notifications.ENGINE_CANCEL_REQUEST_UPLOAD, new ContentCancelRequestUploadCommand());
		registerCommand(Notifications.ENGINE_CANCEL_REQUEST_UPLOAD_QID, new ContentCancelRequestUploadQidCommand());
		
		registerCommand(Notifications.ENGINE_REMOVE_ARTICLE, new RemoveArticleQidCommand());

		// processing
		registerCommand(Notifications.PROCESSING_PROCESS_PASTE, new PasteCommand());

		// autocomplete
		registerCommand(Notifications.REQUEST_AUTOCOMPLETE, new AutocompleteCommand());
		
		// requests
		registerCommand(Notifications.REQUEST, new RequestCommand());
		
		// plugins
		registerCommand(Notifications.REQUEST_SPELLCHECK, new RequestSpellcheckCommand());
		registerCommand(Notifications.REQUEST_PULL, new RequestPullCommand());

		// timer
		registerCommand(Notifications.TIMER_START, new TimerStartCommand());
		registerCommand(Notifications.TIMER_SIGNAL_AUTOSAVE, new AutosaveSessionCommand());

		// find
		registerCommand(Notifications.REQUEST_FIND, new FindCommand());
		
		// suggestion lists
		registerCommand(Notifications.FETCH_LISTS, new FetchListsCommand());
		registerCommand(Notifications.SAVE_LISTS, new SaveListsCommand());
		
		registerCommand(Notifications.SUGGESTION_LIST_ENABLE, new EnableSuggestionList());
		registerCommand(Notifications.SUGGESTION_LIST_REMOVE_SUGGESTION, new RemoveSuggestion());
		registerCommand(Notifications.SUGGESTION_LIST_RESET, new ResetSuggestionLists());
		
	 }
	
	
}
