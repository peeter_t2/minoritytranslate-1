package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.ActionException;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class FetchNamespaces extends MWAction {

	private HttpAction action;
	
	private boolean popped = false;
	
	private HashMap<Namespace, String> mappings = new HashMap<>();
	
	
	public FetchNamespaces() {
		action = getAction();
	}
	
	public HttpAction getAction()
	 {
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "query")
		.param("meta", "siteinfo")
		.param("siprop", "namespaces")
		.param("format", "xml")
		.buildGet();
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		if (response.contains("error")) {
			Pattern errFinder = Pattern.compile("<p>(.*?)</p>", Pattern.DOTALL | Pattern.MULTILINE);
			Matcher m = errFinder.matcher(response);
			String lastP = "";
			while (m.find()) {
			  lastP = MediaWiki.urlDecode(m.group(1));
			}

			throw new ActionException("Namespace retrieval failed - " + lastP);
		}
		
		Pattern pElement = Pattern.compile("<ns(.*?)(?:</ns>|/>)");
		Pattern pID = Pattern.compile("id=\"([0-9]+)\"");
		Pattern pName = Pattern.compile("<ns(?:.*?)>(.*?)</ns>");
		
		Matcher m = pElement.matcher(response);
		while (m.find()) {
			
			String element = m.group(0);
			
			String name = "";
			Matcher mName = pName.matcher(element);
			if (mName.find()) name = mName.group(1);
			
			Matcher mID = pID.matcher(m.group(1));
			
			if (mID.find()) {
				
				int id = Integer.parseInt(mID.group(1));
				Namespace ns = Namespace.find(id);
				
				if (ns != null) {
					mappings.put(ns, name);
				}
				
			}
			
		}
		
		return super.processReturningText(response, action);
	 }

	public HashMap<Namespace, String> getMappings() {
		return mappings;
	}
	
	
}
