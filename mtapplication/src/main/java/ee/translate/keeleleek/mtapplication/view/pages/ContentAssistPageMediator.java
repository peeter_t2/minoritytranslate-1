package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;


public abstract class ContentAssistPageMediator extends Mediator implements PreferencesPage<ContentAssistPreferences> {

	public final static String NAME = "{D6790458-869B-43D2-8925-56A682269FD1}";
	
	
	// INIT
	public ContentAssistPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}
	
	
}
