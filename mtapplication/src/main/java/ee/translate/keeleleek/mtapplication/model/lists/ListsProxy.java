package ee.translate.keeleleek.mtapplication.model.lists;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.actions.QueryCategoryAction;
import ee.translate.keeleleek.mtapplication.controller.actions.QueryPageAction;
import ee.translate.keeleleek.mtapplication.controller.actions.RandomList;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.ContentExpanderProxy;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.content.WikiRequest;
import ee.translate.keeleleek.mtapplication.model.requests.HTTPUtil;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class ListsProxy extends ContentExpanderProxy {

	public final static String NAME = "{570A0120-9C36-4CC0-AE03-9CE5ADEE809C}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ListsProxy.class);
	private static String USERLISTS_URL_STR = "http://mwtranslate2.keeleleek.ee/userslists";
	
	public final static String GOOD_ARTICLES_CATEGORY_LIST = "Wikipedia good articles";
	public final static String FEATURED_ARTICLES_CATEGORY_LIST = "Featured articles";
	public final static String RANDOM_LIST_NAME = "Random";
	
	public final static String GROUP_RANDOM = "Random";
	
	
	private UsersList[] usersLists = null;
	
	transient private HashMap<String, QueryPageList> queryPageLists = new HashMap<>();
	transient private HashMap<String, QueryCategoryList> queryCategoryLists = new HashMap<>();
	transient private HashMap<String, CustomList> customLists = new HashMap<>();
	
	transient private HashMap<WikiReference, List<String>> refNameList = new HashMap<>();
	transient private HashMap<String, List<String>> qidNameList = new HashMap<>();
	transient private List<ListSuggestion> suggestions = Collections.synchronizedList(new ArrayList<ListSuggestion>());
	
	transient private HashMap<String, QueryPageAction> queryPageActions = new HashMap<>();
	transient private HashMap<String, QueryCategoryAction> queryCategoryActions = new HashMap<>();
	
	transient private HashSet<String> enabledLists = new HashSet<>();
	transient private HashSet<String> finishedUsersLists = new HashSet<>();
	
	transient private boolean fetching = false;
	transient private boolean clean = true;
	transient private boolean busy = false;
	
	
	// INIT
	public ListsProxy() {
		super(NAME);
/*		
		// query category
		QueryCategoryList clist;

		clist = new QueryCategoryList("en", GOOD_ARTICLES_CATEGORY_LIST);
		queryCategoryLists.put(clist.getName(), clist);
		
		clist = new QueryCategoryList("en", FEATURED_ARTICLES_CATEGORY_LIST);
		queryCategoryLists.put(clist.getName(), clist);
*/
	}
	
	
	// DOWNLOAD
	public void downloadUserlists()
	 {
		try {
			
			String json = HTTPUtil.sendGET(USERLISTS_URL_STR, "");
			Gson gson = new Gson();
			usersLists = gson.fromJson(json, UsersList[].class);
			
		} catch (IOException e) {
			LOGGER.warn("Failed to download userslists!", e);
			return;
		} catch (JsonSyntaxException e) {
			LOGGER.warn("Failed to parse userslists!", e);
			return;
		}
	 }
	
	public void fetchSuggestions()
	 {
		for (String name : enabledLists) {

			fetching = true;
			
			lookBusy();
			
			fetch(findSuggestionList(name));
			
			fetching = false;
			
			lookBusy();
			
			updateClean();
		}

		lookBusy();
		
		work();

		lookBusy();
		
		updateClean();
	 }
	
	private void fetch(SuggestionList list) {
		if (list instanceof UsersList) fetchUsersList((UsersList) list);
		else if (list instanceof QueryPageList) fetchQueryPageList((QueryPageList) list);
		else if (list instanceof QueryCategoryList) fetchQueryCategoryList((QueryCategoryList) list);
		else if (list instanceof CustomList) fetchCustomList((CustomList) list);
	}

	private void fetchUsersList(UsersList list)
	 {
		String name = list.getName();
		
		if (finishedUsersLists.contains(name)) return; // do once for user lists
		finishedUsersLists.add(name);
		
		Article article = MinorityTranslateModel.bots().fetchArticle(list.getLangCode(), list.getTitle());
		String[] sections = article.getText().split("\n=");
		
		String[] sectionTitles = new String[6];
		int depth = 0;
		
		for (int k = 0; k < sections.length; k++) {

			String section = sections[k];
			
			String sectionTitle = "";
			if (k != 0) {
				int i = section.indexOf("=\n");
				if (i == -1) continue;
				sectionTitle = cleanTags(section.substring(0, i));
				depth = StringUtils.countMatches(sectionTitle, "=") / 2;
				adjustLastValue(sectionTitles, depth, sectionTitle.replace('=', ' ').trim());
			}
			
			Pattern p = Pattern.compile("\\[\\[(.*?)\\]\\]");
			Matcher m = p.matcher(section);

			while (m.find()) {
				
				String link = m.group(1);
				String langCode = list.getLangCode();

				String[] split = link.split(":");
				
				// regular article
				if (split.length == 1) {

					// trim link
					int j = link.indexOf('|');
					if (j != -1) link = link.substring(0, j);

					// invalid language code
					if (!MinorityTranslateModel.wikis().isLangCode(langCode)) continue;
					
					// add
					WikiReference ref = new WikiReference(langCode, Namespace.ARTICLE, link);
					articles.add(ref);
					refNameList.put(ref, mashStrings(sectionTitles, depth));
					
				}
				
				// article
				if (split.length == 3 && split[0].isEmpty()) {
					
					langCode = split[1];
					link = split[2];

					// trim link
					int j = link.indexOf('|');
					if (j != -1) link = link.substring(0, j);

					// invalid language code
					if (!MinorityTranslateModel.wikis().isLangCode(langCode)) continue;
					
					// add
					WikiReference ref = new WikiReference(langCode, Namespace.ARTICLE, link);
					articles.add(ref);
					refNameList.put(ref, mashStrings(sectionTitles, depth));
					
				}
				
				// quid
				if (split.length == 2 && split[0].equals("d")) {
					
					link = split[1];

					// trim link
					int j = link.indexOf('|');
					if (j != -1) link = link.substring(0, j);
					
					// add
					qids.add(link);
					qidNameList.put(link, mashStrings(sectionTitles, depth));
					
				}
				
				// unknown
				else {
					continue;
				}
				
			}
		}
	 }

	private void fetchQueryPageList(QueryPageList list)
	 {
		QueryPageAction action = fetchQueryPageAction(list);
		if (action == null) return;
		
		MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(list.getLangCode());
		bot.getPerformedAction(action);

		ArrayList<String> nameList = new ArrayList<>();
		nameList.add(list.getName());
		
		String langCode = list.getLangCode();
		List<String> titles = action.getResult();
		for (String title : titles) {
			WikiReference ref = new WikiReference(langCode, Namespace.ARTICLE, title);
			articles.add(ref);
			refNameList.put(ref, nameList);
		}
	 }

	private void fetchQueryCategoryList(QueryCategoryList list)
	 {
		QueryCategoryAction action = fetchQueryCategoryAction(list);
		if (action == null) return;
		
		MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(list.getLangCode());
		bot.getPerformedAction(action);

		ArrayList<String> nameList = new ArrayList<>();
		nameList.add(list.getName());
		
		String langCode = list.getLangCode();
		List<String> titles = action.getArticles();
		for (String title : titles) {
			WikiReference ref = new WikiReference(langCode, Namespace.ARTICLE, title);
			articles.add(ref);
			refNameList.put(ref, nameList);
		}
	 }

	private void fetchCustomList(CustomList list)
	 {
		switch (list.getTitle()) {
		case RANDOM_LIST_NAME:

			RandomList action = new RandomList();
			MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(list.getLangCode());
			bot.getPerformedAction(action);
			
			ArrayList<String> nameList = new ArrayList<>();
			nameList.add(list.getName());
			
			String langCode = list.getLangCode();
			List<String> titles = action.getResult();
			for (String title : titles) {
				WikiReference ref = new WikiReference(langCode, Namespace.ARTICLE, title);
				articles.add(ref);
				refNameList.put(ref, nameList);
			}
			break;

		default:
			break;
		}
	 }

	@Override
	protected void workConclude(HashSet<WikiRequest> expanded)
	 {
		int qguard = guard;
		
		for (WikiRequest expand : expanded) {
			
			if (qguard == guard) {
				
				List<String> nameList = refNameList.remove(expand.getOriginal());
				if (nameList == null) nameList = qidNameList.remove(expand.getQid());
				if (nameList == null) nameList = new ArrayList<>();
				if (nameList.isEmpty()) nameList.add(Messages.getString("lists.uncategorized.branch"));
				
				ListSuggestion suggestion = new ListSuggestion(expand, nameList);
				addSuggestion(suggestion);
				
			}
			
		}
	 }

	private void lookBusy()
	 {
		boolean busy = articles.size() > 0 || categories.size() > 0 || redirected.size() > 0 || qids.size() > 0 || expanded.size() > 0 || fetching;
		
		if (this.busy == busy) return;
		
		this.busy = busy;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SUGGESTION_LIST_BUSY, busy);
	 }
	
	public boolean isBusy() {
		return busy;
	}
	
	
	// RESET
	public void cancel() {
		super.cancel();
	}
	
	public void reset()
	 {
		super.cancel();
		
		refNameList.clear();
		qidNameList.clear();
		suggestions.clear();
		
		queryPageActions = new HashMap<>();
		queryCategoryActions = new HashMap<>();
		
		finishedUsersLists.clear();
		
		updateClean();
	 }
	
	public boolean isClean() {
		return clean;
	}
	
	private boolean findClean()
	 {
		if (refNameList.size() > 0) return false;
		if (qidNameList.size() > 0) return false;
		if (suggestions.size() > 0) return false;
		
		if (queryPageActions.size() > 0) return false;
		if (queryCategoryActions.size() > 0) return false;
		
		if (finishedUsersLists.size() > 0) return false;
		
		return true;
	 }
	
	private void updateClean()
	 {
		boolean oldClean = this.clean;
		this.clean = findClean();
		if (oldClean != this.clean) MinorityTranslateModel.notifier().sendNotification(Notifications.SUGGESTION_LIST_CLEAN_CHANGED, this.clean);
	 }
	
	
	
	// SUGGESTION LISTS
	public List<SuggestionList> collectSuggestionLists()
	 {
		ArrayList<SuggestionList> result = new ArrayList<>();
		
		// users lists
		if (usersLists != null) {
			for (UsersList usersList : usersLists) {
				result.add(usersList);
			}
		}
		
		String[] langCodes = MinorityTranslateModel.preferences().getSrcLangCodes();
		QueryPageList qlist;
		CustomList clist;
		for (String langCode : langCodes) {
			
			// query page
			qlist = new QueryPageList(langCode, QueryPageAction.MOST_LINKED_LIST);
			queryPageLists.put(qlist.getName(), qlist);
			result.add(qlist);
			
			qlist = new QueryPageList(langCode, QueryPageAction.MOST_INTERWIKIS_LIST);
			queryPageLists.put(qlist.getName(), qlist);
			result.add(qlist);
			
			qlist = new QueryPageList(langCode, QueryPageAction.MOST_CATEGORIES_LIST);
			queryPageLists.put(qlist.getName(), qlist);
			result.add(qlist);
			
			qlist = new QueryPageList(langCode, QueryPageAction.LONG_PAGES_LIST);
			queryPageLists.put(qlist.getName(), qlist);
			result.add(qlist);
			
			// custom
			clist = new CustomList(langCode, RANDOM_LIST_NAME, GROUP_RANDOM);
			customLists.put(clist.getName(), clist);
			result.add(clist);
			
		}

		// query category
		result.addAll(queryCategoryLists.values());
		
		return result;
	 }

	public SuggestionList findSuggestionList(String name)
	 {
		// user lists
		if (usersLists != null)
		for (SuggestionList list : usersLists) {
			if (list.getName().equals(name)) return list;
		}

		// query page lists
		QueryPageList qlist = queryPageLists.get(name);
		if (qlist != null) return qlist;

		// custom lists
		CustomList culist = customLists.get(name);
		if (culist != null) return culist;

		// query category lists
		QueryCategoryList calist = queryCategoryLists.get(name);
		if (calist != null) return calist;
		
		return null;
	 }
	
	public boolean isSuggestionListEnabled(String name) {
		return enabledLists.contains(name);
	}
	
	public void setSuggectionListEnabled(String name, Boolean enabled) {
		if (enabled) enabledLists.add(name);
		else enabledLists.remove(name);
	}
	
	
	// SUGGESTIONS
	public List<ListSuggestion> collectSuggestions()
	 {
		ArrayList<ListSuggestion> result = new ArrayList<>();
		
		for (int i = 0; i < suggestions.size(); i++) {
			result.add(suggestions.get(i));
		}
		
		return result;
	 }
	
	public void addSuggestion(ListSuggestion suggestion) {
		if (!suggestions.contains(suggestion) && suggestions.add(suggestion)) {
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SUGGESTION_LIST_ARTICLE_ADDED, suggestion);
		}
	}
	
	public void removeSuggestion(ListSuggestion suggestion) {
		if (suggestions.remove(suggestion)) 
			getFacade().sendNotification(Notifications.SUGGESTION_LIST_ARTICLE_REMOVED, suggestion);
	}
	
	public int getSuggestionCount() {
		return suggestions.size();
	}
	
	
	// HELPERS
	private QueryPageAction fetchQueryPageAction(QueryPageList list)
	 {
		QueryPageAction action = queryPageActions.get(list.getName());
		
		// first
		if (action == null) {
			action = new QueryPageAction(list.getTitle());
			queryPageActions.put(list.getName(), action);
		}
		
		// next
		else {
			action = action.next();
			if (action == null) return null; // all done
			queryPageActions.put(list.getName(), action);
		}

		return action;
	 }
	
	private QueryCategoryAction fetchQueryCategoryAction(QueryCategoryList list)
	 {
		QueryCategoryAction action = queryCategoryActions.get(list.getName());
		
		// first
		if (action == null) {
			action = new QueryCategoryAction(list.getTitle());
			queryCategoryActions.put(list.getName(), action);
		}
		
		// next
		else {
			action = action.next();
			if (action == null) return null; // all done
			queryCategoryActions.put(list.getName(), action);
		}

		return action;
	 }

	private static List<String> mashStrings(String[] strings, int lastIndex) {
		ArrayList<String> result = new ArrayList<>();
		for (int i = 0; i < strings.length && i <= lastIndex; i++) {
			if (strings[i] == null || strings[i].isEmpty()) continue;
			result.add(strings[i]);
		}
		return result;
	}

	private static void adjustLastValue(String[] strings, int lastIndex, String lastValue) {
		for (int i = lastIndex; i < strings.length; i++) {
			strings[i] = null;
		}
		if (lastIndex < strings.length) strings[lastIndex] = lastValue;
	}
	
	private static String cleanTags(String string)
	 {
		char[] chars = string.toCharArray();
		boolean tag = false;
		for (int i = 0; i < chars.length; i++) {
			
			if (chars[i] == '<') {
				tag = true;
				chars[i] = ' ';
				continue;
			}

			if (chars[i] == '>') {
				tag = false;
				chars[i] = ' ';
				continue;
			}
			
			if (tag) chars[i] = ' ';
			
		}
		return new String(chars);
	 }
	

}
