package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.javafx.helpers.RemappedStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ContentRequestFX {

	private final StringProperty title;
	private final StringProperty language;
	private final IntegerProperty namespace;
	private final StringProperty namespaceStr;
	
	
	public ContentRequestFX(ContentRequest request)
	 {
		this.title = new SimpleStringProperty(request.getTitle());
		
		String language = MinorityTranslateModel.wikis().getLangName(request.getLangCode());
		if (language == null) language = request.getLangCode();
		this.language = new SimpleStringProperty(language);
		
		this.namespace = new SimpleIntegerProperty(request.getNamespace());
		this.namespaceStr = new RemappedStringProperty(namespace, MinorityTranslateModel.wikis().findNamespaceIntegerMappings());
	 }

	public ContentRequestFX() {
		this(new ContentRequest());
	}
	
	public ContentRequest toContentRequest() throws Exception
	 {
		String langCode = MinorityTranslateModel.wikis().findLangCode(language.getValue());
		if (langCode == null) langCode = language.getValue();
		if (!MinorityTranslateModel.wikis().isLangCode(langCode)) throw new Exception(langCode + " is not a valid language code");
		
		return new ContentRequest(langCode, namespace.getValue(), title.getValue());
	 }
	

	public StringProperty title() {
		return title;
	}
	
	public StringProperty language() {
		return language;
	}
	
	public IntegerProperty namespace() {
		return namespace;
	}

	public StringProperty namespaceStr() {
		return namespaceStr;
	}
	
	
}
