package ee.translate.keeleleek.mtapplication.model.plugins;

import java.util.Collection;

import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugins;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import ee.translate.keeleleek.mtpluginframework.spellcheck.SpellerPlugin;

public class DisabledSpellerPlugin implements SpellerPlugin {

	public final static String PLUGIN_NAME = "Disabled";
	public final static PluginVersion PLUGIN_VERSION = new PluginVersion(1, 1);
	
	
	public String getName() {
		return PLUGIN_NAME;
	}
	
	@Override
	public String getName(String langCode) {
		return Messages.getString("plugins.disabled.name");
	}

	public PluginVersion getSpellerVersion() {
		return PLUGIN_VERSION;
	}

	
	@Override
	public void setup(MinorityTranslatePlugins plugins) {
		
	}
	
	@Override
	public void close() {
		
	}
	
	@Override
	public boolean spellCheck(String langCode, String text, Collection<Misspell> misspells) {
		return true;
	}
	
	
}
