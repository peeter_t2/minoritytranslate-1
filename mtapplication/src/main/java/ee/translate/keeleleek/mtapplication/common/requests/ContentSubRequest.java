package ee.translate.keeleleek.mtapplication.common.requests;

public class ContentSubRequest extends ContentRequest {

	private boolean expand;
	private boolean addArticles;
	private boolean addCategories;
	

	public ContentSubRequest(ContentRequest request, boolean expand, boolean addArticles, boolean addCategories)
	 {
		super(request);

		this.expand = expand;
		this.addArticles = addArticles;
		this.addCategories = addCategories;
	 }
	
	public ContentSubRequest(String langCode, Integer namespace, String title, boolean expand, boolean addArticles, boolean addCategories)
	 {
		super(langCode, namespace, title);
		
		this.expand = expand;
		this.addArticles = addArticles;
		this.addCategories = addCategories;
	 }
	
	public boolean isExpand() {
		return expand;
	}
	
	public boolean isAddArticles() {
		return addArticles;
	}
	
	public boolean isAddCategories() {
		return addCategories;
	}

	
	public boolean isAdd()
	 {
		return addArticles || addCategories;
	 }
	
	
}
