package ee.translate.keeleleek.mtapplication.view.application;

import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public abstract class ApplicationMediator extends Mediator {

	public final static String NAME = "{672E7C99-F80C-4523-A8DA-7E99DE2D5DE6}";
	
	protected static Logger LOGGER = LoggerFactory.getLogger(ApplicationMediator.class);

	
	// INITIATION:
	public ApplicationMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister() {
		
	}


	// STATE:
	@Override
	public String[] listNotificationInterests() {
		return new String[]{
			Notifications.WINDOW_TRANSLATE_OPEN,
			Notifications.WINDOW_TRANSLATE_CLOSE,
			Notifications.WINDOW_PREFERENCES_OPEN,
			Notifications.WINDOW_ADD_CATEGORY_OPEN,
			Notifications.WINDOW_ADD_ARTICLE_OPEN,
			Notifications.WINDOW_REMOVE_ARTICLE_OPEN,
			Notifications.WINDOW_ADD_LINKS_OPEN,
			Notifications.WINDOW_LOGIN_OPEN,
			Notifications.WINDOW_ABOUT_OPEN,
			Notifications.WINDOW_MANUAL_OPEN,
			Notifications.WINDOW_LISTS_OPEN,
			Notifications.WINDOW_OPEN_SESSION_OPEN,
			Notifications.WINDOW_SAVE_SESSION_OPEN,
			Notifications.ERROR_MESSAGE,
			Notifications.SESSION_LOADED,
			Notifications.SESSION_SAVED,
			Notifications.SESSION_TAG_CHANGED,
			Notifications.QUIT
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {

		 case Notifications.WINDOW_TRANSLATE_OPEN:
			translateWindowOpen();
			sendNotification(Notifications.WINDOW_TRANSLATE_VISIBLE);
			break;
			
		 case Notifications.WINDOW_TRANSLATE_CLOSE:
			translateWindowClose();
			break;
			 
		 case Notifications.WINDOW_PREFERENCES_OPEN:
			sendNotification(Notifications.WINDOW_PREFERENCES_PREPARE);
			preferencesWindowOpen();
			break;
			
		 case Notifications.WINDOW_ADD_CATEGORY_OPEN:
			addCategoryWindowOpen();
			break;
			
		 case Notifications.WINDOW_ADD_ARTICLE_OPEN:
			addArticleWindowOpen();
			break;
			
		 case Notifications.WINDOW_REMOVE_ARTICLE_OPEN:
			removeArticleWindowOpen();
			break;
			
		 case Notifications.WINDOW_ADD_LINKS_OPEN:
			addLinksWindowOpen();
			break;
			
		 case Notifications.WINDOW_LOGIN_OPEN:
			sendNotification(Notifications.WINDOW_LOGIN_PREPARE);
			loginWindowOpen();
			break;

		 case Notifications.WINDOW_ABOUT_OPEN:
			aboutWindowOpen();
			break;
			
		 case Notifications.WINDOW_MANUAL_OPEN:
			manualWindowOpen();
			break;
			
		 case Notifications.WINDOW_LISTS_OPEN:
			listsWindowOpen();
			break;
			
		 case Notifications.WINDOW_OPEN_SESSION_OPEN:
			sessionOpenWindowOpen();
			break;

		 case Notifications.WINDOW_SAVE_SESSION_OPEN:
			sessionSaveWindowOpen();
			break;

		 case Notifications.ERROR_MESSAGE:
			showMessage(Messages.getString("messages.error"), (String)notification.getBody(), notification.getType());
			break;

		 case Notifications.SESSION_LOADED:
		 case Notifications.SESSION_SAVED:
		 case Notifications.SESSION_TAG_CHANGED:
			String title = Messages.getString("window.translate");

			Path path = MinorityTranslateModel.session().getSessionPath();
			if (path != null) title+= " - " + path.getFileName();
			
			if (MinorityTranslate.NEUTERED) title+= " NEUTERED MODE";
			
			setTitle(title);
			break;

		 case Notifications.QUIT:
			exit();
			break;

		 default:
			break;
		}
	 }
	
	
	// IMPLEMENTATION:
	public abstract Object getWindow();
	protected abstract void setTitle(String title);
	
	protected abstract void translateWindowOpen();
	protected abstract void loginWindowOpen();
	protected abstract void preferencesWindowOpen();
	protected abstract void addArticleWindowOpen();
	protected abstract void addCategoryWindowOpen();
	protected abstract void removeArticleWindowOpen();
	protected abstract void addLinksWindowOpen();
	protected abstract void aboutWindowOpen();
	protected abstract void manualWindowOpen();
	protected abstract void listsWindowOpen();
	protected abstract void sessionOpenWindowOpen();
	protected abstract void sessionSaveWindowOpen();
	
	protected abstract void translateWindowClose();
	
	protected abstract void showMessage(String title, String message, String details);

	public abstract void showDocument(String uri);

	protected abstract void exit();

	
	// SESSIONS:
	protected void onSessionOpenConfirmed(Path path) {
		sendNotification(Notifications.MENU_SESSION_OPEN_CONFIRMED, path);
	}

	protected void onSessionSaveConfirmed(Path path) {
		sendNotification(Notifications.MENU_SESSION_SAVE_CONFIRMED, path);
	}
	
	
}
