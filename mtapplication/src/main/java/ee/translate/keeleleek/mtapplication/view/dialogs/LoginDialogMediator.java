package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;


public abstract class LoginDialogMediator extends Mediator implements ConfirmDeclineDialog {

	public final static String NAME = "{7DDC83A5-46B8-4745-8D92-7244C1F4AEFD}";


	// INITIATION:
	public LoginDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister()
	 {
		
	 }
	
	
	// STATE:
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.SESSION_LOADED,
			Notifications.WINDOW_LOGIN_PREPARE,
			Notifications.CONNECTION_STATUS_CHANGED,
			Notifications.LOGIN_BUSY_CHANGED
		 };
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {
		 case Notifications.SESSION_LOADED:
			break;
			
		 case Notifications.WINDOW_LOGIN_PREPARE:
			break;
			
		 case Notifications.CONNECTION_STATUS_CHANGED:
		 case Notifications.LOGIN_BUSY_CHANGED:
			break;

		 default:
			break;
		}
	 }

	
	// IMPLEMENTATION:
	protected abstract String getUsername();
	protected abstract String getPassword();
	
	protected abstract boolean isAutoLogin();
	
	protected abstract void setUsername(String text);
	protected abstract void setPassword(String text);
	
	
	// INHERIT (CONFIRM DECLINE DIALOG)
	@Override
	public void prepare()
	 {
		
	 }
	
	@Override
	public void focus()
	 {
		
	 }
	
	@Override
	public void confirm()
	 {
		String username = getUsername().trim();
		String password = getPassword().trim();
		
		setUsername("");
		setPassword("");
		
		if (username.isEmpty()) {
			getFacade().sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.login.error"),  Messages.getString("messages.login.error.username.empty"));
			return;
		}

		if (password.isEmpty()) {
			getFacade().sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.login.error"),  Messages.getString("messages.login.error.password.empty"));
			return;
		}
		
		sendNotification(Notifications.MENU_LOGIN_CONFIMED, isAutoLogin(), username + "\n" + password);
	 }
	
	@Override
	public void decline()
	 {
		setUsername("");
		setPassword("");
	 }
	

	
}
