package ee.translate.keeleleek.mtapplication.view.elements;

import java.util.ArrayList;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.LazyCaller;
import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest;
import ee.translate.keeleleek.mtapplication.common.requests.PullResponse;
import ee.translate.keeleleek.mtapplication.common.requests.SpellCheckRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoices;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteRequest;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.filters.FilesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.IntroductionFilter;
import ee.translate.keeleleek.mtapplication.model.filters.ReferencesFilter;
import ee.translate.keeleleek.mtapplication.model.filters.TemplatesFilter;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;


public abstract class EditorMediator extends Mediator {

	// constants
	public final static String NAME = "{0A54F657-F4D6-450F-B43A-D8D8FB4F06AC-${ID}}";
	protected static Logger LOGGER = LoggerFactory.getLogger(EditorMediator.class);
	
	public final static long TITLE_DELAY = 500;
	public final static long TEXT_DELAY = 500;
	public final static long SPELLER_DELAY = 500;
	
	public enum EditorView { EDIT, PREVIEW };
	public enum EditorMode { NORMAL, FILTERED };
	public enum EditorPosition { DESTINATION, SOURCE };
	
	private static int identifier = 0;
	
	// name
	private String name;
	
	// editor
	private EditorView view = EditorView.EDIT;
	private EditorMode mode = EditorMode.NORMAL;
	private Reference ref = new Reference("", ""); // temporary
	private boolean articleNew;
	
	// callers
	private LazyCaller titleCaller;
	private LazyCaller textCaller;
	private LazyCaller spellerCaller;
	
	// TODO
	protected Object tabObject = null; 
	
	
	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public EditorMediator(EditorMode mode)
	 {
		super(NAME.replace("${ID}", "" + identifier++), null);
		this.mode = mode;
	 }
	
	
	
	/* ******************
	 *                  *
	 *   Notification   *
	 *                  *
	 ****************** */
	@Override
	public String[] listNotificationInterests() {
		return new String[]
			{
				Notifications.SESSION_LOADED,
				Notifications.ENGINE_ARTICLE_DOWNLOAD_INITIALISED,
				Notifications.ENGINE_ARTICLE_PROCESSED,
				Notifications.PULL_COMPLETE,
				Notifications.ENGINE_ARTICLE_STATUS_CHANGED,
				Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED,
				Notifications.ENGINE_ARTICLE_PREVIEW_CHANGED,
				Notifications.ENGINE_ARTICLE_FILTERED_PREVIEW_CHANGED,
				Notifications.PREFERENCES_SYMBOLS_CHANGED,
				Notifications.PREFERENCES_LANGUAGES_CHANGED,
				Notifications.ENGINE_ARTICLE_EXACT_CHANGED,
				Notifications.PREFERENCES_FILTERS_CHANGED,
				Notifications.SHOW_AUTOCOMPLETE,
				Notifications.SPELLCHECK_COMPLETE,
				Notifications.QUITTING_COMMIT
			};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		Reference ref;
		
		switch (notification.getName()) {
		
		case Notifications.SESSION_LOADED:
			fetchSymbols();
			fetchFilters();
			fetchFilteredText();
			break;
		
		case Notifications.ENGINE_ARTICLE_DOWNLOAD_INITIALISED:
		case Notifications.ENGINE_ARTICLE_PROCESSED:
			ref = (Reference) notification.getBody();
			if (!Reference.equals(ref, this.ref)) break;
			fetchContent();
			break;

		case Notifications.PULL_COMPLETE:
			PullResponse response = (PullResponse) notification.getBody();
			ref = response.getDstRef();
			if (!Reference.equals(ref, this.ref)) break;
			replaceText(response.getText());
			break;

		case Notifications.ENGINE_ARTICLE_STATUS_CHANGED:
			ref = (Reference) notification.getBody();
			if (!Reference.equals(ref, this.ref)) break;
			fetchStatus();
			break;

		case Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED:
			ref = (Reference) notification.getBody();
			if (!Reference.equals(ref, this.ref)) break;
			fetchTitleStatus();
			break;

		case Notifications.ENGINE_ARTICLE_PREVIEW_CHANGED:
		case Notifications.ENGINE_ARTICLE_FILTERED_PREVIEW_CHANGED:
			ref = (Reference) notification.getBody();
			if (!Reference.equals(ref, this.ref)) break;
			fetchPreview();
			break;

		case Notifications.PREFERENCES_SYMBOLS_CHANGED:
		case Notifications.PREFERENCES_LANGUAGES_CHANGED:
			fetchSymbols();
			break;
			
		case Notifications.ENGINE_ARTICLE_EXACT_CHANGED:
			ref = (Reference) notification.getBody();
			if (!Reference.equals(ref, this.ref)) break;
			fetchExact();
			break;
			
		case Notifications.PREFERENCES_FILTERS_CHANGED:
			fetchFilters();
			fetchFilteredText();
			break;
			
		case Notifications.SHOW_AUTOCOMPLETE:
			AutocompleteChoices autocomplete = (AutocompleteChoices) notification.getBody();
			ref = autocomplete.getRef();
			if (!Reference.equals(ref, this.ref)) break;
			showAutocomplete(autocomplete);
			break;

		case Notifications.SPELLCHECK_COMPLETE:
			ref = (Reference) notification.getBody();
			if (!Reference.equals(ref, this.ref)) break;
			fetchSpellcheck();
			break;

		case Notifications.QUITTING_COMMIT:
			commit();
			break;
			
		default:
			break;
		}
	 }
	
	@Override
	public void onRegister()
	 {
		setTabView(EditorView.EDIT);
		fetchFilters();
	 }


	
	/* ******************
	 *                  *
	 *       Name       *
	 *                  *
	 ****************** */
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	/* ******************
	 *                  *
	 *      Editor      *
	 *                  *
	 ****************** */
	protected EditorMode getMode() {
		return mode;
	}
	
	public EditorView getTabView() {
		return view;
	}
	
	protected abstract void setTabView(EditorView view);

	
	public Reference getReference() {
		return ref;
	}
	
	public void changeReference(String qid, String langCode)
	 {
		Reference ref = (qid != null && langCode != null) ? new Reference(qid, langCode) : null;
		changeReference(ref);
	 }
	
	public void changeReference(final Reference ref)
	 {
		if (Reference.equals(ref, this.ref)) return; // no change
		
		// flush content
		if (titleCaller != null) titleCaller.force();
		if (textCaller != null) textCaller.force();
		
		// change ref and content
		this.ref = ref;
		fetchContent();
		
		// set up new callers
		switch (mode) {
		case NORMAL:
			titleCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					if (ref == null) return;
					sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_TITLE, ref, getTitle());
				}
				
			}, TITLE_DELAY);
			
			textCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					if (ref == null) return;
					onBeforeTextUpdate();
					sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_TEXT, ref, getText());
					onAfterTextUpdate();
				}
				
			}, TEXT_DELAY, true);
			
			spellerCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					if (ref == null) return;
					
					String pluginName = getSpellerPluginName();
					if (pluginName == null) return;
					
					sendNotification(Notifications.REQUEST_SPELLCHECK, new SpellCheckRequest(pluginName, ref));
				}
				
			}, SPELLER_DELAY, true);
			
			
			if (view == EditorView.PREVIEW && ref != null) sendNotification(Notifications.ENGINE_REQUEST_FETCH_PREVIEW, ref);
			
			break;

		case FILTERED:
			titleCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					// DO NOTHING
				}
				
			}, TITLE_DELAY);
			textCaller = new LazyCaller(new Runnable() {
				
				@Override
				public void run() {
					// DO NOTHING
				}
				
			}, TEXT_DELAY);
			
			if (view == EditorView.PREVIEW && ref != null) sendNotification(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, ref);
			
			break;
			
		default:
			break;
		}
		
		onReferenceChanged();
	 }
	
	
	public boolean isReadOnly() {
		return mode != EditorMode.NORMAL;
	}

	
	public boolean isArticleNew() {
		return articleNew;
	}
	
	protected void setArticleNew(boolean articleNew) {
		this.articleNew = articleNew;
	}
	
	public abstract boolean isEditFocused();
	
	
	
	/* ******************
	 *                  *
	 *     Fetching     *
	 *                  *
	 ****************** */
	public void fetchContent()
	 {
		String title = "";
		String text = "";
		String preview = "";
		boolean translationExact = false;
		boolean articleNew = false;
		
		MinorityArticle article = null;
		if (ref != null) article = MinorityTranslateModel.content().getArticle(ref);
		
		if (article != null) {
			
			title = article.getTitle();
				
			switch (mode) {
			case NORMAL:
				text = article.getText();
				preview = article.getPreview();
				break;
				
			case FILTERED:
				text = article.getFilteredText();
				preview = article.getFilteredPreview();
				break;
				
			default:
				break;
			}
			
			translationExact = article.isExact();
			articleNew = article.isNew();
				
		}

		changeReference(ref);
		setTitle(title);
		setText(text);
		setPreview(preview);
		setTranslationExact(translationExact);
		setArticleNew(articleNew);
		
		fetchStatus();
		fetchTitleStatus();
		
		if (spellerCaller != null) spellerCaller.call();
	 }

	public void fetchStatus()
	 {
		Status status = MinorityTranslateModel.content().getStatus(ref);
		setStatus(status);
	 }

	public void fetchTitleStatus()
	 {
		TitleStatus status = MinorityTranslateModel.content().getTitleStatus(ref);
		setTitleStatus(status);
	 }
	
	public void fetchTitle()
	 {
		String title = MinorityTranslateModel.content().getTitle(ref);
		if (title == null) title = "";
		setTitle(title);
	 }
	
	public void fetchFilteredText()
	 {
		if (mode != EditorMode.FILTERED) return;
		
		String text = MinorityTranslateModel.content().getFilteredText(ref);;
		if (text == null) text = "";
		
		setText(text);
	 }

	public void fetchPreview()
	 {
		String preview = null;
		
		switch (mode) {
		case NORMAL:
			preview = MinorityTranslateModel.content().getPreview(ref);
			break;
			
		case FILTERED:
			preview = MinorityTranslateModel.content().getFilteredPreview(ref);
			break;
			
		default:
			break;
		}
		
		if (preview == null) preview = "";
		
		setPreview(preview);
	 }
	
	public void fetchFilters()
	 {
		switch (mode) {
		case NORMAL:
			break;
			
		case FILTERED:
			if (view == EditorView.PREVIEW && ref != null) sendNotification(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, ref);
			setFilterTemplates(MinorityTranslateModel.preferences().program().isTemplatesFilter());
			setFilterFiles(MinorityTranslateModel.preferences().program().isFilesFilter());
			setFilterIntroduction(MinorityTranslateModel.preferences().program().isIntroductionFilter());
			setFilterReferences(MinorityTranslateModel.preferences().program().isReferencesFilter());
			break;
			
		default:
			break;
		}
	}

	public void fetchExact()
	 {
		if (ref == null) return;
		
		switch (mode) {
		case NORMAL:
		case FILTERED:
			boolean exact = MinorityTranslateModel.content().isExact(ref);
			setTranslationExact(exact);
			break;
			
		default:
			break;
		}
	}

	public void fetchSymbols()
	 {
		if (ref == null) return;
		ArrayList<Symbol> symbols = MinorityTranslateModel.preferences().getSymbols().getSymbols(ref.getLangCode());
		setSymbols(symbols);
	 }

	public void fetchSpellcheck()
	 {
		markMisspells(MinorityTranslateModel.speller().getMisspells(getReference()));
	 }

	

	/* ******************
	 *                  *
	 *      Commit      *
	 *                  *
	 ****************** */
	public void commit()
	 {
		switch (mode) {
		case NORMAL:
			if (titleCaller != null) titleCaller.force();
			if (textCaller != null) textCaller.force();
			break;
			
		case FILTERED:
			break;
			
		default:
			break;
		}
	 }
	
	
	
	/* ******************
	 *                  *
	 *     Content      *
	 *                  *
	 ****************** */
	public abstract String getTitle();
	protected abstract void setTitle(String title);
	
	public abstract String getText();
	public abstract void replaceText(String text); // replace allows undo
	public abstract void pasteText(String text);
	protected abstract void setText(String text);
	
	protected abstract void setStatus(Status status);
	protected abstract void setTitleStatus(TitleStatus status);
	
	protected abstract void setPreview(String html);
	
	protected abstract void setTranslationExact(boolean exact);

	protected abstract void setFilterTemplates(boolean filter);
	protected abstract void setFilterFiles(boolean filter);
	protected abstract void setFilterIntroduction(boolean filter);
	protected abstract void setFilterReferences(boolean filter);
	
	
	
	/* ******************
	 *                  *
	 *   Autocomplete   *
	 *                  *
	 ****************** */
	public abstract void showAutocomplete(AutocompleteChoices autocomplete);
	
	
	
	/* ******************
	 *                  *
	 *     Spelling     *
	 *                  *
	 ****************** */
	public abstract String getSpellerPluginName();
	public abstract void markMisspells(List<Misspell> misspells);
	
	

	/* ******************
	 *                  *
	 *     Symbols      *
	 *                  *
	 ****************** */
	protected abstract void setSymbols(List<Symbol> symbols);

	
	
	/* ******************
	 *                  *
	 *   Find/Replace   *
	 *                  *
	 ****************** */
	public abstract void findReplace(FindReplaceRequest request);
	
	
	
	/* ******************
	 *                  *
	 *      Events      *
	 *                  *
	 ****************** */
	public void onBeforeTextUpdate()
	 {
		
	 }
	
	public void onAfterTextUpdate()
	 {
		
	 }
	
	public void onReferenceChanged()
	 {

	 }
	
	
	public void onTitleEdited() {
		titleCaller.call();
	}
	
	public void onTextEdited() {
		textCaller.call();
		spellerCaller.call();
	}
	
	public void onChangeView(EditorView view)
	 {
		this.view = view;
		
		setTabView(this.view);
		
		switch (mode) {
		case NORMAL:
			switch (this.view)
			 {
				case PREVIEW:
					if (ref != null) {
						commit();
						sendNotification(Notifications.ENGINE_REQUEST_FETCH_PREVIEW, ref);
					}
					break;
	
				default:
					break;
			 }
			
			break;
			
		case FILTERED:
			switch (this.view)
			 {
				case PREVIEW:
					if (ref != null) {
						commit();
						sendNotification(Notifications.ENGINE_REQUEST_FETCH_FILTERED_PREVIEW, ref);
					}
					break;
	
				default:
					break;
			 }
			
			break;

		default:
			break;
		}
		
	 }

	public void onChangeExact(boolean exact)
	 {
		String truefalse = exact ? "true" : "false";
		sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_EXACT, ref, truefalse);
	 }

	public void onChangeTemplatesFilter(boolean filter)
	 {
		sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, TemplatesFilter.NAME);
		sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onChangeFilesFilter(boolean filter)
	 {
		sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, FilesFilter.NAME);
		sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onChangeIntroductionFilter(boolean filter)
	 {
		sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, IntroductionFilter.NAME);
		sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onChangeReferencesFilter(boolean filter)
	 {
		sendNotification(Notifications.PREFERENCES_CHANGE_FILTER, filter, ReferencesFilter.NAME);
		sendNotification(Notifications.PREFERENCES_SAVE);
	 }

	public void onAutocomplete(AutocompleteRequest request)
	 {
		sendNotification(Notifications.REQUEST_AUTOCOMPLETE, request);
	 }

	public void onSpellCheckRequest()
	 {
		if (ref == null) 	return;
		
		String pluginName = getSpellerPluginName();
		if (pluginName == null) return;
		
		sendNotification(Notifications.REQUEST_SPELLCHECK, new SpellCheckRequest(pluginName, ref));
	 }

	public void onReset()
	 {
		Reference ref = getReference();
		if (ref == null) return;
		
		commit();
		
		sendNotification(Notifications.ENGINE_RESET_ARTICLE, ref);
	 }
	

}
