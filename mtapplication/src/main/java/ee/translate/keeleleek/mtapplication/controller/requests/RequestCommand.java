package ee.translate.keeleleek.mtapplication.controller.requests;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class RequestCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		MinorityTranslateModel.requests().fulfill(notification.getBody());
	 }

}
