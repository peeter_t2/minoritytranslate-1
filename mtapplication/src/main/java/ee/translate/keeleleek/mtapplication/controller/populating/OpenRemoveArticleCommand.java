package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;

public class OpenRemoveArticleCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		getFacade().sendNotification(Notifications.WINDOW_REMOVE_ARTICLE_OPEN, true);
	 }
	
}
