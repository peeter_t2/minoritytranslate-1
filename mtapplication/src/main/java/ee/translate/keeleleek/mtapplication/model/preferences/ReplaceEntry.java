package ee.translate.keeleleek.mtapplication.model.preferences;

import ee.translate.keeleleek.mtapplication.model.processing.RegexReplace;

public class ReplaceEntry implements TableEntry {

	public final static String PARAMETER = "#";
	public final static String REGEX_PREFIX = "REGEX:";
	
	private EntryFilter filter;
	private String find;
	private String replace;
	

	// INIT
	public ReplaceEntry()
	 {
		this.filter = new EntryFilter();
		this.find = "";
		this.replace = "";
	 }

	public ReplaceEntry(EntryFilter filter, String find, String replace)
	 {
		this.filter = filter;
		this.find = find;
		this.replace = replace;
	 }

	public ReplaceEntry(ReplaceEntry other)
	 {
		this.filter = other.filter;
		this.find = other.find;
		this.replace = other.replace;
	 }

	
	// REGEX
	public RegexReplace createRegex()
	 {
		if (find.startsWith(REGEX_PREFIX)) return new RegexReplace(find.substring(REGEX_PREFIX.length()), replace);
		String rgxFind = find;
		
		if (rgxFind.startsWith("_")) rgxFind = "(?<=\\s|\\p{Punct} )" + "\\Q" + rgxFind.substring(1, rgxFind.length());
		else if (rgxFind.startsWith("#")) rgxFind = "^" + "\\Q" + rgxFind;
		else rgxFind = "\\Q" + rgxFind; // treat as one (start)

		if (rgxFind.endsWith("_")) rgxFind = rgxFind.substring(0, rgxFind.length() - 1) + "\\E" + "(?=\\s|\\p{Punct} )";
		else if (rgxFind.endsWith("#")) rgxFind = rgxFind + "\\E" + "$";
		else rgxFind = rgxFind + "\\E"; // treat as one (end)
		
		rgxFind = rgxFind.replace("#", "\\E(.*?)\\Q"); // capture params
		
		rgxFind = rgxFind.replace("~", "\\E(?:.*?)\\Q"); // anything between
		
		rgxFind = rgxFind.replace("\\Q\\E", ""); // clean
		
		return new RegexReplace(rgxFind, replace);
	 }

	
	// DATA
	public EntryFilter getFilter() {
		return filter;
	}

	public void setFilter(EntryFilter filter) {
		this.filter = filter;
	}

	public String getFind() {
		return find;
	}

	public void setFind(String find) {
		this.find = find;
	}

	public String getReplace() {
		return replace;
	}

	public void setReplace(String replace) {
		this.replace = replace;
	}

	
	// CONVERT
	@Override
	public String toStringEntry(int c)
	 {
		switch (c) {
		case 0:
			String src = getFilter().getSrcLangRegex();
			String dst = getFilter().getDstLangRegex();
			return src + " ; " + dst;
			
		case 1:
			return getFind().replace("\n", "\\n");
			
		case 2:
			return getReplace().replace("\n", "\\n");

		default:
			return null;
		}
	 }
	
	@Override
	public void fromStringEntry(int c, String value)
	 {
		switch (c) {
		case 0:
			String[] split = value.split(";", 2);
			String src = split[0].trim();
			String dst = split.length == 2 ? split[1].trim() : null;
			setFilter(new EntryFilter(src, dst));
			break;
			
		case 1:
			setFind(value.replace("\\n", "\n"));
			break;
			
		case 2:
			setReplace(value.replace("\\n", "\n"));
			break;

		default:
			return;
		}
	 }
	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result + ((find == null) ? 0 : find.hashCode());
		result = prime * result + ((replace == null) ? 0 : replace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ReplaceEntry other = (ReplaceEntry) obj;
		if (filter == null) {
			if (other.filter != null) return false;
		} else if (!filter.equals(other.filter)) return false;
		if (find == null) {
			if (other.find != null) return false;
		} else if (!find.equals(other.find)) return false;
		if (replace == null) {
			if (other.replace != null) return false;
		} else if (!replace.equals(other.replace)) return false;
		return true;
	}

	
}
