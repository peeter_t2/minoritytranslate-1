package ee.translate.keeleleek.mtapplication.view.messages;

import java.beans.Beans;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class Messages {
	
	static Logger LOGGER = LoggerFactory.getLogger(Messages.class);

	private static final String[] JAR_BUNDLE_LANG_CODES = new String[]{"en", "et", "fiu-vro", "ru"};
	
	public static final String I18N_NAME = "i18n";
	public static final String BUNDLE_NAME = "messages";
	
	private static String languageCode = "";
	private static ResourceBundle resourceBundle = null;
	
	private static boolean init = true;
	private static HashMap<String, ResourceBundle> resourceBundles = new HashMap<>();
	private static String[] langCodes;
	
	
	public static void init ()
	{
		File i18nPath = new File(MinorityTranslate.ROOT_PATH + "/" + I18N_NAME);
		
		// from directory
		if (i18nPath.isDirectory()) {
			
			FileFilter sessionFilter = new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if (!pathname.isFile()) return false;
					String name = pathname.getName();
					Pattern p = Pattern.compile("messages_(.+?).properties");
					Matcher m = p.matcher(name);
					return m.find();
				}
			};
			
			File[] bundlePaths = i18nPath.listFiles(sessionFilter);
			for (File bundlePath : bundlePaths) {
				
				String langCode = bundlePath.getName().replace("messages_", "").replace(".properties", "");
				ResourceBundle bundle = new MessagesBundle(langCode);
				resourceBundles.put(langCode, bundle);
				
			}
			
		} else {
			LOGGER.info("Localisation messages directory '" + i18nPath.getAbsolutePath() + "' not found.");
		}
		
		// from jar
		for (int i = 0; i < JAR_BUNDLE_LANG_CODES.length; i++) {
			
			String langCode = JAR_BUNDLE_LANG_CODES[i];
			if (resourceBundles.containsKey(langCode)) continue;
			ResourceBundle bundle = new MessagesBundle(langCode);
			resourceBundles.put(langCode, bundle);
			
		}
		
		// language codes
		langCodes = resourceBundles.keySet().toArray(new String[resourceBundles.size()]);
		Arrays.sort(langCodes);
		
		init = false;
	 }
	
	public static String getString(String key)
	 {
		if (init) init();
		
		return getResourceBundle().getString(key);
	 }
	
	
	public static ResourceBundle getResourceBundle()
	 {
		if (Beans.isDesignTime()) return ResourceBundle.getBundle(BUNDLE_NAME);
		
		if (init) init();
		
		if (languageCode.equals(MinorityTranslateModel.preferences().program().getGUILangCode())) return resourceBundle;
		
		languageCode = MinorityTranslateModel.preferences().program().getGUILangCode();
		resourceBundle = resourceBundles.get(languageCode);
		
		return resourceBundle;
	 }

	public static String[] getLangCodes()
	 {
		if (init) init();
		return langCodes;
	 }
	
}
