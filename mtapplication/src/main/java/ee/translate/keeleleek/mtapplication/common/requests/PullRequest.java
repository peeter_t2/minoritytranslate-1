package ee.translate.keeleleek.mtapplication.common.requests;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class PullRequest {

	private String pluginName;
	private Reference srcRef;
	private Reference dstRef;
	
	
	public PullRequest(String pluginName, Reference srcRef, Reference dstRef) {
		super();
		this.pluginName = pluginName;
		this.srcRef = srcRef;
		this.dstRef = dstRef;
	}
	

	public String getPluginName() {
		return pluginName;
	}
	
	public Reference getSrcRef() {
		return srcRef;
	}
	
	public Reference getDstRef() {
		return dstRef;
	}
	
	
}
