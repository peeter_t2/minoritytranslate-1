package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.CorpusPreferences;

public class ChangePreferencesCorpusCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		CorpusPreferences preferences = (CorpusPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeCorpus(preferences);
	 }
	
}
