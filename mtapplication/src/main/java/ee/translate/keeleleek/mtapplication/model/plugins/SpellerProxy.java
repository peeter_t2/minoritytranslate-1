package ee.translate.keeleleek.mtapplication.model.plugins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.SpellCheckRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtpluginframework.spellcheck.Misspell;
import ee.translate.keeleleek.mtpluginframework.spellcheck.SpellerPlugin;

public class SpellerProxy extends Proxy {
	
	public final static String NAME = "{32E280C0-B06D-4A23-B48A-9B8F6EB291F0}";
	
	private ArrayList<SpellerPlugin> spellerPlugins = new ArrayList<>();
	private HashMap<Reference,List<Misspell>> allMisspells = new HashMap<>();
	
	int sid = 0;
	
	
	// INIT
	public SpellerProxy() {
		super(NAME, "");
	}

	void addPlugin(SpellerPlugin plugin) {
		spellerPlugins.add(plugin);
	}
	
	
	// PLUGINS
	public List<String> getPluginNames(String langCode)
	 {
		ArrayList<String> result = new ArrayList<>();
		for (SpellerPlugin plugin : spellerPlugins) {
			result.add(plugin.getName(langCode));
		}
		return result;
	 }
	
	public SpellerPlugin getPlugin(String pluginName)
	 {
		for (SpellerPlugin plugin : spellerPlugins) {
			if (plugin.getName().equals(pluginName)) return plugin;
		}
		return null;
	 }

	public SpellerPlugin getPlugin(String langCode, String pluginName)
	 {
		for (SpellerPlugin plugin : spellerPlugins) {
			if (plugin.getName(langCode).equals(pluginName)) return plugin;
		}
		return null;
	 }
	
	
	// SPELLCHECK
	public void requestSpellCheck(SpellCheckRequest request) throws Exception
	 {
		String pluginName = request.getPluginName();
		Reference ref = request.getRef();
		
		if (ref == null) return;
		
		sid++;
		int rsid = sid;
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SPELL_CHECKER_BUSY, true);
		
		MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
		if (article == null) return;
		
		spellCheckArticle(pluginName, article);
		
		if (rsid != sid) return; // another request
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SPELL_CHECKER_BUSY, false);
	 }
	
	public void spellCheckArticle(String pluginName, MinorityArticle article) throws Exception
	 {
		String guiLangCode = MinorityTranslateModel.preferences().getGUILangCode();
		
		SpellerPlugin plugin = getPlugin(guiLangCode, pluginName);
		if (plugin == null) return;

		String langCode = article.getRef().getLangCode();
		
		ArrayList<Misspell> misspells = new ArrayList<>();
		
		boolean success = plugin.spellCheck(langCode, article.getText(), misspells);
		
		if (success) {
			allMisspells.put(article.getRef(), misspells);
			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SPELLCHECK_COMPLETE, article.getRef());
		}
	 }
	
	public List<Misspell> getMisspells(Reference ref)
	 {
		return allMisspells.get(ref);
	 }
	
	
	
}
