package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.VisualPreferences;

public class ChangePreferencesVisualCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		VisualPreferences preferences = (VisualPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeVisual(preferences);
	 }
	
}
