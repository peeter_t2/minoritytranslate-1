package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtpluginframework.mapping.PullMapping;


public abstract class PullMappingsDialogMediator extends Mediator implements ConfirmDeclineDialog {

	public final static String NAME = "{19EA4A6A-92C4-4D68-B0EA-09CA3E52D331}";
	
	
	// INIT
	public PullMappingsDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}
	
	// INHERIT (CONFIRM DECLINE DIALOG)
	public void prepare() {
		
	};
	
	@Override
	public void focus() {
		
	}
	
	@Override
	public void confirm() {
		applyMappings();
	}
	
	@Override
	public void decline() {
		
	}
	
	
	// INHERIT (PROGRESS)
	public abstract void setCurrent(int current);
	public abstract void setTotal(int total);
	public abstract void setAction(String action);
	public abstract void setDone();
	public abstract boolean isSaveTemplateMappings();
	
	// INHERIT (MAPPING)
	public abstract void addMapping(PullMapping mapping);
	public abstract void applyMappings();
	
	
	// EVENTS
	public void onCancel() {
		
	}

	

	
}
