package ee.translate.keeleleek.mtapplication.model.media;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Articon;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;

public abstract class IconsProxy extends Proxy {

	public final static String NAME = "{ACDA2143-5CDF-4907-BC5D-4565EC497B4D}";

	
	// INIT
	public IconsProxy() {
		super(NAME, null);
	}

	
	// INHERIT (ICONS)
	public abstract Object getStatusIcon(Status status);

	public abstract Object getArticon(Articon articon);
	
	
}
