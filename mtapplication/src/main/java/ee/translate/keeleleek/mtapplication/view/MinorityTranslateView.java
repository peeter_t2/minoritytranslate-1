package ee.translate.keeleleek.mtapplication.view;

import org.puremvc.java.multicore.core.view.View;
import org.puremvc.java.multicore.interfaces.IMediator;

public class MinorityTranslateView extends View {

	// INIT
	public synchronized static View getInstance(String key)
	 {
		if (instanceMap.get(key) == null) new MinorityTranslateView(key);
		return instanceMap.get(key);
	 }
	
	protected MinorityTranslateView(String key) {
		super(key);
	}

	@Override
	protected void initializeView()
	 {
		
	 }
	
	@Override
	public void registerMediator(IMediator mediator)
	 {
		String mediatorName = mediator.getMediatorName();
		if (hasMediator(mediatorName)) removeMediator(mediatorName);
		
		super.registerMediator(mediator);
	 }
	
	
}
