package ee.translate.keeleleek.mtapplication.model.gui;

import javafx.application.Platform;

public class GUINotifierProxyFX extends GUINotifierProxy {

	
	// INIT:
	public GUINotifierProxyFX() {
		super();
	}

	
	// NOTIFICATIONS:
	@Override
	public void sendNotificationThreadSafe(final String name, final Object body, final String type) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				sendNotification(name, body, type);
			}
		});
	}
	
	@Override
	public void sendNotificationThreadSafe(final String name, final Object body) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				sendNotification(name, body);
			}
		});
	}
	
	@Override
	public void sendNotificationThreadSafe(final String name) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				sendNotification(name);
			}
		});
	}
	
	@Override
	public void doThreadSafe(Runnable runnable) {
		Platform.runLater(runnable);
	}
	
}
