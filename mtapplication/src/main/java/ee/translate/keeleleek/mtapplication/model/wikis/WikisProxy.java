package ee.translate.keeleleek.mtapplication.model.wikis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.controller.actions.ExtractIncubatorsAction;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.bots.BotsProxy;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class WikisProxy extends Proxy {

	public final static String NAME = "{EE9722FB-414C-4D74-82DF-DFA61A052701}";
	
	public final static String INCUBATOR_LIST_TITLE = "Incubator:Wikis";
	public final static String INCUBATOR_LANG_CODE = "incubator";
	public final static String INCUBATOR_PREFIX = "Wp/#langCode/";
	
	public final static int RESULT_COUNT = 10;

	private static Logger LOGGER = LoggerFactory.getLogger(WikisProxy.class);

	// languages
	private HashMap<String, String> langNames = new HashMap<>();
	private HashMap<String, WikiType> wikiTypes = new HashMap<>();
	transient private String[] langNameList = new String[0];

	// namespaces
	private HashMap<String, HashMap<Namespace, String>> namespaces = new HashMap<>();

	// updating
	private Long lastWikisUpdate = null;
	
	
	// INIT
	public WikisProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		// special types
		langNames.put(BotsProxy.META_LANG_CODE, "meta");
		wikiTypes.put(BotsProxy.META_LANG_CODE, WikiType.REGULAR);
		
		// languages
		langNameList = new String[langNames.size()];
		langNameList = langNames.values().toArray(langNameList);
		Arrays.sort(langNameList);
		
		// remove incomplete
		Iterator<Entry<String, HashMap<Namespace, String>>> it = namespaces.entrySet().iterator();
		while (it.hasNext()) {
			
			Entry<String, HashMap<Namespace, String>> element = it.next();
			
			HashMap<Namespace, String> mappings = element.getValue();
			Namespace[] values = Namespace.values();
			for (Namespace namespace : values) {
				if (!mappings.containsKey(namespace)) {
					it.remove();
					LOGGER.warn("Removing namespace mappings for " + element.getKey() + " because the mapping for " + namespace + " is missing!");
					break;
				}
			}
			
		}

		// en namespace if fallback and must always exist
		if (!namespaces.containsKey("en")) {
			HashMap<Namespace, String> mappings = new HashMap<>();
			mappings.put(Namespace.ARTICLE, "");
			mappings.put(Namespace.TALK, "Talk");
			mappings.put(Namespace.USER, "User");
			mappings.put(Namespace.USER_TALK, "User talk");
			mappings.put(Namespace.WIKIPEDIA, "Wikipedia");
			mappings.put(Namespace.WIKIPEDIA_TALK, "Wikipedia talk");
			mappings.put(Namespace.FILE, "File");
			mappings.put(Namespace.FILE_TALK, "File talk");
			mappings.put(Namespace.MEDIAWIKI, "MediaWiki");
			mappings.put(Namespace.MEDIAWIKI_TALK, "MediaWiki talk");
			mappings.put(Namespace.TEMPLATE, "Template");
			mappings.put(Namespace.TEMPLATE_TALK, "Template talk");
			mappings.put(Namespace.HELP, "Help");
			mappings.put(Namespace.HELP_TALK, "Help talk");
			mappings.put(Namespace.CATEGORY, "Category");
			mappings.put(Namespace.CATEGORY_TALK, "Category talk");
			namespaces.put("en", mappings);
		}

		// meta mappings
		if (!namespaces.containsKey(BotsProxy.META_LANG_CODE)) {
			namespaces.put(BotsProxy.META_LANG_CODE, namespaces.get("en"));
		}
		
		// bugfix
		langNames.remove("nb");
	 }

	/**
	 * Downloads new languages.
	 */
	public void download()
	 {
		try {
			
			this.langNames.clear();
			this.wikiTypes.clear();
			
			downloadWikis();
			downloadIncubators();
			
			lastWikisUpdate = System.currentTimeMillis();
			
		} catch (IllegalStateException e) {
			LOGGER.warn("Failed to download languages: " + e.getMessage() + "!");
		}

		lastWikisUpdate = System.currentTimeMillis();
	 }
	
	/**
	 * Downloads new wiki languages.
	 * 
	 */
	private void downloadWikis()
	 {
		HashMap<String, String> wikiLangNames = MinorityTranslateModel.bots().retrieveLanguages();
		
		this.langNames.putAll(wikiLangNames);
		
		Set<String> wikiLangCodes = wikiLangNames.keySet();
		for (String wikiLangCode : wikiLangCodes) {
			wikiTypes.put(wikiLangCode, WikiType.REGULAR);
		}
	 }
	

	/**
	 * Downloads new incubator languages.
	 * 
	 */
	private void downloadIncubators()
	 {
		MediaWikiBot incuBot = MinorityTranslateModel.bots().getIncubatorBot();
		
		Article article = incuBot.getArticle(INCUBATOR_LIST_TITLE);
		String text = article.getText();
		
		ExtractIncubatorsAction action = new ExtractIncubatorsAction(text);
		incuBot.getPerformedAction(action);
		
		HashMap<String, String> incuLangNames = action.getLangNames();
		this.langNames.putAll(incuLangNames);
		
		Set<String> incuLangCodes = incuLangNames.keySet();
		for (String incuLangCode : incuLangCodes) {
			wikiTypes.put(incuLangCode, WikiType.INCUBATOR);
		}
	 }
	
	
	// LANGUAGES
	/**
	 * Gets language name.
	 * 
	 * @param langCode language code
	 * @return language name, null if none
	 */
	public String getLangName(String langCode) {
		return langNames.get(langCode);
	}

	/**
	 * Gets language code.
	 * 
	 * @param langName language name
	 * @return language code, null if none
	 */
	public String getLangCode(String langName) {
		if (langName == null) return null;
		langName = langName.toLowerCase();
		Set<Entry<String, String>> entries = langNames.entrySet();
		for (Entry<String, String> entry : entries) {
			if (entry.getValue().equals(langName)) return entry.getKey();
		}
		return null;
	}
	
	/**
	 * Checks if the given language code.
	 * 
	 * @param langCode language code to test
	 * @return true if language code
	 */
	public boolean isLangCode(String langCode) {
		return langNames.containsKey(langCode);
	}
	
	/**
	 * Finds a language code.
	 * @param lang language
	 * @return language code; null if none
	 */
	public String findLangCode(String lang)
	 {
		String langCode = null;
		if (MinorityTranslateModel.wikis().isLangCode(lang)) langCode = lang;
		else langCode = MinorityTranslateModel.wikis().getLangCode(lang);
		return langCode;
	 }
	
	/**
	 * Gets all language names.
	 * 
	 * @return gets all language names
	 */
	public String[] getLangNames() {
		return langNameList;
	}

	/**
	 * Gets language names.
	 * 
	 * @return gets language names, null elements if none
	 */
	public String[] getLangNames(String[] langCodes) {
		String[] langNames = new String[langCodes.length];
		for (int i = 0; i < langCodes.length; i++) {
			langNames[i] = getLangName(langCodes[i]);
		}
		
		return langNames;
	}

	/**
	 * Gets language names.
	 * 
	 * @return gets language names, null elements if none
	 */
	public ArrayList<String> getLangNames(ArrayList<String> langCodes) {
		ArrayList<String> langNames = new ArrayList<>();
		for (String langCode : langCodes) {
			langNames.add(getLangName(langCode));
		}
		return langNames;
	}
	
	/**
	 * Checks if languages are loaded.
	 * 
	 * @return true if loaded
	 */
	public boolean isLoaded() {
		return langNames.size() > 0;
	}

	/**
	 * Gets the wiki type.
	 * 
	 * @param langCode language code
	 * @return wiki type
	 */
	public WikiType getWikiType(String langCode) {
		WikiType result = wikiTypes.get(langCode);
		if (result == null) return WikiType.UNKNOWN;
		return result;
	}

	
	// SUGGEST
	/**
	 * Finds all language suggestions.
	 * 
	 * @param search search term
	 * @return all suggestions
	 */
	public String[] findSuggestions(String search)
	 {
		if (search.isEmpty()) return new String[0];
		
		search = search.toLowerCase();
		
		ArrayList<String> results = new ArrayList<>(10);
		
		for (int i = 0; i < langNameList.length && results.size() < RESULT_COUNT; i++) {
			if (langNameList[i].toLowerCase().startsWith(search)) results.add(langNameList[i]); 
		}
		
		String langName = getLangName(search);
		if (langName != null) {
			results.remove(langName);
			if (results.size() == 10) results.remove(9);
			results.add(0, langName);
		}
		
		return results.toArray(new String[results.size()]);
	 }
	
	
	// NAMESPACES
	public boolean isNamespaceMapped(String langCode)
	 {
		return namespaces.containsKey(langCode);
	 }
	
	public void mapNamespaces(String langCode, HashMap<Namespace, String> mappings)
	 {
		namespaces.put(langCode, mappings);
	 }
	
	public HashMap<Namespace, String> findNamespaceMappings(String langCode)
	 {
		HashMap<Namespace, String> mappings = namespaces.get(langCode);
		if (mappings == null) mappings = namespaces.get("en");
		
		return mappings;
	}

	public HashMap<Namespace, String> findNamespaceMappings()
	 {
		return findNamespaceMappings(MinorityTranslateModel.preferences().getGUILangCode());
	 }
	
	public HashMap<Integer, String> findNamespaceIntegerMappings()
	 {
		HashMap<Namespace, String> mappings = findNamespaceMappings(MinorityTranslateModel.preferences().getGUILangCode());
		HashMap<Integer, String> intMappings = new HashMap<>();
		
		Set<Entry<Namespace, String>> entries = mappings.entrySet();
		for (Entry<Namespace, String> entry : entries) {
			intMappings.put(entry.getKey().ordinal(), entry.getValue());
		}
		
		return intMappings;
	 }

	public String findNamespace(String langCode, Namespace namespace)
	 {
		return findNamespaceMappings(langCode).get(namespace);
	 }

	public String findNamespace(Namespace namespace)
	 {
		return findNamespaceMappings().get(namespace);
	 }
	
	public Namespace findNamespace(String langCode, String strNamespace)
	 {
		HashMap<Namespace, String> mappings = findNamespaceMappings(langCode);
		if (mappings == null) return null;
		
		Set<Entry<Namespace, String>> entries = mappings.entrySet();
		for (Entry<Namespace, String> entry : entries) {
			if (entry.getValue().equalsIgnoreCase(strNamespace)) return entry.getKey();
		}
		
		return null;
	 }

	
	// UTILITY
	public static String wikiTitle(String title, String langCode)
	 {
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
		switch (wikiType) {
		case REGULAR:
			return title;
		
		case INCUBATOR:
			String prefix = WikisProxy.prefix(langCode);
			return WikisProxy.prefixTitle(title, prefix);
			
		default:
			return title;
		}
	 }
	
	public static String prefix(String langCode)
	 {
		return INCUBATOR_PREFIX.replace("#langCode", langCode);
	 }

	public static String prefixTitle(String title, String prefix)
	 {
		return prefix + title;
	 }

	public static String deprefixTitle(String title, String prefix)
	 {
		if (title.startsWith(prefix)) title = title.substring(prefix.length());
		return title;
	 }

	public long getWikisUpdateTime() {
		if (lastWikisUpdate == null) return 0;
		return lastWikisUpdate;
	}

	
	
}
