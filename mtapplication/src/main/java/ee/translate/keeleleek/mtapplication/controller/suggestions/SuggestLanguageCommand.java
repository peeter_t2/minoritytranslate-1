package ee.translate.keeleleek.mtapplication.controller.suggestions;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.suggestions.Suggestable;

public class SuggestLanguageCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Suggestable suggestable = (Suggestable) notification.getBody();
		String search = suggestable.getSearchTerm();
		
		String[] results = MinorityTranslateModel.wikis().findSuggestions(search);
		capitalise(results);
		
		suggestable.suggest(results);
	 }
	
	
	// UTILITY:
	public static String capitalise(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	public static void capitalise(String[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = capitalise(arr[i]);
		}
	}
	
}
