package ee.translate.keeleleek.mtapplication.controller.suggestions;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.suggestions.SearchRequest;

public class RequestSuggestionsCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		SearchRequest request = (SearchRequest) notification.getBody();
		
		MinorityTranslateModel.suggestions().searchSuggestions(request);
	 }
	
}
