package ee.translate.keeleleek.mtapplication.view.messages;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;

public class MessagesBundle extends ResourceBundle {

	private final ResourceBundle mainBundle;
	private final ResourceBundle enBundle;
	
	
	public MessagesBundle(String langCode)
	 {
		ResourceBundle mainBundle = null;

		// from file
		File path = new File(MinorityTranslate.ROOT_PATH + "/" + Messages.I18N_NAME + "/" + "messages_" + langCode + ".properties");
		if (path.isFile()) {
			try (InputStreamReader fis =  new InputStreamReader(new FileInputStream(path), "UTF-8")) {
				mainBundle = new PropertyResourceBundle(fis);
				Messages.LOGGER.info("Loaded " + langCode + " messages file from " + Messages.I18N_NAME);
			} catch (Exception e) {
				Messages.LOGGER.warn("Failed to read resource bundle.", e);
			}
		}
		
		// from jar
		if (mainBundle == null) {
			mainBundle = ResourceBundle.getBundle("messages", new Locale(langCode), new UTF8Control());
			Messages.LOGGER.info("Loaded " + langCode + " messages file from jar");
		}
		
		// set
		this.mainBundle = mainBundle;
		if (langCode.equals("en")) this.enBundle = this.mainBundle;
		else this.enBundle = ResourceBundle.getBundle("messages", new Locale("en"), new UTF8Control());
	 }

	@Override
	protected Object handleGetObject(String key) {
		if (mainBundle.containsKey(key)) return mainBundle.getObject(key);
		if (enBundle.containsKey(key)) return enBundle.getObject(key);
		return "!" + key + "!";
	}

	@Override
	public Enumeration<String> getKeys() {
		return enBundle.getKeys();
	}

	@Override
	public boolean containsKey(String key) {
		return true;
	}
	
	
}
