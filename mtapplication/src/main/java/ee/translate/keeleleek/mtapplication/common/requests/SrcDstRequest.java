package ee.translate.keeleleek.mtapplication.common.requests;

import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class SrcDstRequest {

	private Reference srcRef;
	private Reference dstRef;
	
	
	public SrcDstRequest(Reference srcRef, Reference dstRef) {
		super();
		this.srcRef = srcRef;
		this.dstRef = dstRef;
	}


	public Reference getSrcRef() {
		return srcRef;
	}


	public Reference getDstRef() {
		return dstRef;
	}
	
	
}
