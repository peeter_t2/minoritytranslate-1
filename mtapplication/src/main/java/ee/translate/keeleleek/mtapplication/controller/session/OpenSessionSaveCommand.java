package ee.translate.keeleleek.mtapplication.controller.session;

import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class OpenSessionSaveCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Path path = MinorityTranslateModel.session().getSessionPath();
		
		if (path == null) {
			sendNotification(Notifications.WINDOW_SAVE_SESSION_OPEN);
		} else {
			sendNotification(Notifications.SESSION_SAVE, path);
		}
	 }
	
}
