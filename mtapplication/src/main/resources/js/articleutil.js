var Range = ace.require('ace/range').Range;

// http://stackoverflow.com/questions/6846230/coordinates-of-selected-text-in-browser-page
function findCaretCoordinates()
{
    var pos = editor.getCursorPosition();
    return editor.renderer.textToScreenCoordinates(pos).pageX + "," + editor.renderer.textToScreenCoordinates(pos).pageY;
}

// http://stackoverflow.com/questions/11247737/how-can-i-get-the-word-that-the-caret-is-upon-inside-a-contenteditable-div
function findPreceding()
{
    var pos = editor.getCursorPosition();
    var row = pos.row;
    var col1 = 0;
    var col2 = pos.column;
    if ((col2 - col1) > 48) col1 = col2 - 48;
    var range = new Range(row, col1, row, col2);
    return editor.getSession().getTextRange(range);
}

function findSelected()
{
    return editor.getSelectedText();
}

function extendBackward(count)
{
	var range = editor.selection.getRange();
	var col = range.start.column - count;
	if (col < 0) col = 0;
	editor.selection.setRange(new Range(range.start.row, col, range.end.row, range.end.column));
}

function moveBackward(count)
{
	editor.navigateLeft(count);
}

function moveForward(count)
{
	editor.navigateRight(count);
}

function positionCaret() {

}

function doHighlight(bodyText, searchTerm, highlightStartTag, highlightEndTag) 
{
  // the highlightStartTag and highlightEndTag parameters are optional
  if ((!highlightStartTag) || (!highlightEndTag)) {
    highlightStartTag = "<font style='color:blue; background-color:yellow;'>";
    highlightEndTag = "</font>";
  }
  
  // find all occurences of the search term in the given text,
  // and add some "highlight" tags to them (we're not using a
  // regular expression search, because we want to filter out
  // matches that occur within HTML tags and script blocks, so
  // we have to do a little extra validation)
  var newText = "";
  var i = -1;
  var lcSearchTerm = searchTerm.toLowerCase();
  var lcBodyText = bodyText.toLowerCase();
    
  while (bodyText.length > 0) {
    i = lcBodyText.indexOf(lcSearchTerm, i+1);
    if (i < 0) {
      newText += bodyText;
      bodyText = "";
    } else {
      // skip anything inside an HTML tag
      if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
        // skip anything inside a <script> block
        if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
          newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
          bodyText = bodyText.substr(i + searchTerm.length);
          lcBodyText = bodyText.toLowerCase();
          i = -1;
        }
      }
    }
  }
  
  return newText;
}

/*
 * This is sort of a wrapper function to the doHighlight function.
 * It takes the searchText that you pass, optionally splits it into
 * separate words, and transforms the text on the current web page.
 * Only the "searchText" parameter is required; all other parameters
 * are optional and can be omitted.
 */
function highlightSearchTerms(searchText, treatAsPhrase, warnOnFailure, highlightStartTag, highlightEndTag)
{
  // if the treatAsPhrase parameter is true, then we should search for 
  // the entire phrase that was entered; otherwise, we will split the
  // search string so that each word is searched for and highlighted
  // individually
  if (treatAsPhrase) {
    searchArray = [searchText];
  } else {
    searchArray = searchText.split(" ");
  }
  
  if (!document.body || typeof(document.body.innerHTML) == "undefined") {
    if (warnOnFailure) {
      alert("Sorry, for some reason the text of this page is unavailable. Searching will not work.");
    }
    return false;
  }
  
  var bodyText = document.body.innerHTML;
  for (var i = 0; i < searchArray.length; i++) {
    bodyText = doHighlight(bodyText, searchArray[i], highlightStartTag, highlightEndTag);
  }
  
  document.body.innerHTML = bodyText;
  return true;
}

function highlight(text)
{
  highlightSearchTerms(text, false, true, "<u>", "</u>")
}

var lastRange = null;
var lastText = null;

function find(text, isBackwards, isWarp, isCaseSensitive, isWholeWord)
{
  var foundRange = editor.find(text,{
    backwards: isBackwards
    ,wrap: isWarp
    ,caseSensitive: isCaseSensitive
    ,wholeWord: isWholeWord
  });
  
  find.lastRange = foundRange;
  
  return foundRange;
}

find.lastRange = null;

function replaceFind(text)
{
  if (editor.selection.getRange().isEqual(find.lastRange)) {
  	editor.replace(text);
  }
}

function replaceAll(text, replacement, isBackwards, isWarp, isCaseSensitive, isWholeWord)
{
  editor.replaceAll(replacement,{
    needle: text  
    ,backwards: isBackwards
    ,wrap: isWarp
    ,caseSensitive: isCaseSensitive
    ,wholeWord: isWholeWord
  });
}



