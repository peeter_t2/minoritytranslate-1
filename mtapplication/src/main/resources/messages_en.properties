
# menu
menu.program = Program
menu.program.new.session = New Session
menu.program.open.session = Open Session...
menu.program.save.session = Save Session
menu.program.save.session.as = Save Session As...
menu.program.preferences = Preferences...
menu.program.login = Log In...
menu.program.logout = Log Out
menu.program.quit = Quit

menu.navigation = Navigation
menu.navigation.next = Next
menu.navigation.previous = Previous
menu.navigation.toggle.preview = Toggle Preview
menu.navigation.toggle.exact = Toggle Exact

menu.articles = Articles
menu.articles.upload.enabled = Upload Enabled
menu.articles.upload = Upload Selected Articles
menu.articles.add.article = Add Article...
menu.articles.add.category = Add Category...
menu.articles.add.links = Add Article Links...
menu.articles.add.content = Add Content...
menu.articles.stop.downloading = Halt Downloading
menu.articles.suggestion.lists = Suggestion Lists...
menu.articles.remove.selected = Remove Selected
menu.articles.remove.article = Remove Article...

menu.info = Info
menu.info.about = About...
menu.info.manual = Manual...
menu.info.reset.quick.start = Reset Quick Start...

# toolbar
toolbar.next.button = Next
toolbar.next.tooltip = Select next article (Ctrl+.)
toolbar.previous.button = Previous
toolbar.previous.tooltip = Select previous article (Ctrl+,)
toolbar.upload.button = Upload
toolbar.upload.tooltip = Mark active article(s) for upload (Ctrl+Shift+U)
toolbar.pull.button = Pull
toolbar.pull.tooltip = Translate/adjust text from source to destination
toolbar.horizontal.button = Horizontal
toolbar.horizontal.tooltip = Switch to horizontal view
toolbar.vertical.button = Vertical
toolbar.vertical.tooltip = Switch to vertical view
toolbar.align.button = Align
toolbar.align.tooltip = Switch to align view
toolbar.single.button = Single
toolbar.single.tooltip = Open single editor
toolbar.tag.button = Tag
toolbar.tag.tooltip = Tag the session (keeleleek)
toolbar.speller.button = Speller
toolbar.speller.button.tooltip = Select a speller to use
toolbar.preview.button = Preview
toolbar.preview.tooltip = Toggle preview for active tab (Ctrl+P)
toolbar.exact.checkbox = Exact translation
toolbar.exact.tooltip = Translation is sentence for sentence (Ctrl+E)
toolbar.adaptation.checkbox = Adaptation
toolbar.adaptation.tooltip = Text has been adapted loosely (Ctrl+E)
toolbar.spell.check.button = Spell Check
toolbar.spell.check.tooltip = Enable or disable a spell check
toolbar.reset.button = Reset
toolbar.reset.tooltip = Reset and re-download article

# views
align.down.button = Down
align.up.button = Up
align.bind.scrollbars = Bind scrolling

# statusbar
statusbar.logged.in = Logged in
statusbar.not.logged.in = Not logged in
statusbar.no.connection = No connection

statusbar.unknown = Unknown
statusbar.download.requested = Download requested
statusbar.download.in.progress = Downloading...
statusbar.standby = Standing by
statusbar.upload.requested = Upload requested
statusbar.upload.preparing = Preparing to upload
statusbar.upload.in.progress = Uploading...
statusbar.upload.confirming = Confirming upload
statusbar.linking.articles = Linking articles
statusbar.sending.usage.info = Sending usage info to Keeleleek
statusbar.upload.conflict = Upload conflict!
statusbar.upload.failed = Upload failed!
statusbar.checking.title.tooltip = Checking title availability

statusbar.pulling = Pulling
statusbar.spell.checking = Spell checking
statusbar.queuing = Queuing
statusbar.downloading = Downloading
statusbar.previewing = Previewing
statusbar.uploading = Uploading
statusbar.saving.session = Saving
statusbar.checking.title = Checking title

statusbar.filters.templates = Filter Templates
statusbar.filters.files = Filter Files
statusbar.filters.introduction = Filter Introduction
statusbar.filters.references = Filter References

# window
window.translate = Minority Translate
window.login = Login
window.add.article = Add Article
window.remove.article = Remove Article
window.add.category = Add Category
window.add.links = Add Article Links
window.preferences = Preferences
window.about = About
window.manual = Manual
window.lists = Lists

# addons
addons.notes = Notes
addons.notes.tooltip = Session notes
addons.articles = Articles
addons.articles.tooltip = Currently open articles list
addons.articlons.remove = Remove
addons.snippets = Snippets
addons.snippets.tooltip = Pasteable text snippets

addons.lookup = Lookup
addons.lookup.tooltip = Word lookup
addons.lookup.search.button = Search

addons.find = Find/Replace
addons.find.tooltip = Text search and replacement
addons.find.find.label = Find what
addons.find.replace.label = Replace with
addons.find.wrap.select = Wrap
addons.find.wrap.case.sensitive.select = Case sensitive
addons.find.wrap.whole.word.select = Whole word
addons.find.next.button = Next
addons.find.previous.button = Previous
addons.find.replace.button = Replace
addons.find.replace.all.button = Replace All

# dialog
button.ok = Ok
button.cancel = Cancel
button.add = Add
button.remove = Remove
button.reset = Reset
button.insert.variables = Variables
button.insert.variables.tooltip = Variables will be replaced with the corresponding value

# dialog (session)
session.dialog.open = Open
session.dialog.save = Save
session.dialog.look.in = Look in
session.dialog.save.in = Save in
session.dialog.file.name = File name
session.dialog.files.type = Files of type
session.dialog.session.file = Session file
session.dialog.file.name = Name
session.dialog.file.size = Size
session.dialog.file.type = Type
session.dialog.file.date = Modified
session.dialog.new.folder.error = Error creating new folder
session.dialog.yes = Yes
session.dialog.no = No

# dialog (about)
about.version = Version
about.license = License
about.authors = Authors
about.supported.by = Supported by
about.thanks.to = Thanks to

# dialog (simple dialog)
sdialog.language = Language

# dialog (simple dialog add article)
sdialog.add.article.header = Enter article
sdialog.add.article.title = Title

# dialog (simple dialog remove article)
sdialog.remove.article.header = Enter article
sdialog.remove.article.title = Title

# dialog (simple dialog add category)
sdialog.add.category.header = Enter category
sdialog.add.category.title = Title

# dialog (simple dialog add links)
sdialog.add.links.header = Enter article
sdialog.add.links.title = Title

# dialog (lists)
lists.title = Select articles
lists.articles.column = Articles
lists.loading.status = Loading suggestions
lists.uncategorized.branch = Uncategorized
lists.buttons.list.enabled = Enabled
lists.buttons.remove = Remove

# dialog (login)
login.header = Log in to Wikipedia
login.username = Username
login.password = Password
login.stay.logged.in = Stay logged in

# dialog (manual)
manual.program = Program
manual.wikitext = Wikitext

# dialog (unicode table)
unicode.table.title = Unicode table

# dialog (add content)
add.content.dialog.title = Add content
add.content.dialog.header = Enter titles
add.content.dialog.name = Name
add.content.dialog.language = Language
add.content.dialog.namespace = Namespace
add.content.paste.button = Paste
add.content.paste.button.tooltip = Paste a list of titles.\nUses selected language and namespace.
add.content.expand.categories = Expand categories
add.content.expand.categories.tooltip = If checked, all category items will be included
add.content.add.category.articles = Add category articles
add.content.add.category.articles.tooltip = If checked, category articles will be added to the list
add.content.add.category.categories = Add category subcategories
add.content.add.category.articles.tooltip = If checked, category subcategories will be added to the list

# dialog (preferences)
preferences.common.filter = Filter
preferences.common.filter.source.tooltip = The source language codes for which the change will be applied to (regex)
preferences.common.filter.destination.tooltip = The target language codes for which the change will be applied to (regex)

preferences.program = Program
preferences.program.category.depth = Category depth
preferences.program.category.depth.tooltip = Specifies how many category sublevels will be included on adding articles.
preferences.program.filters = Filters
preferences.program.filters.templates = Don't show templates
preferences.program.filters.templates.tooltip = Show source article without templates
preferences.program.filters.files = Don't show images
preferences.program.filters.files.tooltip = Show source article without images
preferences.program.filters.introduction.only = Show only introduction
preferences.program.filters.introduction.only.tooltip = Show only introduction in translation source
preferences.program.filters.references = Don't show references
preferences.program.filters.references.tooltip = Show source article without references
preferences.program.interface.language = Interface language
preferences.program.interface.language.tooltip = Available user interface languages.\nNew languages can be added to i18n folder.\nRequires program restart. 
preferences.program.font.size = Font size
preferences.program.font.size.tooltip = Font size.\nRequires program restart.

preferences.program.languages = Languages
preferences.program.languages.explanation = Languages can be added by specifying the language code or language name (as written by native speakers). You can specify whether to display each chosen language as source or target or neither for the current article. If you add a new language, also update your language skills levels in text corpus.
preferences.program.languages.language = Language
preferences.program.languages.display = Display
preferences.program.languages.display.from = Source
preferences.program.languages.display.to = Target
preferences.program.languages.display.none = Don't show
preferences.program.languages.display.tooltip = Source languages (lower tabs), target languages (upper tabs) or don't show.
preferences.program.languages.buttons.add = Add
preferences.program.languages.buttons.remove = Remove
preferences.program.languages.edit.tooltip = Enter Wikipedia language code or language name in native use.

preferences.program.corpus = Language skills
preferences.program.corpus.explanation = Your submissions will be included in building a corpus of text translations that may benefit research and language technology. For the best quality, please indicate your skill level in the languages used. For more information visit:
preferences.program.corpus.link = http://translate.keeleleek.ee/wiki/Corpus
preferences.program.corpus.collect = I want my submissions to be included in the corpus
preferences.program.corpus.collect.tooltip = If checked, edit data is sent to the Keeleleek server
preferences.program.corpus.proficiency = Skill level
preferences.program.corpus.proficiency.bad = Bad
preferences.program.corpus.proficiency.average = Average
preferences.program.corpus.proficiency.good = Good
preferences.program.corpus.proficiency.unspecified = Unspecified
preferences.program.corpus.proficiency.level1 = Basic
preferences.program.corpus.proficiency.level2 = Limited
preferences.program.corpus.proficiency.level3 = Good
preferences.program.corpus.proficiency.level4 = Excellent
preferences.program.corpus.proficiency.level5 = Native
preferences.program.corpus.default.translation = I usually (by default) 
preferences.program.corpus.default.translation.adaptation = adapt the sources to fit our needs more freely.
preferences.program.corpus.default.translation.adaptation.tooltip = Article default is set to adaptation translation (loose translation)
preferences.program.corpus.default.translation.exact = translate texts exactly
preferences.program.corpus.default.translation.exact.tooltip = Article default is set to exact translation (preserves sentence structure)

preferences.processing.page = Text Processing
preferences.processing.explanation = For information on text processing visit:
preferences.processing.link = http://translate.keeleleek.ee/wiki/Text_processing
preferences.processing.remove.row = Remove
preferences.processing.create.row = Create
preferences.processing.create.row.before = Create (Before)
preferences.processing.create.row.after = Create (After)
preferences.processing.move.row.up = Move Up
preferences.processing.move.row.down = Move Down

preferences.processing.collect.page = Collect
preferences.processing.collect.filter = Filter
preferences.processing.collect.find = Find
preferences.processing.collect.parameters = Parameters

preferences.processing.replace.page = Replace
preferences.processing.replace.filter = Filter
preferences.processing.replace.find = Find
preferences.processing.replace.replace = Replace

preferences.processing.template.mapping.page = Templates
preferences.processing.template.mapping.source.column = Source
preferences.processing.template.mapping.destination.column = Target
preferences.processing.template.mapping.source.template = Source Template
preferences.processing.template.mapping.source.parameter = Source Parameter
preferences.processing.template.mapping.source.parameter.tooltip = Parameter name to replace.\nEmpty value will add a new parameter.\nNumbers as names affect ordering.
preferences.processing.template.mapping.destination.parameter = Target Parameter
preferences.processing.template.mapping.destination.parameter.tooltip = Parameter name to replace with.\nSetting name=value will overwrite the current value.\nEmpty value will remove the parameter.\nNumbers as names affect ordering.
preferences.processing.template.mapping.add.row.after = Add (After)
preferences.processing.template.mapping.insert.after = Add (Before)
preferences.processing.template.mapping.move.row.up = Move Up
preferences.processing.template.mapping.move.row.down = Move Down
preferences.processing.template.mapping.remove.row = Remove

preferences.content.assist = Content Assist
preferences.content.assist.insert.automatically = Insert automatically
preferences.content.assist.insert.automatically.tooltip = If checked, single autocomplete choices will be inserted without a prompt
preferences.content.assist.search.links = Search links
preferences.content.assist.search.links.tooltip = If checked, autocomplete will also look for possible wikilinks

preferences.content.assist.pull.translate.wikilinks = Translate wikilinks when pulling
preferences.content.assist.pull.translate.wikilinks.tooltip = If checked, wikilinkls will be translated during pulling
preferences.content.assist.pull.translate.templates = Translate templates when pulling
preferences.content.assist.pull.translate.templates.tooltip = If checked, template names will be translated during pulling.\nParameters can be mapped from the preferences and pull dialog

preferences.content.assist.inserts = Inserts
preferences.content.assist.inserts.name = Name
preferences.content.assist.inserts.name.tooltip = Name to display in the autocomplete dialog
preferences.content.assist.inserts.trigger = Trigger
preferences.content.assist.inserts.trigger.tooltip = Preceeding text which triggers the insert
preferences.content.assist.inserts.insert = Insert
preferences.content.assist.inserts.insert.tooltip = Text to insert
preferences.content.assist.inserts.add.row.after = Add (After)
preferences.content.assist.inserts.insert.after = Add (Before)
preferences.content.assist.inserts.move.row.up = Move Up
preferences.content.assist.inserts.move.row.down = Move Down
preferences.content.assist.inserts.remove.row = Remove

preferences.content.assist.symbols = Symbols
preferences.content.assist.symbols.add.after.button = Add (After)
preferences.content.assist.symbols.add.before.button = Add (Before)
preferences.content.assist.symbols.remove.button = Remove
preferences.content.assist.symbols.set.import.button = Import Set
preferences.content.assist.symbols.set.export.button = Export Set
preferences.content.assist.symbols.move.up.button = Move Up
preferences.content.assist.symbols.move.down.button = Move Down
preferences.content.assist.symbols.symbol = Symbol
preferences.content.assist.symbols.description = Description
preferences.content.assist.symbols.description.tooltip = Symbol description
preferences.content.assist.symbols.trigger = Trigger
preferences.content.assist.symbols.trigger.tooltip = Preceeding text that will trigger the symbol when autocompleting
preferences.content.assist.symbols.filter = Filter
preferences.content.assist.symbols.row = Toolbar row
preferences.content.assist.symbols.row.tooltip = The toolbar row in which the symbol will be displayed
preferences.content.assist.symbols.row.none = Don't show
preferences.content.assist.symbols.row.first = First
preferences.content.assist.symbols.row.second = Second
preferences.content.assist.symbols.row.third = Third
preferences.content.assist.symbols.file.name = Symbol set file
preferences.content.assist.symbols.dialog.explanation = Double click a symbol above or enter manually

preferences.lookups = Lookup
preferences.lookups.name = Name
preferences.lookups.name.tooltip = Lookup name to display
preferences.lookups.url = URL
preferences.lookups.url.tooltip = URL to send the request to
preferences.lookups.parameters = Parameters
preferences.lookups.parameters.tooltip = Request parameters
preferences.lookups.source.element = Source Element
preferences.lookups.source.element.tooltip = Source element to look for (use collect rules)
preferences.lookups.destination.element = Destination Element
preferences.lookups.destination.element.tooltip = Destination element to insert into (use replace rules)
preferences.lookups.destination.opening = Start with
preferences.lookups.destination.opening.tooltip = Result beginning (followed by destination elements)
preferences.lookups.destination.closing = End with
preferences.lookups.destination.closing.tooltip = Result ending (preceeded by destination elements)
preferences.lookups.add.row.after = Add (After)
preferences.lookups.add.row.before = Add (Before)
preferences.lookups.move.row.up = Move Up
preferences.lookups.move.row.down = Move Down
preferences.lookups.remove.row = Remove

# messages
messages.error = Error
messages.warning = Warning
messages.info = Info

# messages (login)
messages.login.error = Login failed
messages.login.error.username.empty = Username field is empty!
messages.login.error.password.empty = Password field is empty!
messages.login.error.wrong.password = Wrong password!
messages.login.upload.login.required = Login required!

# messages (adding)
messages.invalid.language.header = Invalid language
messages.invalid.language.content.language.not.valid = Entered language "#" is not valid

messages.invalid.name.header = Invalid title
messages.invalid.name.content.name.empty = Please enter a title

messages.adding.header.no.languages = No languages selected
messages.adding.content.add.one.language = Please add at least one language before loading/removing content

# messages (upload request)
messages.upload.request.failure.header = Upload request failed
messages.upload.request.failure.content.missing.title = Please enter a title before uploading
messages.upload.request.failure.content.title.in.use = An article with the entered title already exists
messages.upload.request.failure.content.title.not.checked = Title availability not determined
messages.upload.request.failure.content.no.edits.found = There are no changes to upload

# messages (session)
messages.session.save.failed = Failed to save session
messages.session.save.failed.to = Failed to save session # - #!

messages.session.load.failed = Failed to load session
messages.session.load.failed.to = Failed to load session # - #!

# messages (preferences)
messages.preferences.save.failed = Failed to save preferences
messages.preferences.save.failed.to = Failed to save preferences # - #!

messages.preferences.load.failed = Failed to load preferences
messages.preferences.load.failed.to = Failed to load preferences # - #!

# messages (lists)
messages.list.no.items.selected = No items selected
messages.list.no.items.selected.select = Please select at least one item!

# messages (pull)
messages.pull.header.pulling.failed = Pull failed
messages.pull.description.target.not.editable = Pull can only be performed on editable articles!
messages.pull.description.regex.error = Regular expression is not valid (#description)!\n#pattern

# messages (paste)
messages.paste.header.pulling.failed = Paste failed
messages.paste.description.target.not.editable = Paste can only be performed on editable articles!
messages.paste.description.regex.error = Regular expression is not valid (#description)!\n#pattern

# messages (confirmation)
confirmation.yes = Yes
confirmation.no = No
confirmation.cancel = Cancel
confirmation.reset = Reset

# messages (confirmation save)
confirmation.save.title = Save Changes?
confirmation.save.quit.message = Save changes before closing the program?
confirmation.save.new.session.message = Save changes before closing the session?
confirmation.save.details = Unsaved translations will be lost
confirmation.save.yes = Save
confirmation.save.no = Discard

# messages (tagging)
tag.title = Tag Session
tag.header = Specify session tag
tag.content = Tag

# messages (align)
messages.align.error.header = Align failed
messages.align.error.content.destination.not.editable = Target article is not editable!
messages.align.error.content.source.not.ready = Source article is not ready!

# messages (symbols)
messages.symbols.import.error.header = Symbols import failed
messages.symbols.import.error.failed.to.read = Failed to read file #path!\n#message
messages.symbols.import.error.failed.to.parse = Failed to parse import file!\n#message

messages.symbols.export.error.header = Symbols export failed
messages.symbols.export.error.failed.to.write = Failed to write file #path!\n#message

# messages (lookup)
messages.lookup.error.header = Lookup failed
messages.lookup.error.lookup.not.found = Lookup with name #name not found!
messages.lookup.error.failed.to.retrieve.content = Failed to retrieve content!\n#message

# messages (reset article confirmation)
messages.reset.article.confirmation.title = Confirm reset
messages.reset.article.confirmation.header = Are you sure you want to reset?
messages.reset.article.confirmation.content = Resetting may cause the loss of progress

# messages (spell check)
messages.spell.check.failed.header = Spell check failed
messages.spell.check.failed.content.language.not.supported = # plugin does not support # language
messages.spell.check.failed.content.exception = #

# messages (pull)
messages.pull.failed.header = Pull failed
messages.pull.failed.content.language.pair.not.supported = # plugin does not support #-# language pair
messages.pull.failed.content.exception = #

# messages (linking)
messages.linking.failed.header = Linking failed
messages.linking.failed.content.message = #message

# quick-start
quick.start.title = Quick Start
quick.start.next.button = Next
quick.start.finish.button = Finish
quick.start.select.interface.language.html = /guide/interface_language-en.html
quick.start.select.interface.language = Select language
quick.start.languages.html = /guide/languages-en.html
quick.start.languages.destination.language = Target language
quick.start.languages.source.language = Source language
quick.start.languages.destination.language.prompt = Enter language
quick.start.languages.source.language.prompt = Enter language
quick.start.adding.articles.html = /guide/adding_articles-en.html
quick.start.translate.features.html = /guide/translate_features-en.html
quick.start.translate.extra.features.html = /guide/translate_extra_features-en.html
quick.start.sessions.html = /guide/sessions-en.html
quick.start.uploading.articles.html = /guide/uploading_articles-en.html
quick.start.text.corpus.html = /guide/text_corpus-en.html
quick.start.empty.html = /guide/empty.html
quick.start.corpus.language.proficiency = Skill level in #language language
quick.start.corpus.collect.data = I want my submissions to be included in the corpus

# plugins
plugins.pull.title = Pull
plugins.pull.header = Pulling article

plugins.pull.preparing = Preparing to pull
plugins.pull.traslating.wikilinks = Translating wikilinks
plugins.pull.traslating.templates = Translating templates
plugins.pull.translating.text = Translating text
plugins.pull.mapping.group = Group
plugins.pull.mapping.group.tooltip = Used to sort similar mappings
plugins.pull.mapping.source = Source
plugins.pull.mapping.destination = Target
plugins.pull.mapping.source.name = Source
plugins.pull.mapping.destination.name = Target
plugins.pull.save.template.mappings = Save template parameter mappings
plugins.pull.save.template.mappings.tooltip = If checked, the preferences will be updated wit new mappings

plugins.disabled.name = Disabled
plugins.hunspell.name = Hunspell
plugins.copy.replace.paste.name = Copy Replace Paste
plugins.apertium.name = Apertium


